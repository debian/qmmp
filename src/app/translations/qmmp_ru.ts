<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>BuiltinCommandLineOption</name>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="46"/>
        <source>Don&apos;t clear the playlist</source>
        <translation>Не очищать лист</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="47"/>
        <source>Start playing current song</source>
        <translation>Воспроизвести текущую песню</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="48"/>
        <source>Pause current song</source>
        <translation>Приостановить текущую песню</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="49"/>
        <source>Pause if playing, play otherwise</source>
        <translation>Приостановить/воспроизвести</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="50"/>
        <source>Stop current song</source>
        <translation>Остановить текущую песню</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="51"/>
        <source>Display Jump to Track dialog</source>
        <translation>Показать диалог перехода к дорожке</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="52"/>
        <source>Quit application</source>
        <translation>Завершить приложение</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="53"/>
        <source>Set playback volume (example: qmmp --volume 20)</source>
        <translation>Установить громкость (пример: qmmp --volume 20)</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="54"/>
        <source>Print volume level</source>
        <translation>Вывести уровень громкости</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="55"/>
        <source>Mute/Restore volume</source>
        <translation>Приглушить/Восстановить громкость</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="56"/>
        <source>Print mute status</source>
        <translation>Проверить, приглушен ли звук</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="57"/>
        <source>Skip forward in playlist</source>
        <translation>Перейти к следующему фрагменту</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="58"/>
        <source>Skip backwards in playlist</source>
        <translation>Перейти к предыдущему фрагменту</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="59"/>
        <source>Show/hide application</source>
        <translation>Показать/скрыть приложение</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="60"/>
        <source>Show main window</source>
        <translation>Показать главное окно</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="61"/>
        <source>Display Add File dialog</source>
        <translation>Показать диалог добавления файлов</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="62"/>
        <source>Display Add Directory dialog</source>
        <translation>Показать диалог добавления каталогов</translation>
    </message>
</context>
<context>
    <name>QMMPStarter</name>
    <message>
        <location filename="../qmmpstarter.cpp" line="149"/>
        <source>Unknown command</source>
        <translation>Неизвестная команда</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="508"/>
        <source>Usage: qmmp [options] [files]</source>
        <translation>Использование: qmmp [options] [files]</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="509"/>
        <source>Options:</source>
        <translation>Опции:</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="515"/>
        <source>Start qmmp with the specified user interface</source>
        <translation>Запустить qmmp с указанным интерфейсом пользователя</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="516"/>
        <source>List all available user interfaces</source>
        <translation>Вывести доступные интерфейсы пользователя</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="517"/>
        <source>Don&apos;t start the application</source>
        <translation>Не запускать приложение</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="518"/>
        <source>Print debugging messages</source>
        <translation>Показывать отладочные сообщения</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="519"/>
        <source>Display this text and exit</source>
        <translation>Показать этот текст и выйти</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="520"/>
        <source>Print version number and exit</source>
        <translation>Показать версии и выйти</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="522"/>
        <source>Home page: %1</source>
        <translation>Домашняя страница: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="523"/>
        <source>Development page: %1</source>
        <translation>Страница разработки: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="524"/>
        <source>Bug tracker: %1</source>
        <translation>Система регистрации ошибок: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="171"/>
        <location filename="../qmmpstarter.cpp" line="529"/>
        <source>Command Line Help</source>
        <translation>Справка по командной строке</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="542"/>
        <source>QMMP version: %1</source>
        <translation>Версия QMMP: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="543"/>
        <source>Compiled with Qt version: %1</source>
        <translation>Собрано с версией Qt: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="544"/>
        <source>Using Qt version: %1</source>
        <translation>Используемая версия Qt: %1</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="547"/>
        <source>Qmmp Version</source>
        <translation>Версия Qmmp</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="564"/>
        <source>User Interfaces</source>
        <translation>Интерфейсы пользователя</translation>
    </message>
</context>
</TS>
