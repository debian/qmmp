<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW">
<context>
    <name>BuiltinCommandLineOption</name>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="46"/>
        <source>Don&apos;t clear the playlist</source>
        <translation>不要清除這個播放清單</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="47"/>
        <source>Start playing current song</source>
        <translation>開始播放目前曲目</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="48"/>
        <source>Pause current song</source>
        <translation>暫停目前曲目</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="49"/>
        <source>Pause if playing, play otherwise</source>
        <translation>正在播放則暫停，相反處於暫停則播放</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="50"/>
        <source>Stop current song</source>
        <translation>停止目前曲目</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="51"/>
        <source>Display Jump to Track dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="52"/>
        <source>Quit application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="53"/>
        <source>Set playback volume (example: qmmp --volume 20)</source>
        <translation>設置回放音量 (例如：qmmp --volume 20)</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="54"/>
        <source>Print volume level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="55"/>
        <source>Mute/Restore volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="56"/>
        <source>Print mute status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="57"/>
        <source>Skip forward in playlist</source>
        <translation>跳到播放清單中的下一曲</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="58"/>
        <source>Skip backwards in playlist</source>
        <translation>跳到播放清單中的上一曲</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="59"/>
        <source>Show/hide application</source>
        <translation>察看/隱藏程式</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="60"/>
        <source>Show main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="61"/>
        <source>Display Add File dialog</source>
        <translation>察看添加文件對話</translation>
    </message>
    <message>
        <location filename="../builtincommandlineoption.cpp" line="62"/>
        <source>Display Add Directory dialog</source>
        <translation>察看添加目錄對話</translation>
    </message>
</context>
<context>
    <name>QMMPStarter</name>
    <message>
        <location filename="../qmmpstarter.cpp" line="149"/>
        <source>Unknown command</source>
        <translation>未知指令</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="508"/>
        <source>Usage: qmmp [options] [files]</source>
        <translation>使用：qmmp [設定] [文件]</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="509"/>
        <source>Options:</source>
        <translation>設定：</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="515"/>
        <source>Start qmmp with the specified user interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="516"/>
        <source>List all available user interfaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="517"/>
        <source>Don&apos;t start the application</source>
        <translation>無法啟動此程式</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="518"/>
        <source>Print debugging messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="519"/>
        <source>Display this text and exit</source>
        <translation>察看這些字檔並結束</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="520"/>
        <source>Print version number and exit</source>
        <translation>察看版本並結束</translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="522"/>
        <source>Home page: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="523"/>
        <source>Development page: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="524"/>
        <source>Bug tracker: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="171"/>
        <location filename="../qmmpstarter.cpp" line="529"/>
        <source>Command Line Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="542"/>
        <source>QMMP version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="543"/>
        <source>Compiled with Qt version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="544"/>
        <source>Using Qt version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="547"/>
        <source>Qmmp Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpstarter.cpp" line="564"/>
        <source>User Interfaces</source>
        <translation>使用者介面</translation>
    </message>
</context>
</TS>
