<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="el">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../forms/aboutdialog.ui" line="14"/>
        <source>About Qmmp</source>
        <translation>Σχετικά με το Qmmp</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="49"/>
        <source>About</source>
        <translation>Σχετικά</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="63"/>
        <source>Authors</source>
        <translation>Συγγραφείς</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="77"/>
        <source>Translators</source>
        <translation>Μεταφραστές</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="91"/>
        <source>Thanks To</source>
        <translation>Ευχαριστίες στους</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="105"/>
        <source>License Agreement</source>
        <translation>Άδεια χρήσης</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="69"/>
        <source>Qt-based Multimedia Player (Qmmp)</source>
        <translation>Αναπαραγωγέας πολυμέσων βασισμένος στη Qt</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="72"/>
        <source>Version: %1</source>
        <translation>Έκδοση: %1</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="79"/>
        <source>(c) %1-%2 Qmmp Development Team</source>
        <translation>(c) %1-%2 Qmmp Ομάδα ανάπτυξης</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="84"/>
        <source>Transports:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="90"/>
        <source>Decoders:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="98"/>
        <source>Engines:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="105"/>
        <source>Effects:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="134"/>
        <source>File dialogs:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="143"/>
        <source>User interfaces:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="126"/>
        <source>Output plugins:</source>
        <translation>Πρόσθετα εξόδου:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="73"/>
        <source>Qt version: %1 (compiled with %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="74"/>
        <source>Qt platform: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="75"/>
        <source>System: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="76"/>
        <source>Build ABI: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="113"/>
        <source>Visual plugins:</source>
        <translation>Οπτικά πρόσθετα:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="120"/>
        <source>General plugins:</source>
        <translation>Γενικά πρόσθετα:</translation>
    </message>
</context>
<context>
    <name>AddUrlDialog</name>
    <message>
        <location filename="../forms/addurldialog.ui" line="14"/>
        <source>Enter URL to add</source>
        <translation>Εισαγάγετε το URL προς προσθήκη</translation>
    </message>
    <message>
        <location filename="../forms/addurldialog.ui" line="55"/>
        <source>&amp;Add</source>
        <translation>&amp;Προσθήκη</translation>
    </message>
    <message>
        <location filename="../forms/addurldialog.ui" line="62"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Ακύρωση</translation>
    </message>
    <message>
        <location filename="../addurldialog.cpp" line="90"/>
        <source>Error</source>
        <translation>Σφάλμα</translation>
    </message>
</context>
<context>
    <name>ColorWidget</name>
    <message>
        <location filename="../colorwidget.cpp" line="46"/>
        <source>Select Color</source>
        <translation>Επιλογή χρώματος</translation>
    </message>
</context>
<context>
    <name>ColumnEditor</name>
    <message>
        <location filename="../forms/columneditor.ui" line="14"/>
        <source>Edit Column</source>
        <translation>Επεξεργασία στήλης</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="36"/>
        <source>Name:</source>
        <translation>Όνομα:</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="76"/>
        <source>Format:</source>
        <translation>Μορφή:</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="64"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="29"/>
        <source>Type:</source>
        <translation>Τύπος:</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="86"/>
        <source>Artist</source>
        <translation>Καλλιτέχνης</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="87"/>
        <source>Album</source>
        <translation>Δίσκος</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="91"/>
        <source>Title</source>
        <translation>Τίτλος</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="94"/>
        <source>Genre</source>
        <translation>Είδος</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="95"/>
        <source>Comment</source>
        <translation>Σχόλιο</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="96"/>
        <source>Composer</source>
        <translation>Συνθέτης</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="97"/>
        <source>Duration</source>
        <translation>Διάρκεια</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="102"/>
        <source>Year</source>
        <translation>Έτος</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="101"/>
        <source>Track Index</source>
        <translation>Δείκτης κομματιού</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="88"/>
        <source>Artist - Album</source>
        <translation>Καλλιτέχνης - Δίσκος</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="89"/>
        <source>Artist - Title</source>
        <translation>Καλλιτέχνης - Τίτλος</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="90"/>
        <source>Album Artist</source>
        <translation>Καλλιτέχνης Δίσκος</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="92"/>
        <source>Track Number</source>
        <translation>Αριθμός κομματιού</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="93"/>
        <source>Two-digit Track Number</source>
        <translation>Διψήφιος αριθμός κομματιού</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="98"/>
        <source>Disc Number</source>
        <translation>Αριθμός δίσκου</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="99"/>
        <source>File Name</source>
        <translation>Όνομα αρχείου</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="100"/>
        <source>File Path</source>
        <translation>Διαδρομή αρχείου</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="103"/>
        <source>Parent Directory Name</source>
        <translation>Όνομα γονικού καταλόγου</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="104"/>
        <source>Parent Directory Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="105"/>
        <source>Custom</source>
        <translation>Προσαρμοσμένο</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../forms/configdialog.ui" line="14"/>
        <source>Qmmp Settings</source>
        <translation>Ρυθμίσεις Qmmp</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="58"/>
        <source>Playlist</source>
        <translation>Λίστα αναπαραγωγής</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="67"/>
        <source>Plugins</source>
        <translation>Πρόσθετα</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="76"/>
        <source>Advanced</source>
        <translation>Προχωρημένα</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="85"/>
        <source>Connectivity</source>
        <translation>Συνδεσιμότητα</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="94"/>
        <location filename="../forms/configdialog.ui" line="996"/>
        <source>Audio</source>
        <translation>Ήχος</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="153"/>
        <source>Metadata</source>
        <translation>Μεταδεδομένα</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="183"/>
        <source>Convert %20 to blanks</source>
        <translation>Μετατροπή %20 σε κενά</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="159"/>
        <source>Load metadata from files</source>
        <translation>Φόρτωση μεταδεδομένων από αρχεία</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="176"/>
        <source>Convert underscores to blanks</source>
        <translation>Μετατροπή κάτω παυλών σε κενά</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="226"/>
        <source>Group format:</source>
        <translation>Μορφή ομαδοποίησης:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="242"/>
        <location filename="../forms/configdialog.ui" line="267"/>
        <location filename="../forms/configdialog.ui" line="710"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="169"/>
        <source>Read tags while loading a playlist</source>
        <translation>Ανάγνωση των ετικετών κατά την φόρτωση μιας λίστας αναπαραγωγής</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="193"/>
        <source>Group Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="202"/>
        <source>Group size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="219"/>
        <source>Show dividing line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="251"/>
        <source>Extra row format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="276"/>
        <source>Show extra row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="283"/>
        <source>Show cover</source>
        <translation>Εμφάνιση εξώφυλλου</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="293"/>
        <source>Directory Scanning Options</source>
        <translation>Επιλογές σάρωσης καταλόγων</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="299"/>
        <source>Restrict files to:</source>
        <translation>Περιορισμός των αρχείων σε:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="313"/>
        <location filename="../forms/configdialog.ui" line="588"/>
        <source>Exclude files:</source>
        <translation>Εξαίρεση αρχείων:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="330"/>
        <source>Miscellaneous</source>
        <translation>Διάφορα</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="336"/>
        <source>Auto-save playlist when modified</source>
        <translation>Αυτόματη αποθήκευση της λίστας αναπαραγωγής όταν τροποποιείται</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="343"/>
        <source>Clear previous playlist when opening new one</source>
        <translation>Καθαρισμός της προηγούμενης λίστας αναπαραγωγής κατά το άνοιγμα μιας νέας</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="396"/>
        <location filename="../configdialog.cpp" line="341"/>
        <source>Preferences</source>
        <translation>Προτιμήσεις</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="413"/>
        <location filename="../configdialog.cpp" line="344"/>
        <source>Information</source>
        <translation>Πληροφορίες</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="459"/>
        <source>Description</source>
        <translation>Περιγραφή</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="464"/>
        <source>Filename</source>
        <translation>Όνομα αρχείου</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="476"/>
        <source>Look and Feel</source>
        <translation>Όψη και αίσθηση</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="482"/>
        <source>Language:</source>
        <translation>Γλώσσα:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="515"/>
        <source>Display average bitrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="525"/>
        <source>Playback</source>
        <translation>Αναπαραγωγή</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="531"/>
        <source>Continue playback on startup</source>
        <translation>Συνέχιση της αναπαραγωγής κατά την εκκίνηση</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="538"/>
        <source>Determine file type by content</source>
        <translation>Προσδιορισμός του τύπου του αρχείου βάσει περιεχομένου</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="545"/>
        <source>Add files from command line to this playlist:</source>
        <translation>Προσθήκη αρχείων στη λίστα αναπαραγωγής από τη γραμμή εντολών:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="562"/>
        <source>Cover Image Retrieve</source>
        <translation>Λήψη εικόνας εξώφυλλου</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="568"/>
        <source>Use separate image files</source>
        <translation>Χρήση ξεχωριστών αρχείων εικόνων</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="578"/>
        <source>Include files:</source>
        <translation>Συμπερίληψη των αρχείων:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="600"/>
        <source>Recursive search depth:</source>
        <translation>Βάθος αναδρομικής αναζήτησης:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="632"/>
        <source>URL Dialog</source>
        <translation>Διάλογος URL</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="638"/>
        <source>Auto-paste URL from clipboard</source>
        <translation>Αυτόματη επικόλληση του URL από το πρόχειρο</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="648"/>
        <source>CUE Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="654"/>
        <source>Use system font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="675"/>
        <source>Font:</source>
        <translation>Γραμματοσειρά:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="700"/>
        <source>???</source>
        <translation>;;;</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="739"/>
        <source>Proxy</source>
        <translation>Διαμεσολαβητής</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="751"/>
        <source>Enable proxy usage</source>
        <translation>Ενεργοποίηση της χρήσης διαμεσολαβητή</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="758"/>
        <source>Proxy type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="768"/>
        <source>Proxy host name:</source>
        <translation>Όνομα υπολογιστή του διαμεσολαβητή:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="785"/>
        <source>Proxy port:</source>
        <translation>Θύρα διαμεσολαβητή:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="802"/>
        <source>Use authentication with proxy</source>
        <translation>Χρήση ταυτοποίησης με τον διαμεσολαβητή</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="809"/>
        <source>Proxy user name:</source>
        <translation>Όνομα χρήστη του διαμεσολαβητή:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="826"/>
        <source>Proxy password:</source>
        <translation>Κωδικός πρόσβασης διαμεσολαβητή:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="866"/>
        <source>Replay Gain</source>
        <translation>Κανονικοποίηση ακουστότητας</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="872"/>
        <source>Replay Gain mode:</source>
        <translation>Λειτουργία κανονικοποίησης ακουστότητας:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="889"/>
        <source>Preamp:</source>
        <translation>Προενίσχυση:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="921"/>
        <location filename="../forms/configdialog.ui" line="966"/>
        <source>dB</source>
        <translation>dB</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="934"/>
        <source>Default gain:</source>
        <translation>Εξ ορισμού ενίσχυση:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="986"/>
        <source>Use  peak info to prevent clipping</source>
        <translation>Χρήση αιχμών για την αποτροπή διακοπών</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1002"/>
        <source>Buffer size:</source>
        <translation>Μέγεθος ενδιάμεσης μνήμης:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1028"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1061"/>
        <source>Use software volume control</source>
        <translation>Χρήση ελέγχου έντασης μέσω λογισμικού</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1101"/>
        <source>Use two passes for equalizer</source>
        <translation>Χρήση δυο περασμάτων για τον ισοσταθμιστή</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1044"/>
        <source>Volume adjustment step:</source>
        <translation>Βήμα προσαρμογής έντασης:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="350"/>
        <source>Skip already existing tracks when adding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="357"/>
        <source>Stop playback after removing of current track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1068"/>
        <source>Output bit depth:</source>
        <translation>Βάθος bit εξόδου:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1009"/>
        <source>Use dithering</source>
        <translation>Χρήση αμφιταλάντευσης</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="63"/>
        <source>1 row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="64"/>
        <source>3 rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="65"/>
        <source>4 rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="66"/>
        <source>5 rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="67"/>
        <source>Track</source>
        <translation>Κομμάτι</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="68"/>
        <source>Album</source>
        <translation>Δίσκος</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="69"/>
        <source>Disabled</source>
        <translation>Απενεργοποιημένο</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="76"/>
        <source>HTTP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="77"/>
        <source>SOCKS5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="204"/>
        <source>Transports</source>
        <translation>Πρωτόκολλο μεταφοράς</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="215"/>
        <source>Decoders</source>
        <translation>Αποκωδικοποιητές</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="226"/>
        <source>Engines</source>
        <translation>Μηχανές</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="238"/>
        <source>Effects</source>
        <translation>Τεχνάσματα εντυπωσιασμού</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="249"/>
        <source>Visualization</source>
        <translation>Οπτικοποίηση</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="261"/>
        <source>General</source>
        <translation>Γενικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="272"/>
        <source>Output</source>
        <translation>Έξοδος</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="283"/>
        <source>File Dialogs</source>
        <translation>Διάλογοι αρχείων</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="295"/>
        <source>User Interfaces</source>
        <translation>Περιβάλλοντα χρήστη</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="354"/>
        <source>&lt;Autodetect&gt;</source>
        <translation>&lt;αυτόματο&gt;</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="355"/>
        <source>Brazilian Portuguese</source>
        <translation>Πορτογαλικά Βραζιλίας</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="356"/>
        <source>Chinese Simplified</source>
        <translation>Κινέζικα απλά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="357"/>
        <source>Chinese Traditional</source>
        <translation>Κινέζικα παραδοσιακά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="358"/>
        <source>Czech</source>
        <translation>Τσέχικα</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="359"/>
        <source>Dutch</source>
        <translation>Ολλανδικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="360"/>
        <source>English</source>
        <translation>Αγγλικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="361"/>
        <source>French</source>
        <translation>Γαλλικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="362"/>
        <source>Galician</source>
        <translation>Γαλικιανά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="363"/>
        <source>German</source>
        <translation>Γερμανικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="364"/>
        <source>Greek</source>
        <translation>Ελληνικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="365"/>
        <source>Hebrew</source>
        <translation>Εβραϊκά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="366"/>
        <source>Hungarian</source>
        <translation>Ουγγρικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="367"/>
        <source>Indonesian</source>
        <translation>Ινδονησιακά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="368"/>
        <source>Italian</source>
        <translation>Ιταλικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="369"/>
        <source>Japanese</source>
        <translation>Ιαπωνικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="370"/>
        <source>Kazakh</source>
        <translation>Καζακικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="371"/>
        <source>Korean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="372"/>
        <source>Lithuanian</source>
        <translation>Λιθουανικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="373"/>
        <source>Polish</source>
        <translation>Πολωνικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="374"/>
        <source>Portuguese</source>
        <translation>Πορτογαλικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="375"/>
        <source>Russian</source>
        <translation>Ρωσικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="376"/>
        <source>Serbian</source>
        <translation>Σερβικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="377"/>
        <source>Slovak</source>
        <translation>Σλοβακικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="378"/>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="379"/>
        <source>Spanish</source>
        <translation>Ισπανικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="380"/>
        <source>Turkish</source>
        <translation>Τουρκικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="381"/>
        <source>Ukrainian</source>
        <translation>Ουκρανικά</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="382"/>
        <source>Serbian (Ijekavian)</source>
        <translation>Σερβικά Ijekavian</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="383"/>
        <source>Serbian (Ekavian)</source>
        <translation>Σερβικά (Ekavian)</translation>
    </message>
</context>
<context>
    <name>CoverEditor</name>
    <message>
        <location filename="../forms/covereditor.ui" line="22"/>
        <source>Image source:</source>
        <translation>Πηγή εικόνας:</translation>
    </message>
    <message>
        <location filename="../forms/covereditor.ui" line="76"/>
        <source>Load</source>
        <translation>Φόρτωση</translation>
    </message>
    <message>
        <location filename="../forms/covereditor.ui" line="83"/>
        <source>Delete</source>
        <translation>Διαγραφή</translation>
    </message>
    <message>
        <location filename="../forms/covereditor.ui" line="90"/>
        <source>Save as...</source>
        <translation>Αποθήκευση ως...</translation>
    </message>
    <message>
        <location filename="../covereditor.cpp" line="34"/>
        <source>External file</source>
        <translation>Εξωτερικό αρχείο</translation>
    </message>
    <message>
        <location filename="../covereditor.cpp" line="35"/>
        <source>Tag</source>
        <translation>Ετικέτα</translation>
    </message>
</context>
<context>
    <name>CoverViewer</name>
    <message>
        <location filename="../coverviewer.cpp" line="35"/>
        <source>&amp;Save As...</source>
        <translation>&amp;Αποθήκευση ως...</translation>
    </message>
    <message>
        <location filename="../coverviewer.cpp" line="68"/>
        <source>Save Cover As</source>
        <translation>Αποθήκευση εξώφυλλου ως</translation>
    </message>
    <message>
        <location filename="../coverviewer.cpp" line="70"/>
        <location filename="../coverviewer.cpp" line="83"/>
        <source>Images</source>
        <translation>Εικόνες</translation>
    </message>
    <message>
        <location filename="../coverviewer.cpp" line="81"/>
        <source>Open Image</source>
        <translation>Άνοιγμα εικόνας</translation>
    </message>
</context>
<context>
    <name>CueEditor</name>
    <message>
        <location filename="../forms/cueeditor.ui" line="40"/>
        <source>Load</source>
        <translation>Φόρτωση</translation>
    </message>
    <message>
        <location filename="../forms/cueeditor.ui" line="47"/>
        <source>Delete</source>
        <translation>Διαγραφή</translation>
    </message>
    <message>
        <location filename="../forms/cueeditor.ui" line="54"/>
        <source>Save as...</source>
        <translation>Αποθήκευση ως...</translation>
    </message>
    <message>
        <location filename="../cueeditor.cpp" line="131"/>
        <source>Open CUE File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cueeditor.cpp" line="131"/>
        <location filename="../cueeditor.cpp" line="150"/>
        <source>CUE Files</source>
        <translation>Αρχεία CUE</translation>
    </message>
    <message>
        <location filename="../cueeditor.cpp" line="148"/>
        <source>Save CUE File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailsDialog</name>
    <message>
        <location filename="../forms/detailsdialog.ui" line="14"/>
        <source>Details</source>
        <translation>Λεπτομέρειες</translation>
    </message>
    <message>
        <location filename="../forms/detailsdialog.ui" line="44"/>
        <source>Open the directory containing this file</source>
        <translation>Ανοίξτε το αρχείο που περιέχει το αρχείο</translation>
    </message>
    <message>
        <location filename="../forms/detailsdialog.ui" line="47"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/detailsdialog.ui" line="63"/>
        <source>Summary</source>
        <translation>Σύνοψη</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="207"/>
        <source>%1/%2</source>
        <translation>%1/%2</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="257"/>
        <source>Cover</source>
        <translation>Εξώφυλλο</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="283"/>
        <source>Lyrics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="303"/>
        <source>Title</source>
        <translation>Τίτλος</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="304"/>
        <source>Artist</source>
        <translation>Καλλιτέχνης</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="305"/>
        <source>Album artist</source>
        <translation>Καλλιτέχνης δίσκος</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="306"/>
        <source>Album</source>
        <translation>Δίσκος</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="307"/>
        <source>Comment</source>
        <translation>Σχόλιο</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="308"/>
        <source>Genre</source>
        <translation>Είδος</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="309"/>
        <source>Composer</source>
        <translation>Συνθέτης</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="310"/>
        <source>Year</source>
        <translation>Έτος</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="311"/>
        <source>Track</source>
        <translation>Κομμάτι</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="312"/>
        <source>Disc number</source>
        <translation>Αριθμός δίσκου</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="331"/>
        <source>Duration</source>
        <translation>Διάρκεια</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="334"/>
        <source>Bitrate</source>
        <translation>Ρυθμός bit</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="334"/>
        <source>kbps</source>
        <translation>kbps</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="335"/>
        <source>Sample rate</source>
        <translation>Ρυθμός δειγματοληψίας</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="335"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="336"/>
        <source>Channels</source>
        <translation>Κανάλια</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="337"/>
        <source>Sample size</source>
        <translation>Μέγεθος δειγματοληψίας</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="337"/>
        <source>bits</source>
        <translation>bit</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="338"/>
        <source>Format name</source>
        <translation>Όνομα τύπου</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="339"/>
        <source>File size</source>
        <translation>Μέγεθος αρχείου</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="339"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="383"/>
        <source>Yes</source>
        <translation>Ναι</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="383"/>
        <source>No</source>
        <translation>Όχι</translation>
    </message>
</context>
<context>
    <name>JumpToTrackDialog</name>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="14"/>
        <source>Jump To Track</source>
        <translation>Μεταπήδηση σε κομμάτι</translation>
    </message>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="46"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="85"/>
        <location filename="../jumptotrackdialog.cpp" line="120"/>
        <location filename="../jumptotrackdialog.cpp" line="151"/>
        <source>Queue</source>
        <translation>Ουρά αναμονής</translation>
    </message>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="105"/>
        <source>Jump To</source>
        <translation>Μεταπήδηση σε</translation>
    </message>
    <message>
        <location filename="../jumptotrackdialog.cpp" line="95"/>
        <source>Q</source>
        <translation>Q</translation>
    </message>
    <message>
        <location filename="../jumptotrackdialog.cpp" line="96"/>
        <source>J</source>
        <translation>J</translation>
    </message>
    <message>
        <location filename="../jumptotrackdialog.cpp" line="118"/>
        <location filename="../jumptotrackdialog.cpp" line="149"/>
        <source>Unqueue</source>
        <translation>Αφαίρεση από την ουρά αναμονής</translation>
    </message>
</context>
<context>
    <name>MetaDataFormatterMenu</name>
    <message>
        <location filename="../metadataformattermenu.cpp" line="27"/>
        <source>Artist</source>
        <translation>Καλλιτέχνης</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="28"/>
        <source>Album</source>
        <translation>Δίσκος</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="29"/>
        <source>Album Artist</source>
        <translation>Καλλιτέχνης δίσκος</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="32"/>
        <source>Title</source>
        <translation>Τίτλος</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="33"/>
        <source>Track Number</source>
        <translation>Αριθμός κομματιού</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="34"/>
        <source>Two-digit Track Number</source>
        <translation>Διψήφιος αριθμός κομματιού</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="38"/>
        <source>Track Index</source>
        <translation>Δείκτης κομματιού</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="40"/>
        <source>Genre</source>
        <translation>Είδος</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="41"/>
        <source>Comment</source>
        <translation>Σχόλιο</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="42"/>
        <source>Composer</source>
        <translation>Συνθέτης</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="47"/>
        <location filename="../metadataformattermenu.cpp" line="61"/>
        <source>Duration</source>
        <translation>Διάρκεια</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="55"/>
        <source>Artist - Album</source>
        <translation>Καλλιτέχνης - Δίσκος</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="62"/>
        <source>Duration | Format | Bitrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="63"/>
        <source>Duration | Format | Bitrate | Sample rate </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="64"/>
        <source>Year | Duration | Bitrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="68"/>
        <source>Parent Directory Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="70"/>
        <source>Bitrate</source>
        <translation>Ρυθμός bit</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="71"/>
        <source>Sample Rate</source>
        <translation>Ρυθμός δειγματοληψίας</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="72"/>
        <source>Number of Channels</source>
        <translation>Αριθμός καναλιών</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="73"/>
        <source>Sample Size</source>
        <translation>Μέγεθος δειγματοληψίας</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="74"/>
        <source>Format</source>
        <translation>Μορφή</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="75"/>
        <source>Decoder</source>
        <translation>Αποκωδικοποιητής</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="78"/>
        <source>File Size</source>
        <translation>Μέγεθος αρχείου</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="43"/>
        <source>Disc Number</source>
        <translation>Αριθμός δίσκου</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="48"/>
        <source>File Name</source>
        <translation>Όνομα αρχείου</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="49"/>
        <source>File Path</source>
        <translation>Διαδρομή αρχείου</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="44"/>
        <source>Year</source>
        <translation>Έτος</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="51"/>
        <location filename="../metadataformattermenu.cpp" line="57"/>
        <location filename="../metadataformattermenu.cpp" line="65"/>
        <source>Condition</source>
        <translation>Συνθήκη</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="50"/>
        <source>Artist - Title</source>
        <translation>Καλλιτέχνης - Τίτλος</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="56"/>
        <source>Artist - [Year] Album</source>
        <translation>Καλλιτέχνης - [Έτος] Δίσκος</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="62"/>
        <source>%if(%l,%l | ,)%{format} | %{bitrate} kbps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="63"/>
        <source>%if(%l,%l | ,)%{format} | %{bitrate} kbps | %{samplerate} Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="64"/>
        <source>%y | %if(%l,%l | ,)%{bitrate} kbps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="67"/>
        <source>Parent Directory Name</source>
        <translation>Όνομα γονικού καταλόγου</translation>
    </message>
</context>
<context>
    <name>PlayListDownloader</name>
    <message>
        <location filename="../playlistdownloader.cpp" line="123"/>
        <source>Unsupported playlist format</source>
        <translation>Μη υποστηριζόμενη μορφή λίστας αναπαραγωγής</translation>
    </message>
</context>
<context>
    <name>PlayListHeaderModel</name>
    <message>
        <location filename="../playlistheadermodel.cpp" line="35"/>
        <source>Artist - Title</source>
        <translation>Καλλιτέχνης - Τίτλος</translation>
    </message>
    <message>
        <location filename="../playlistheadermodel.cpp" line="185"/>
        <source>Title</source>
        <translation>Τίτλος</translation>
    </message>
    <message>
        <location filename="../playlistheadermodel.cpp" line="186"/>
        <source>Add Column</source>
        <translation>Προσθήκη στήλης</translation>
    </message>
</context>
<context>
    <name>PlayListManager</name>
    <message>
        <location filename="../playlistmanager.cpp" line="177"/>
        <location filename="../playlistmanager.cpp" line="319"/>
        <source>Playlist</source>
        <translation>Λίστα αναπαραγωγής</translation>
    </message>
</context>
<context>
    <name>PlayListTrack</name>
    <message>
        <location filename="../playlisttrack.cpp" line="245"/>
        <source>Streams</source>
        <translation>Ροές</translation>
    </message>
    <message>
        <location filename="../playlisttrack.cpp" line="250"/>
        <source>Empty group</source>
        <translation>Κενή ομάδα</translation>
    </message>
</context>
<context>
    <name>QmmpUiSettings</name>
    <message>
        <location filename="../qmmpuisettings.cpp" line="39"/>
        <source>%if(%l,%l | ,)%{format} | %{bitrate} kbps | %{samplerate} Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpuisettings.cpp" line="64"/>
        <source>Playlist</source>
        <translation>Λίστα αναπαραγωγής</translation>
    </message>
</context>
<context>
    <name>QtFileDialogFactory</name>
    <message>
        <location filename="../qtfiledialog.cpp" line="35"/>
        <source>Qt File Dialog</source>
        <translation>Διάλογος αρχείων Qt</translation>
    </message>
</context>
<context>
    <name>ShortcutDialog</name>
    <message>
        <location filename="../forms/shortcutdialog.ui" line="14"/>
        <source>Change Shortcut</source>
        <translation>Αλλαγή συντόμευσης</translation>
    </message>
    <message>
        <location filename="../forms/shortcutdialog.ui" line="29"/>
        <source>Press the key combination you want to assign</source>
        <translation>Πιέστε το συνδυασμό πλήκτρων που θέλετε να αναθέσετε</translation>
    </message>
    <message>
        <location filename="../forms/shortcutdialog.ui" line="52"/>
        <source>Clear</source>
        <translation>Καθαρισμός</translation>
    </message>
</context>
<context>
    <name>TagEditor</name>
    <message>
        <location filename="../forms/tageditor.ui" line="14"/>
        <source>Tag Editor</source>
        <translation>Επεξεργαστής ετικετών</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="38"/>
        <source>Title:</source>
        <translation>Τίτλος:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="64"/>
        <source>Artist:</source>
        <translation>Καλλιτέχνης:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="90"/>
        <source>Album:</source>
        <translation>Δίσκος:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="113"/>
        <source>Album artist:</source>
        <translation>Καλλιτέχνης δίσκος:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="123"/>
        <source>Composer:</source>
        <translation>Συνθέτης:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="143"/>
        <source>Genre:</source>
        <translation>Είδος:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="169"/>
        <source>Track:</source>
        <translation>Κομμάτι:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="190"/>
        <location filename="../forms/tageditor.ui" line="228"/>
        <location filename="../forms/tageditor.ui" line="260"/>
        <source>?</source>
        <translation>;</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="203"/>
        <source>Year:</source>
        <translation>Έτος:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="250"/>
        <source>Disc number:</source>
        <translation>Αριθμός δίσκου:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="275"/>
        <source>Comment:</source>
        <translation>Σχόλιο:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="311"/>
        <source>Include selected tag in file</source>
        <translation>Συμπερίληψη των επιλεγμένων ετικετών στο αρχείο</translation>
    </message>
</context>
<context>
    <name>TemplateEditor</name>
    <message>
        <location filename="../forms/templateeditor.ui" line="14"/>
        <source>Template Editor</source>
        <translation>Επεξεργαστής πρότυπων</translation>
    </message>
    <message>
        <location filename="../forms/templateeditor.ui" line="39"/>
        <source>Reset</source>
        <translation>Επαναφορά</translation>
    </message>
    <message>
        <location filename="../forms/templateeditor.ui" line="46"/>
        <source>Insert</source>
        <translation>Εισαγωγή</translation>
    </message>
</context>
<context>
    <name>UiHelper</name>
    <message>
        <location filename="../uihelper.cpp" line="136"/>
        <location filename="../uihelper.cpp" line="148"/>
        <source>All Supported Bitstreams</source>
        <translation>Όλα τα υποστηριζόμενα δυφιορρεύματα</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="142"/>
        <source>Select one or more files to open</source>
        <translation>Επιλογή ανοίγματος ενός ή περισσοτέρων αρχείων</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="154"/>
        <source>Select one or more files to play</source>
        <translation>Επιλογή αναπαραγωγής ενός ή περισσοτέρων αρχείων</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="162"/>
        <source>Choose a directory</source>
        <translation>Επιλογή ενός καταλόγου</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="178"/>
        <location filename="../uihelper.cpp" line="202"/>
        <source>Playlist Files</source>
        <translation>Αρχεία της λίστα αναπαραγωγής</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="180"/>
        <source>Open Playlist</source>
        <translation>Άνοιγμα λίστα αναπαραγωγής</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="205"/>
        <location filename="../uihelper.cpp" line="224"/>
        <source>Save Playlist</source>
        <translation>Αποθήκευση της λίστας αναπαραγωγής</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="224"/>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation>Το %1 υπάρχει ήδη.
Επιθυμείτε να το αντικαταστήσετε;</translation>
    </message>
</context>
<context>
    <name>VisualMenu</name>
    <message>
        <location filename="../visualmenu.cpp" line="26"/>
        <source>Visualization</source>
        <translation>Οπτικοποίηση</translation>
    </message>
</context>
<context>
    <name>WinFileAssocPage</name>
    <message>
        <location filename="../forms/winfileassocpage.ui" line="43"/>
        <source>Media files handled by Qmmp:</source>
        <translation>Αρχεία πολυμέσων που υποστηρίζονται από το Qmmp:</translation>
    </message>
    <message>
        <location filename="../forms/winfileassocpage.ui" line="17"/>
        <source>Select All</source>
        <translation>Επιλογή όλων</translation>
    </message>
</context>
</TS>
