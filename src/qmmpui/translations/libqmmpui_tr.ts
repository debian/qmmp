<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../forms/aboutdialog.ui" line="14"/>
        <source>About Qmmp</source>
        <translation>Qmmp Hakkında</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="49"/>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="63"/>
        <source>Authors</source>
        <translation>Yazarlar</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="77"/>
        <source>Translators</source>
        <translation>Çevirmenler</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="91"/>
        <source>Thanks To</source>
        <translation>Teşekkürler</translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="105"/>
        <source>License Agreement</source>
        <translation>Lisans Anlaşması</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="69"/>
        <source>Qt-based Multimedia Player (Qmmp)</source>
        <translation>Qt tabanlı Çokluortam Oynatıcısı (Qmmp)</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="72"/>
        <source>Version: %1</source>
        <translation>Uyarlama: %1</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="79"/>
        <source>(c) %1-%2 Qmmp Development Team</source>
        <translation>(c) %1-%2 Qmmp Geliştirme Takımı</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="84"/>
        <source>Transports:</source>
        <translation>Taşımalar:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="90"/>
        <source>Decoders:</source>
        <translation>Kodlayıcılar:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="98"/>
        <source>Engines:</source>
        <translation>Motorlar:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="105"/>
        <source>Effects:</source>
        <translation>Efektler:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="134"/>
        <source>File dialogs:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="143"/>
        <source>User interfaces:</source>
        <translation>Kullanıcı arabirimleri:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="126"/>
        <source>Output plugins:</source>
        <translation>Çıktı eklentileri:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="73"/>
        <source>Qt version: %1 (compiled with %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="74"/>
        <source>Qt platform: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="75"/>
        <source>System: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="76"/>
        <source>Build ABI: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="113"/>
        <source>Visual plugins:</source>
        <translation>Görsel eklentiler:</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="120"/>
        <source>General plugins:</source>
        <translation>Genel eklentiler:</translation>
    </message>
</context>
<context>
    <name>AddUrlDialog</name>
    <message>
        <location filename="../forms/addurldialog.ui" line="14"/>
        <source>Enter URL to add</source>
        <translation>Eklenecek URL&apos;yi girin</translation>
    </message>
    <message>
        <location filename="../forms/addurldialog.ui" line="55"/>
        <source>&amp;Add</source>
        <translation>&amp;Ekle</translation>
    </message>
    <message>
        <location filename="../forms/addurldialog.ui" line="62"/>
        <source>&amp;Cancel</source>
        <translation>&amp;İptal</translation>
    </message>
    <message>
        <location filename="../addurldialog.cpp" line="90"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
</context>
<context>
    <name>ColorWidget</name>
    <message>
        <location filename="../colorwidget.cpp" line="46"/>
        <source>Select Color</source>
        <translation>Renk Seçin</translation>
    </message>
</context>
<context>
    <name>ColumnEditor</name>
    <message>
        <location filename="../forms/columneditor.ui" line="14"/>
        <source>Edit Column</source>
        <translation>Sütun Düzenle</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="36"/>
        <source>Name:</source>
        <translation>Ad:</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="76"/>
        <source>Format:</source>
        <translation>Biçim:</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="64"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/columneditor.ui" line="29"/>
        <source>Type:</source>
        <translation>Tür:</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="86"/>
        <source>Artist</source>
        <translation>Sanatçı</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="87"/>
        <source>Album</source>
        <translation>Albüm</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="91"/>
        <source>Title</source>
        <translation>Başlık</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="94"/>
        <source>Genre</source>
        <translation>Tarz</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="95"/>
        <source>Comment</source>
        <translation>Yorum</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="96"/>
        <source>Composer</source>
        <translation>Besteci</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="97"/>
        <source>Duration</source>
        <translation>Süre</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="102"/>
        <source>Year</source>
        <translation>Yıl</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="101"/>
        <source>Track Index</source>
        <translation>Parça Fihristi</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="88"/>
        <source>Artist - Album</source>
        <translation>Sanatçı - Albüm</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="89"/>
        <source>Artist - Title</source>
        <translation>Sanatçı - Başlık</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="90"/>
        <source>Album Artist</source>
        <translation>Albüm Sanatçısı</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="92"/>
        <source>Track Number</source>
        <translation>Parça Numarası</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="93"/>
        <source>Two-digit Track Number</source>
        <translation>İki-rakam Parça Numarası</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="98"/>
        <source>Disc Number</source>
        <translation>Disk Numarası</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="99"/>
        <source>File Name</source>
        <translation>Dosya Adı</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="100"/>
        <source>File Path</source>
        <translation>Dosya Yolu</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="103"/>
        <source>Parent Directory Name</source>
        <translation>Ana Dizin Adı</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="104"/>
        <source>Parent Directory Path</source>
        <translation>Ana Dizin Yolu</translation>
    </message>
    <message>
        <location filename="../columneditor.cpp" line="105"/>
        <source>Custom</source>
        <translation>Özel</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../forms/configdialog.ui" line="14"/>
        <source>Qmmp Settings</source>
        <translation>Qmmp Ayarları</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="58"/>
        <source>Playlist</source>
        <translation>Çalma Listesi</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="67"/>
        <source>Plugins</source>
        <translation>Eklentiler</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="76"/>
        <source>Advanced</source>
        <translation>Gelişmiş</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="85"/>
        <source>Connectivity</source>
        <translation>Bağlanırlık</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="94"/>
        <location filename="../forms/configdialog.ui" line="996"/>
        <source>Audio</source>
        <translation>Ses</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="153"/>
        <source>Metadata</source>
        <translation>Veri bilgisi</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="183"/>
        <source>Convert %20 to blanks</source>
        <translation>%20 yi boşluğa çevir</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="159"/>
        <source>Load metadata from files</source>
        <translation>Veri bilgisini dosyadan yükle</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="176"/>
        <source>Convert underscores to blanks</source>
        <translation>Alt çizgileri boşluğa çevir</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="226"/>
        <source>Group format:</source>
        <translation>Grup biçimi:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="242"/>
        <location filename="../forms/configdialog.ui" line="267"/>
        <location filename="../forms/configdialog.ui" line="710"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="169"/>
        <source>Read tags while loading a playlist</source>
        <translation>Bir çalma listesi yüklerken etiketleri oku</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="193"/>
        <source>Group Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="202"/>
        <source>Group size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="219"/>
        <source>Show dividing line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="251"/>
        <source>Extra row format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="276"/>
        <source>Show extra row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="283"/>
        <source>Show cover</source>
        <translation>Kapağı göster</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="293"/>
        <source>Directory Scanning Options</source>
        <translation>Dizin Tarama Seçenekleri</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="299"/>
        <source>Restrict files to:</source>
        <translation>Dosyaları kısıtla:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="313"/>
        <location filename="../forms/configdialog.ui" line="588"/>
        <source>Exclude files:</source>
        <translation>Dosyaları hariç tut:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="330"/>
        <source>Miscellaneous</source>
        <translation>Çeşitli</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="336"/>
        <source>Auto-save playlist when modified</source>
        <translation>Değiştirildiği zaman çalma listesini otomatik kaydet</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="343"/>
        <source>Clear previous playlist when opening new one</source>
        <translation>Yeni birini açarken önceki çalma listesini temizle</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="396"/>
        <location filename="../configdialog.cpp" line="341"/>
        <source>Preferences</source>
        <translation>Tercihler</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="413"/>
        <location filename="../configdialog.cpp" line="344"/>
        <source>Information</source>
        <translation>Bilgi</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="459"/>
        <source>Description</source>
        <translation>Açıklama</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="464"/>
        <source>Filename</source>
        <translation>Dosya adı</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="476"/>
        <source>Look and Feel</source>
        <translation>Bak ve Hisset</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="482"/>
        <source>Language:</source>
        <translation>Dil:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="515"/>
        <source>Display average bitrate</source>
        <translation>Ortalama bit hızını sergile</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="525"/>
        <source>Playback</source>
        <translation>Kayıttan Çal</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="531"/>
        <source>Continue playback on startup</source>
        <translation>Başlangıçta kayıttan çalmaya devam ettir</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="538"/>
        <source>Determine file type by content</source>
        <translation>İçeriğe göre dosya türünü belirleyin</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="545"/>
        <source>Add files from command line to this playlist:</source>
        <translation>Bu çalma listesine komut satırından dosya ekleyin:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="562"/>
        <source>Cover Image Retrieve</source>
        <translation>Kapak Resmini Al</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="568"/>
        <source>Use separate image files</source>
        <translation>Ayrıştırılmış görüntü dosyaları kullanın</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="578"/>
        <source>Include files:</source>
        <translation>Dosyaları dahil et:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="600"/>
        <source>Recursive search depth:</source>
        <translation>Özyinelemeli arama derinliği:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="632"/>
        <source>URL Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="638"/>
        <source>Auto-paste URL from clipboard</source>
        <translation>URL&apos;yi panodan otomatik yapıştır</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="648"/>
        <source>CUE Editor</source>
        <translation>CUE Düzenleyici</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="654"/>
        <source>Use system font</source>
        <translation>Sistem yazı tipini kullan</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="675"/>
        <source>Font:</source>
        <translation>Шрифт:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="700"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="739"/>
        <source>Proxy</source>
        <translation>Vekil sunucu</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="751"/>
        <source>Enable proxy usage</source>
        <translation>Vekil sunucu kullanımını etkinleştir</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="758"/>
        <source>Proxy type:</source>
        <translation>Vekil sunucu türü:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="768"/>
        <source>Proxy host name:</source>
        <translation>Vekil sunucu adı:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="785"/>
        <source>Proxy port:</source>
        <translation>Vekil sunucu portu:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="802"/>
        <source>Use authentication with proxy</source>
        <translation>Vekil sunucu yetkilendirmesi kullan</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="809"/>
        <source>Proxy user name:</source>
        <translation>Vekil sunucu kullanıcı adı:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="826"/>
        <source>Proxy password:</source>
        <translation>Vekil sunucu parolası:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="866"/>
        <source>Replay Gain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="872"/>
        <source>Replay Gain mode:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="889"/>
        <source>Preamp:</source>
        <translation>Önamfi:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="921"/>
        <location filename="../forms/configdialog.ui" line="966"/>
        <source>dB</source>
        <translation>dB</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="934"/>
        <source>Default gain:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="986"/>
        <source>Use  peak info to prevent clipping</source>
        <translation>Kırpmayı önlemek için en yüksek bilgiyi kullanın</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1002"/>
        <source>Buffer size:</source>
        <translation>Tampon boyutu:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1028"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1061"/>
        <source>Use software volume control</source>
        <translation>Yazılımsal ses kontrolünü kullan</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1101"/>
        <source>Use two passes for equalizer</source>
        <translation>Ekolayzır için iki geçiş kullanın</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1044"/>
        <source>Volume adjustment step:</source>
        <translation>Ses ayarlama adımı:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="350"/>
        <source>Skip already existing tracks when adding</source>
        <translation>Eklerken zaten var olan parçaları atla</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="357"/>
        <source>Stop playback after removing of current track</source>
        <translation>Geçerli parçayı kaldırdıktan sonra oynatmayı durdur</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1068"/>
        <source>Output bit depth:</source>
        <translation>Çıkış bit derinliği:</translation>
    </message>
    <message>
        <location filename="../forms/configdialog.ui" line="1009"/>
        <source>Use dithering</source>
        <translation>Titreme kullanın</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="63"/>
        <source>1 row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="64"/>
        <source>3 rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="65"/>
        <source>4 rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="66"/>
        <source>5 rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="67"/>
        <source>Track</source>
        <translation>Parça</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="68"/>
        <source>Album</source>
        <translation>Albüm</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="69"/>
        <source>Disabled</source>
        <translation>Etkisiz</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="76"/>
        <source>HTTP</source>
        <translation>HTTP</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="77"/>
        <source>SOCKS5</source>
        <translation>SOCKS5</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="204"/>
        <source>Transports</source>
        <translation>Taşımalar</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="215"/>
        <source>Decoders</source>
        <translation>Kodlayıcılar</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="226"/>
        <source>Engines</source>
        <translation>Motorlar</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="238"/>
        <source>Effects</source>
        <translation>Efektler</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="249"/>
        <source>Visualization</source>
        <translation>Görsellik</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="261"/>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="272"/>
        <source>Output</source>
        <translation>Çıktı</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="283"/>
        <source>File Dialogs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="295"/>
        <source>User Interfaces</source>
        <translation>Kullanıcı Arayüzleri</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="354"/>
        <source>&lt;Autodetect&gt;</source>
        <translation>&lt;Autodetect&gt;</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="355"/>
        <source>Brazilian Portuguese</source>
        <translation>Brezilya Portekizcesi</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="356"/>
        <source>Chinese Simplified</source>
        <translation>Çince Basitleştirilmiş</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="357"/>
        <source>Chinese Traditional</source>
        <translation>Çince Ticari</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="358"/>
        <source>Czech</source>
        <translation>Çekçe</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="359"/>
        <source>Dutch</source>
        <translation>Flamanca</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="360"/>
        <source>English</source>
        <translation>İngilizce</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="361"/>
        <source>French</source>
        <translation>Fransızca</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="362"/>
        <source>Galician</source>
        <translation>Galiçyaca</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="363"/>
        <source>German</source>
        <translation>Almanca</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="364"/>
        <source>Greek</source>
        <translation>Yunanca</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="365"/>
        <source>Hebrew</source>
        <translation>İbranice</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="366"/>
        <source>Hungarian</source>
        <translation>Macarca</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="367"/>
        <source>Indonesian</source>
        <translation>Endonezyaca</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="368"/>
        <source>Italian</source>
        <translation>İtalyanca</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="369"/>
        <source>Japanese</source>
        <translation>Japonca</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="370"/>
        <source>Kazakh</source>
        <translation>Kazakça</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="371"/>
        <source>Korean</source>
        <translation>Korece</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="372"/>
        <source>Lithuanian</source>
        <translation>Litvanyaca</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="373"/>
        <source>Polish</source>
        <translation>Lehçe</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="374"/>
        <source>Portuguese</source>
        <translation>Portekizce</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="375"/>
        <source>Russian</source>
        <translation>Rusça</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="376"/>
        <source>Serbian</source>
        <translation>Sırpça</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="377"/>
        <source>Slovak</source>
        <translation>Slovakça</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="378"/>
        <source>Swedish</source>
        <translation>İsveçce</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="379"/>
        <source>Spanish</source>
        <translation>İspanyolca</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="380"/>
        <source>Turkish</source>
        <translation>Türkçe</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="381"/>
        <source>Ukrainian</source>
        <translation>Ukraynaca</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="382"/>
        <source>Serbian (Ijekavian)</source>
        <translation>Sırpça (Ijekavian)</translation>
    </message>
    <message>
        <location filename="../configdialog.cpp" line="383"/>
        <source>Serbian (Ekavian)</source>
        <translation>Sırpça (Ekavian)</translation>
    </message>
</context>
<context>
    <name>CoverEditor</name>
    <message>
        <location filename="../forms/covereditor.ui" line="22"/>
        <source>Image source:</source>
        <translation>Görüntü kaynağı:</translation>
    </message>
    <message>
        <location filename="../forms/covereditor.ui" line="76"/>
        <source>Load</source>
        <translation>Yükle</translation>
    </message>
    <message>
        <location filename="../forms/covereditor.ui" line="83"/>
        <source>Delete</source>
        <translation>Sil</translation>
    </message>
    <message>
        <location filename="../forms/covereditor.ui" line="90"/>
        <source>Save as...</source>
        <translation>Olarak kaydet...</translation>
    </message>
    <message>
        <location filename="../covereditor.cpp" line="34"/>
        <source>External file</source>
        <translation>Harici dosya</translation>
    </message>
    <message>
        <location filename="../covereditor.cpp" line="35"/>
        <source>Tag</source>
        <translation>Etiket</translation>
    </message>
</context>
<context>
    <name>CoverViewer</name>
    <message>
        <location filename="../coverviewer.cpp" line="35"/>
        <source>&amp;Save As...</source>
        <translation>&amp;Olarak Kaydet...</translation>
    </message>
    <message>
        <location filename="../coverviewer.cpp" line="68"/>
        <source>Save Cover As</source>
        <translation>Kapağı Farklı Kaydet</translation>
    </message>
    <message>
        <location filename="../coverviewer.cpp" line="70"/>
        <location filename="../coverviewer.cpp" line="83"/>
        <source>Images</source>
        <translation>Görüntüler</translation>
    </message>
    <message>
        <location filename="../coverviewer.cpp" line="81"/>
        <source>Open Image</source>
        <translation>Görüntü Açın</translation>
    </message>
</context>
<context>
    <name>CueEditor</name>
    <message>
        <location filename="../forms/cueeditor.ui" line="40"/>
        <source>Load</source>
        <translation>Yükle</translation>
    </message>
    <message>
        <location filename="../forms/cueeditor.ui" line="47"/>
        <source>Delete</source>
        <translation>Sil</translation>
    </message>
    <message>
        <location filename="../forms/cueeditor.ui" line="54"/>
        <source>Save as...</source>
        <translation>Olarak kaydet...</translation>
    </message>
    <message>
        <location filename="../cueeditor.cpp" line="131"/>
        <source>Open CUE File</source>
        <translation>CUE Dosyası Aç</translation>
    </message>
    <message>
        <location filename="../cueeditor.cpp" line="131"/>
        <location filename="../cueeditor.cpp" line="150"/>
        <source>CUE Files</source>
        <translation>CUE Dosyaları</translation>
    </message>
    <message>
        <location filename="../cueeditor.cpp" line="148"/>
        <source>Save CUE File</source>
        <translation>CUE Dosyası Kaydet</translation>
    </message>
</context>
<context>
    <name>DetailsDialog</name>
    <message>
        <location filename="../forms/detailsdialog.ui" line="14"/>
        <source>Details</source>
        <translation>Ayrıntılar</translation>
    </message>
    <message>
        <location filename="../forms/detailsdialog.ui" line="44"/>
        <source>Open the directory containing this file</source>
        <translation>Bu dosyayı içeren dizini açın</translation>
    </message>
    <message>
        <location filename="../forms/detailsdialog.ui" line="47"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/detailsdialog.ui" line="63"/>
        <source>Summary</source>
        <translation>Özet</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="207"/>
        <source>%1/%2</source>
        <translation>%1/%2</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="257"/>
        <source>Cover</source>
        <translation>Kapak</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="283"/>
        <source>Lyrics</source>
        <translation>Şarkı Sözleri</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="303"/>
        <source>Title</source>
        <translation>Başlık</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="304"/>
        <source>Artist</source>
        <translation>Sanatçı</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="305"/>
        <source>Album artist</source>
        <translation>Albüm sanatçısı</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="306"/>
        <source>Album</source>
        <translation>Albüm</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="307"/>
        <source>Comment</source>
        <translation>Yorum</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="308"/>
        <source>Genre</source>
        <translation>Tarz</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="309"/>
        <source>Composer</source>
        <translation>Besteci</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="310"/>
        <source>Year</source>
        <translation>Yıl</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="311"/>
        <source>Track</source>
        <translation>Parça</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="312"/>
        <source>Disc number</source>
        <translation>Disk numarası</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="331"/>
        <source>Duration</source>
        <translation>Süre</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="334"/>
        <source>Bitrate</source>
        <translation>Bit hızı</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="334"/>
        <source>kbps</source>
        <translation>kbps</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="335"/>
        <source>Sample rate</source>
        <translation>Örnekleme oranı</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="335"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="336"/>
        <source>Channels</source>
        <translation>Kanallar</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="337"/>
        <source>Sample size</source>
        <translation>Örnekleme boyutu</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="337"/>
        <source>bits</source>
        <translation>bitler</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="338"/>
        <source>Format name</source>
        <translation>Biçim adı</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="339"/>
        <source>File size</source>
        <translation>Dosya boyutu</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="339"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="383"/>
        <source>Yes</source>
        <translation>Evet</translation>
    </message>
    <message>
        <location filename="../detailsdialog.cpp" line="383"/>
        <source>No</source>
        <translation>Hayır</translation>
    </message>
</context>
<context>
    <name>JumpToTrackDialog</name>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="14"/>
        <source>Jump To Track</source>
        <translation>Parçaya Atla</translation>
    </message>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="46"/>
        <source>Filter</source>
        <translation>Süzgeç</translation>
    </message>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="85"/>
        <location filename="../jumptotrackdialog.cpp" line="120"/>
        <location filename="../jumptotrackdialog.cpp" line="151"/>
        <source>Queue</source>
        <translation>Kuyruk</translation>
    </message>
    <message>
        <location filename="../forms/jumptotrackdialog.ui" line="105"/>
        <source>Jump To</source>
        <translation>Atla</translation>
    </message>
    <message>
        <location filename="../jumptotrackdialog.cpp" line="95"/>
        <source>Q</source>
        <translation>Q</translation>
    </message>
    <message>
        <location filename="../jumptotrackdialog.cpp" line="96"/>
        <source>J</source>
        <translation>J</translation>
    </message>
    <message>
        <location filename="../jumptotrackdialog.cpp" line="118"/>
        <location filename="../jumptotrackdialog.cpp" line="149"/>
        <source>Unqueue</source>
        <translation>Sırayı kaldır</translation>
    </message>
</context>
<context>
    <name>MetaDataFormatterMenu</name>
    <message>
        <location filename="../metadataformattermenu.cpp" line="27"/>
        <source>Artist</source>
        <translation>Sanatçı</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="28"/>
        <source>Album</source>
        <translation>Albüm</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="29"/>
        <source>Album Artist</source>
        <translation>Albüm Sanatçısı</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="32"/>
        <source>Title</source>
        <translation>Başlık</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="33"/>
        <source>Track Number</source>
        <translation>Parça Numarası</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="34"/>
        <source>Two-digit Track Number</source>
        <translation>İki-rakam Parça Numarası</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="38"/>
        <source>Track Index</source>
        <translation>Parça Fihristi</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="40"/>
        <source>Genre</source>
        <translation>Tarz</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="41"/>
        <source>Comment</source>
        <translation>Yorum</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="42"/>
        <source>Composer</source>
        <translation>Besteci</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="47"/>
        <location filename="../metadataformattermenu.cpp" line="61"/>
        <source>Duration</source>
        <translation>Süre</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="55"/>
        <source>Artist - Album</source>
        <translation>Sanatçı - Albüm</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="62"/>
        <source>Duration | Format | Bitrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="63"/>
        <source>Duration | Format | Bitrate | Sample rate </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="64"/>
        <source>Year | Duration | Bitrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="68"/>
        <source>Parent Directory Path</source>
        <translation>Ana Dizin Yolu</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="70"/>
        <source>Bitrate</source>
        <translation>Bit hızı</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="71"/>
        <source>Sample Rate</source>
        <translation>Örnekleme Oranı</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="72"/>
        <source>Number of Channels</source>
        <translation>Kanalların sayısı</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="73"/>
        <source>Sample Size</source>
        <translation>Örnekleme Boyutu</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="74"/>
        <source>Format</source>
        <translation>Biçim</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="75"/>
        <source>Decoder</source>
        <translation>Kodlayıcı</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="78"/>
        <source>File Size</source>
        <translation>Dosya Boyutu</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="43"/>
        <source>Disc Number</source>
        <translation>Disk Numarası</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="48"/>
        <source>File Name</source>
        <translation>Dosya Adı</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="49"/>
        <source>File Path</source>
        <translation>Dosya Yolu</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="44"/>
        <source>Year</source>
        <translation>Yıl</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="51"/>
        <location filename="../metadataformattermenu.cpp" line="57"/>
        <location filename="../metadataformattermenu.cpp" line="65"/>
        <source>Condition</source>
        <translation>Koşul</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="50"/>
        <source>Artist - Title</source>
        <translation>Sanatçı - Başlık</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="56"/>
        <source>Artist - [Year] Album</source>
        <translation>Sanatçı - [Yıl] Albüm</translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="62"/>
        <source>%if(%l,%l | ,)%{format} | %{bitrate} kbps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="63"/>
        <source>%if(%l,%l | ,)%{format} | %{bitrate} kbps | %{samplerate} Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="64"/>
        <source>%y | %if(%l,%l | ,)%{bitrate} kbps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../metadataformattermenu.cpp" line="67"/>
        <source>Parent Directory Name</source>
        <translation>Ana Dizin Adı</translation>
    </message>
</context>
<context>
    <name>PlayListDownloader</name>
    <message>
        <location filename="../playlistdownloader.cpp" line="123"/>
        <source>Unsupported playlist format</source>
        <translation>Desteklenmeyen çalma listesi biçimi</translation>
    </message>
</context>
<context>
    <name>PlayListHeaderModel</name>
    <message>
        <location filename="../playlistheadermodel.cpp" line="35"/>
        <source>Artist - Title</source>
        <translation>Sanatçı - Başlık</translation>
    </message>
    <message>
        <location filename="../playlistheadermodel.cpp" line="185"/>
        <source>Title</source>
        <translation>Başlık</translation>
    </message>
    <message>
        <location filename="../playlistheadermodel.cpp" line="186"/>
        <source>Add Column</source>
        <translation>Sütun Ekle</translation>
    </message>
</context>
<context>
    <name>PlayListManager</name>
    <message>
        <location filename="../playlistmanager.cpp" line="177"/>
        <location filename="../playlistmanager.cpp" line="319"/>
        <source>Playlist</source>
        <translation>Çalma Listesi</translation>
    </message>
</context>
<context>
    <name>PlayListTrack</name>
    <message>
        <location filename="../playlisttrack.cpp" line="245"/>
        <source>Streams</source>
        <translation>Akışlar</translation>
    </message>
    <message>
        <location filename="../playlisttrack.cpp" line="250"/>
        <source>Empty group</source>
        <translation>Boş grup</translation>
    </message>
</context>
<context>
    <name>QmmpUiSettings</name>
    <message>
        <location filename="../qmmpuisettings.cpp" line="39"/>
        <source>%if(%l,%l | ,)%{format} | %{bitrate} kbps | %{samplerate} Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmmpuisettings.cpp" line="64"/>
        <source>Playlist</source>
        <translation>Çalma Listesi</translation>
    </message>
</context>
<context>
    <name>QtFileDialogFactory</name>
    <message>
        <location filename="../qtfiledialog.cpp" line="35"/>
        <source>Qt File Dialog</source>
        <translation>Qt Dosya Diyaloğu</translation>
    </message>
</context>
<context>
    <name>ShortcutDialog</name>
    <message>
        <location filename="../forms/shortcutdialog.ui" line="14"/>
        <source>Change Shortcut</source>
        <translation>Kısayolu Değiştir</translation>
    </message>
    <message>
        <location filename="../forms/shortcutdialog.ui" line="29"/>
        <source>Press the key combination you want to assign</source>
        <translation>Atamak istediğiniz tuş kombinasyonuna basın</translation>
    </message>
    <message>
        <location filename="../forms/shortcutdialog.ui" line="52"/>
        <source>Clear</source>
        <translation>Temizle</translation>
    </message>
</context>
<context>
    <name>TagEditor</name>
    <message>
        <location filename="../forms/tageditor.ui" line="14"/>
        <source>Tag Editor</source>
        <translation>Etiket Düzenleyici</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="38"/>
        <source>Title:</source>
        <translation>Başlık:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="64"/>
        <source>Artist:</source>
        <translation>Artist:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="90"/>
        <source>Album:</source>
        <translation>Albüm:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="113"/>
        <source>Album artist:</source>
        <translation>Albüm sanatçısı:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="123"/>
        <source>Composer:</source>
        <translation>Besteci:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="143"/>
        <source>Genre:</source>
        <translation>Tarz:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="169"/>
        <source>Track:</source>
        <translation>Parça:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="190"/>
        <location filename="../forms/tageditor.ui" line="228"/>
        <location filename="../forms/tageditor.ui" line="260"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="203"/>
        <source>Year:</source>
        <translation>Yıl:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="250"/>
        <source>Disc number:</source>
        <translation>Disk numarası:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="275"/>
        <source>Comment:</source>
        <translation>Yorum:</translation>
    </message>
    <message>
        <location filename="../forms/tageditor.ui" line="311"/>
        <source>Include selected tag in file</source>
        <translation>Seçili etiketi dosyaya dahil et</translation>
    </message>
</context>
<context>
    <name>TemplateEditor</name>
    <message>
        <location filename="../forms/templateeditor.ui" line="14"/>
        <source>Template Editor</source>
        <translation>Şablon Düzenleyici</translation>
    </message>
    <message>
        <location filename="../forms/templateeditor.ui" line="39"/>
        <source>Reset</source>
        <translation>Sıfırla</translation>
    </message>
    <message>
        <location filename="../forms/templateeditor.ui" line="46"/>
        <source>Insert</source>
        <translation>Araya Ekle</translation>
    </message>
</context>
<context>
    <name>UiHelper</name>
    <message>
        <location filename="../uihelper.cpp" line="136"/>
        <location filename="../uihelper.cpp" line="148"/>
        <source>All Supported Bitstreams</source>
        <translation>Tüm Desteklenen Bit akışları</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="142"/>
        <source>Select one or more files to open</source>
        <translation>Açmak için bir veya daha fazla dosya seçin
</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="154"/>
        <source>Select one or more files to play</source>
        <translation>Oynatmak için bir veya daha fazla dosya seçin</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="162"/>
        <source>Choose a directory</source>
        <translation>Dizin seç</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="178"/>
        <location filename="../uihelper.cpp" line="202"/>
        <source>Playlist Files</source>
        <translation>Çalma Listesi Dosyaları</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="180"/>
        <source>Open Playlist</source>
        <translation>Çalma Listesini Aç</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="205"/>
        <location filename="../uihelper.cpp" line="224"/>
        <source>Save Playlist</source>
        <translation>Çalma Listesini Kaydet</translation>
    </message>
    <message>
        <location filename="../uihelper.cpp" line="224"/>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation>%1 zaten var.
Bunu değiştirmek istiyor musunuz?</translation>
    </message>
</context>
<context>
    <name>VisualMenu</name>
    <message>
        <location filename="../visualmenu.cpp" line="26"/>
        <source>Visualization</source>
        <translation>Görüntüleme</translation>
    </message>
</context>
<context>
    <name>WinFileAssocPage</name>
    <message>
        <location filename="../forms/winfileassocpage.ui" line="43"/>
        <source>Media files handled by Qmmp:</source>
        <translation>Qmmp tarafından ele alınan medya dosyaları:</translation>
    </message>
    <message>
        <location filename="../forms/winfileassocpage.ui" line="17"/>
        <source>Select All</source>
        <translation>Hepsini seçin</translation>
    </message>
</context>
</TS>
