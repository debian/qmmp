Brezilya Portekizcesi:
    Bruno Gonçalves
    Klaos Lacerda
    Heitor Thury Barreiros Barbosa <hthury@gmail.com>
    Vitor Pereira

Bulgarca:
    Kiril Kirilov
    Miroslav Mihov

Çince Geleneksel:
    Cosmos Chen
    lon <lon83129@126.com>
    Jeff Huang

Çince Basitleştirilmiş:
    lon <lon83129@126.com>
    mabier
    Mingcong Bai
    Mingye Wang

Çekçe:
    Jaroslav Lichtblau
    Karel Volný <kvolny@redhat.com>

Flamanca:
    Heimen Stoffels
    Ronald Uitermark <ronald645@gmail.com>

Fince:
    Jiri Grönroos
    Kimmo Kujansuu

Fransızca:
    Adrien Vigneron - VRad <adrienvigneron@ml1.net>
    Sébastien Aperghis-Tramoni (maddingue)
    Stanislas Zeller <uncensored.assault@gmail.com>

Galiçyaca:
    Delio Docampo Cordeiro
    Óscar Pereira <oscarpereira1989@gmail.com>

Almanca:
    Ettore Atalan
    Stefan Koelling <stefankoelling.ext@googlemail.com>
    Panagiotis Papadopoulos <pano_90@gmx.net>

Yunanca:
    Dimitrios Glentadakis

İbranice:
    Genghis Khan <genghiskhan@gmx.ca>

Macarca:
    Németh Gábor <sutee84@freemail.hu>

Endonezyaca:
    Andika Triwidada
    Wantoyo <wantoyek@gmail.com>

İtalyanca:
    Gian Paolo Renello <emmerkar@gmail.com>
    Luigi Toscano

Japonca:
    Ryota Shimamoto <liangtai.s16@gmail.com>

Kazakça:
    Baurzhan Muftakhidinov <baurthefirst@gmail.com>

Korece:
   Jung Hee Lee <daemul72@gmail.com>

Litvanyaca:
    Algirdas Butkus <butkus.algirdas@gmail.com>

Lehçe:
    Marek Adamski
    Daniel Krawczyk
    Grzegorz Gibas <amigib@gmail.com>

Portekizce:
    Sérgio Marques <smarquespt@gmail.com>

Rusça:
    Alexey Loginov <loginov.alex.valer.gmail.com>
    Ilya Kotov <forkotov02@ya.ru>
    Andrei Stepanov
    Viktor Eruhin

Sırpça:
    Mladen Pejaković <pejakm@autistici.org>

Slovakça:
    Ján Ďanovský <dagsoftware@yahoo.com>

İspanyolca:
    Bohdan Shuba
    Félix Medrano <xukosky@yahoo.es>
    Joel Barrios <darkshram@gmail.com>

İsveçce
    Luna Jernberg <droidbittin@gmail.com>

Türkçe:
    Mustafa GUNAY <mustafagunay@pgmail.com>
    Bilgesu Güngör <h.ibrahim.gungor@gmail.com>
    Uğur KUYU

Ukraynaca:
    Gennadi Motsyo <drool@altlinux.ru>
