<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>TwoPanelFileDialog</name>
    <message>
        <location filename="../twopanelfiledialog.ui" line="14"/>
        <source>Add Files</source>
        <translation>ファイルを追加</translation>
    </message>
    <message>
        <location filename="../twopanelfiledialog.ui" line="70"/>
        <source>File name:</source>
        <translation>ファイル名:</translation>
    </message>
    <message>
        <location filename="../twopanelfiledialog.ui" line="80"/>
        <source>Files of type:</source>
        <translation>この種のファイル:</translation>
    </message>
    <message>
        <location filename="../twopanelfiledialog.ui" line="117"/>
        <source>Play</source>
        <translation>再生</translation>
    </message>
    <message>
        <location filename="../twopanelfiledialog.ui" line="130"/>
        <source>Add</source>
        <translation>追加</translation>
    </message>
    <message>
        <location filename="../twopanelfiledialog.ui" line="143"/>
        <source>Close</source>
        <translation>閉じる</translation>
    </message>
</context>
<context>
    <name>TwoPanelFileDialogFactory</name>
    <message>
        <location filename="../twopanelfiledialog.cpp" line="67"/>
        <location filename="../twopanelfiledialog.cpp" line="77"/>
        <source>Two-panel File Dialog</source>
        <translation>二パネル式ファイルダイアログ</translation>
    </message>
    <message>
        <location filename="../twopanelfiledialog.cpp" line="76"/>
        <source>About Two-panel File Dialog</source>
        <translation>二パネル式ファイルダイアログについて</translation>
    </message>
    <message>
        <location filename="../twopanelfiledialog.cpp" line="78"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>制作: Илья Котов (Ilya Kotov) &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../twopanelfiledialog.cpp" line="79"/>
        <source>Based on code from the Qt library</source>
        <translation>Qt ライブラリのコードを基に作成</translation>
    </message>
</context>
<context>
    <name>TwoPanelFileDialogImpl</name>
    <message>
        <location filename="../twopanelfiledialogimpl.cpp" line="322"/>
        <location filename="../twopanelfiledialogimpl.cpp" line="338"/>
        <source>Add</source>
        <translation>追加</translation>
    </message>
    <message>
        <location filename="../twopanelfiledialogimpl.cpp" line="340"/>
        <source>Directories</source>
        <translation>ディレクトリ</translation>
    </message>
    <message>
        <location filename="../twopanelfiledialogimpl.cpp" line="352"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../twopanelfiledialogimpl.cpp" line="458"/>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation>%1 は既にあります。
置き換えますか。</translation>
    </message>
</context>
</TS>
