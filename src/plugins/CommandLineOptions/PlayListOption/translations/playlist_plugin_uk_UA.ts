<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>PlayListOption</name>
    <message>
        <location filename="../playlistoption.cpp" line="33"/>
        <source>Show playlist manipulation commands</source>
        <translation>Показати команди керування грайлистом</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="34"/>
        <source>List all available playlists</source>
        <translation>Показати всі доступні грайлисти</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="35"/>
        <source>Show playlist content</source>
        <translation>Показати вміст грайлиста</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="36"/>
        <source>Play track &lt;track&gt; in playlist &lt;id&gt;</source>
        <translation>Відтворити доріжку &lt;track&gt; в переліку &lt;id&gt;</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="37"/>
        <source>Clear playlist</source>
        <translation>Очистити грайлист</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="38"/>
        <source>Activate next playlist</source>
        <translation>Активувати наступний перелік</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="39"/>
        <source>Activate previous playlist</source>
        <translation>Активувати попередній перелік</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="40"/>
        <source>Toggle playlist repeat</source>
        <translation>Увімкнути/вимкнути повторення грайлиста</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="41"/>
        <source>Toggle playlist shuffle</source>
        <translation>Увімкнути/вимкнути режим випадкового відтворення</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="42"/>
        <source>Show playlist options</source>
        <translation>Показати налаштування грайлиста</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="112"/>
        <location filename="../playlistoption.cpp" line="132"/>
        <location filename="../playlistoption.cpp" line="171"/>
        <source>Invalid playlist ID</source>
        <translation>Невірний номер грайлиста</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="126"/>
        <source>Invalid number of arguments</source>
        <translation>Невірне число аргументів</translation>
    </message>
    <message>
        <location filename="../playlistoption.cpp" line="136"/>
        <source>Invalid track ID</source>
        <translation>Невірний номер доріжки</translation>
    </message>
</context>
</TS>
