<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sr_BA">
<context>
    <name>SkinnedActionManager</name>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="39"/>
        <source>&amp;Play</source>
        <translation type="unfinished">&amp;Пусти</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="39"/>
        <source>X</source>
        <translation type="unfinished">X</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="40"/>
        <source>&amp;Pause</source>
        <translation type="unfinished">П&amp;аузирај</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="40"/>
        <source>C</source>
        <translation type="unfinished">C</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="41"/>
        <source>&amp;Stop</source>
        <translation type="unfinished">&amp;Заустави</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="41"/>
        <source>V</source>
        <translation type="unfinished">V</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="42"/>
        <source>&amp;Previous</source>
        <translation type="unfinished">П&amp;ретходна</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="42"/>
        <source>Z</source>
        <translation type="unfinished">Z</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="43"/>
        <source>&amp;Next</source>
        <translation type="unfinished">&amp;Сљедећа</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="43"/>
        <source>B</source>
        <translation type="unfinished">B</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="44"/>
        <source>&amp;Play/Pause</source>
        <translation type="unfinished">П&amp;усти/паузирај</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="44"/>
        <source>Space</source>
        <translation type="unfinished">Размак</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="45"/>
        <source>&amp;Jump to Track</source>
        <translation type="unfinished">С&amp;кочи на нумеру</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="45"/>
        <source>J</source>
        <translation type="unfinished">J</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="46"/>
        <source>&amp;Repeat Playlist</source>
        <translation type="unfinished">Понављај &amp;листу</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="46"/>
        <source>R</source>
        <translation type="unfinished">R</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="47"/>
        <source>&amp;Repeat Track</source>
        <translation type="unfinished">Понављај &amp;нумеру</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="47"/>
        <source>Ctrl+R</source>
        <translation type="unfinished">Ctrl+R</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="48"/>
        <source>&amp;Shuffle</source>
        <translation type="unfinished">На&amp;сумично</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="48"/>
        <source>S</source>
        <translation type="unfinished">S</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="49"/>
        <source>&amp;No Playlist Advance</source>
        <translation type="unfinished">&amp;Без напредовања листе</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="49"/>
        <source>Ctrl+N</source>
        <translation type="unfinished">Ctrl+N</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="50"/>
        <source>&amp;Stop After Selected</source>
        <translation type="unfinished">&amp;Заустави након избора</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="50"/>
        <source>Ctrl+S</source>
        <translation type="unfinished">Ctrl+S</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="51"/>
        <source>&amp;Transit between playlists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="52"/>
        <source>&amp;Clear Queue</source>
        <translation type="unfinished">&amp;Очисти ред</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="52"/>
        <source>Alt+Q</source>
        <translation type="unfinished">Alt+Q</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="54"/>
        <source>Show Playlist</source>
        <translation type="unfinished">Листа нумера</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="54"/>
        <source>Alt+E</source>
        <translation type="unfinished">Alt+E</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="55"/>
        <source>Show Equalizer</source>
        <translation type="unfinished">Еквилајзер</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="55"/>
        <source>Alt+G</source>
        <translation type="unfinished">Alt+G</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="56"/>
        <source>Always on Top</source>
        <translation type="unfinished">Увијек на врху</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="57"/>
        <source>Put on All Workspaces</source>
        <translation type="unfinished">На свим површима</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="58"/>
        <source>Double Size</source>
        <translation type="unfinished">Двострука величина</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="58"/>
        <source>Meta+D</source>
        <translation type="unfinished">Meta+D</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="59"/>
        <source>Anti-aliasing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="61"/>
        <source>Volume &amp;+</source>
        <translation type="unfinished">Јачина &amp;+</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="61"/>
        <source>0</source>
        <translation type="unfinished">0</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="62"/>
        <source>Volume &amp;-</source>
        <translation type="unfinished">Јачина &amp;-</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="62"/>
        <source>9</source>
        <translation type="unfinished">9</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="63"/>
        <source>&amp;Mute</source>
        <translation type="unfinished">Утиша&amp;ј</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="63"/>
        <source>M</source>
        <translation type="unfinished">M</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="65"/>
        <source>&amp;Add File</source>
        <translation type="unfinished">Додај &amp;фајл</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="65"/>
        <source>F</source>
        <translation type="unfinished">F</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="66"/>
        <source>&amp;Add Directory</source>
        <translation type="unfinished">Додај фас&amp;циклу</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="66"/>
        <source>D</source>
        <translation type="unfinished">D</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="67"/>
        <source>&amp;Add Url</source>
        <translation type="unfinished">Додај Ур&amp;л</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="67"/>
        <source>U</source>
        <translation type="unfinished">U</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="68"/>
        <source>&amp;Remove Selected</source>
        <translation type="unfinished">Уклони &amp;избор</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="68"/>
        <source>Del</source>
        <translation type="unfinished">Del</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="69"/>
        <source>&amp;Remove All</source>
        <translation type="unfinished">Уклони &amp;све</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="70"/>
        <source>&amp;Remove Unselected</source>
        <translation type="unfinished">Уклони &amp;неизабрано</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="71"/>
        <source>Remove unavailable files</source>
        <translation type="unfinished">Уклони недоступне фајлове</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="72"/>
        <source>Remove duplicates</source>
        <translation type="unfinished">Уклони дупликате</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="73"/>
        <source>Refresh</source>
        <translation type="unfinished">Освјежи</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="74"/>
        <source>&amp;Queue Toggle</source>
        <translation type="unfinished">Стави у &amp;ред</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="74"/>
        <source>Q</source>
        <translation type="unfinished">Q</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="75"/>
        <source>Invert Selection</source>
        <translation type="unfinished">Обрни избор</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="76"/>
        <source>&amp;Select None</source>
        <translation type="unfinished">&amp;Очисти избор</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="77"/>
        <source>&amp;Select All</source>
        <translation type="unfinished">Изабери &amp;све</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="77"/>
        <source>Ctrl+A</source>
        <translation type="unfinished">Ctrl+A</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="78"/>
        <source>&amp;View Track Details</source>
        <translation type="unfinished">&amp;Види детаље нумере</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="78"/>
        <source>Alt+I</source>
        <translation type="unfinished">Alt+I</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="79"/>
        <source>&amp;New List</source>
        <translation type="unfinished">&amp;Нова листа нумера</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="79"/>
        <source>Ctrl+T</source>
        <translation type="unfinished">Ctrl+T</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="80"/>
        <source>&amp;Delete List</source>
        <translation type="unfinished">&amp;Обриши листу нумера</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="80"/>
        <source>Ctrl+W</source>
        <translation type="unfinished">Ctrl+W</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="81"/>
        <source>&amp;Load List</source>
        <translation type="unfinished">&amp;Учитај листу нумера</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="81"/>
        <source>O</source>
        <translation type="unfinished">O</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="82"/>
        <source>&amp;Save List</source>
        <translation type="unfinished">&amp;Сачувај листу нумера</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="82"/>
        <source>Shift+S</source>
        <translation type="unfinished">Shift+S</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="83"/>
        <source>&amp;Rename List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="83"/>
        <source>F2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="84"/>
        <source>&amp;Select Next Playlist</source>
        <translation type="unfinished">Изабери сље&amp;дећу</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="84"/>
        <source>Ctrl+PgDown</source>
        <translation type="unfinished">Ctrl+PgDown</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="85"/>
        <source>&amp;Select Previous Playlist</source>
        <translation type="unfinished">Изабери &amp;претходну</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="85"/>
        <source>Ctrl+PgUp</source>
        <translation type="unfinished">Ctrl+PgUp</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="86"/>
        <source>&amp;Show Playlists</source>
        <translation type="unfinished">П&amp;рикажи листе нумера</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="86"/>
        <source>P</source>
        <translation type="unfinished">P</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="87"/>
        <source>&amp;Group Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="87"/>
        <source>Ctrl+G</source>
        <translation type="unfinished">Ctrl+G</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="88"/>
        <source>&amp;Show Column Headers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="88"/>
        <source>Ctrl+H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="89"/>
        <source>Show &amp;Tab Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="89"/>
        <source>Alt+T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="91"/>
        <source>&amp;Settings</source>
        <translation type="unfinished">По&amp;дешавање</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="91"/>
        <source>Ctrl+P</source>
        <translation type="unfinished">Ctrl+P</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="92"/>
        <source>&amp;About</source>
        <translation type="unfinished">&amp;О програму</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="93"/>
        <source>&amp;About Qt</source>
        <translation type="unfinished">О &amp;Куту</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="94"/>
        <source>&amp;Exit</source>
        <translation type="unfinished">&amp;Напусти</translation>
    </message>
    <message>
        <location filename="../skinnedactionmanager.cpp" line="94"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished">Ctrl+Q</translation>
    </message>
</context>
<context>
    <name>SkinnedDisplay</name>
    <message>
        <location filename="../skinneddisplay.cpp" line="59"/>
        <source>Previous</source>
        <translation type="unfinished">Претходна</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="63"/>
        <source>Play</source>
        <translation type="unfinished">Пусти</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="66"/>
        <source>Pause</source>
        <translation type="unfinished">Паузирај</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="69"/>
        <source>Stop</source>
        <translation type="unfinished">Заустави</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="72"/>
        <source>Next</source>
        <translation type="unfinished">Сљедећа</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="75"/>
        <source>Play files</source>
        <translation type="unfinished">Пусти фајлове</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="80"/>
        <source>Equalizer</source>
        <translation type="unfinished">Еквилајзер</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="82"/>
        <source>Playlist</source>
        <translation type="unfinished">Листа нумера</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="85"/>
        <source>Repeat playlist</source>
        <translation type="unfinished">Понављање</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="87"/>
        <source>Shuffle</source>
        <translation type="unfinished">Насумично</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="97"/>
        <source>Volume</source>
        <translation type="unfinished">Јачина</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="103"/>
        <source>Balance</source>
        <translation type="unfinished">Баланс</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="290"/>
        <source>Volume: %1%</source>
        <translation type="unfinished">Јачина: %1%</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="294"/>
        <source>Balance: %1% right</source>
        <translation type="unfinished">Баланс: %1% десно</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="296"/>
        <source>Balance: %1% left</source>
        <translation type="unfinished">Баланс: %1% лијево</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="298"/>
        <source>Balance: center</source>
        <translation type="unfinished">Баланс: центар</translation>
    </message>
    <message>
        <location filename="../skinneddisplay.cpp" line="304"/>
        <source>Seek to: %1</source>
        <translation type="unfinished">Иди на: %1</translation>
    </message>
</context>
<context>
    <name>SkinnedEqWidget</name>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="47"/>
        <source>Equalizer</source>
        <translation type="unfinished">Еквилајзер</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="160"/>
        <location filename="../skinnedeqwidget.cpp" line="177"/>
        <source>preset</source>
        <translation type="unfinished">поставка</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="261"/>
        <source>&amp;Load/Delete</source>
        <translation type="unfinished">&amp;Учитај/обриши</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="263"/>
        <source>&amp;Save Preset</source>
        <translation type="unfinished">&amp;Сачувај поставку</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="264"/>
        <source>&amp;Save Auto-load Preset</source>
        <translation type="unfinished">&amp;Сачувај ауто-учитану поставку</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="265"/>
        <source>&amp;Import</source>
        <translation type="unfinished">У&amp;вези</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="267"/>
        <source>&amp;Clear</source>
        <translation type="unfinished">&amp;Очисти</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="296"/>
        <source>Saving Preset</source>
        <translation type="unfinished">Уписивање поставке</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="297"/>
        <source>Preset name:</source>
        <translation type="unfinished">Име поставке:</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="298"/>
        <source>preset #</source>
        <translation type="unfinished">поставка еквилајзера #</translation>
    </message>
    <message>
        <location filename="../skinnedeqwidget.cpp" line="394"/>
        <source>Import Preset</source>
        <translation type="unfinished">Увоз поставке еквилајзера</translation>
    </message>
</context>
<context>
    <name>SkinnedFactory</name>
    <message>
        <location filename="../skinnedfactory.cpp" line="35"/>
        <source>Skinned User Interface</source>
        <translation>Корисничко сучеље са маском</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="61"/>
        <source>About Qmmp Skinned User Interface</source>
        <translation>О Кумп корисничком сучељу са маском</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="62"/>
        <source>Qmmp Skinned User Interface</source>
        <translation>Кумп корисничко сучеље са маском</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="63"/>
        <source>Simple user interface with Winamp-2.x/XMMS skins support</source>
        <translation>Једноставно корисничко сучеље са подршком за Винамп-2.x/ИксММС маске</translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="64"/>
        <source>Written by:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="65"/>
        <source>Vladimir Kuznetsov &lt;vovanec@gmail.com&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="66"/>
        <source>Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="67"/>
        <source>Artwork:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="68"/>
        <source>Andrey Adreev &lt;andreev00@gmail.com&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedfactory.cpp" line="69"/>
        <source>sixsixfive &lt;http://sixsixfive.deviantart.com/&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SkinnedHotkeyEditor</name>
    <message>
        <location filename="../forms/skinnedhotkeyeditor.ui" line="33"/>
        <source>Change shortcut...</source>
        <translation type="unfinished">Измијени пречицу...</translation>
    </message>
    <message>
        <location filename="../forms/skinnedhotkeyeditor.ui" line="44"/>
        <source>Reset</source>
        <translation type="unfinished">Ресетуј</translation>
    </message>
    <message>
        <location filename="../forms/skinnedhotkeyeditor.ui" line="58"/>
        <source>Action</source>
        <translation type="unfinished">радња</translation>
    </message>
    <message>
        <location filename="../forms/skinnedhotkeyeditor.ui" line="63"/>
        <source>Shortcut</source>
        <translation type="unfinished">пречица</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="57"/>
        <source>Playback</source>
        <translation type="unfinished">Пуштање</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="63"/>
        <source>View</source>
        <translation type="unfinished">Приказ</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="69"/>
        <source>Volume</source>
        <translation type="unfinished">Јачина</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="75"/>
        <source>Playlist</source>
        <translation type="unfinished">Листа нумера</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="81"/>
        <source>Misc</source>
        <translation type="unfinished">Разно</translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="93"/>
        <source>Reset Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedhotkeyeditor.cpp" line="94"/>
        <source>Do you want to restore default shortcuts?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SkinnedMainWindow</name>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="358"/>
        <source>Appearance</source>
        <translation type="unfinished">Изглед</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="359"/>
        <source>Shortcuts</source>
        <translation type="unfinished">Пречице</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="433"/>
        <source>View</source>
        <translation type="unfinished">Приказ</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="442"/>
        <source>Playlist</source>
        <translation type="unfinished">Листа нумера</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="461"/>
        <source>Audio</source>
        <translation type="unfinished">Звук</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="469"/>
        <source>Tools</source>
        <translation type="unfinished">Алатке</translation>
    </message>
    <message>
        <location filename="../skinnedmainwindow.cpp" line="541"/>
        <source>Qmmp</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SkinnedPlayList</name>
    <message>
        <location filename="../skinnedplaylist.cpp" line="56"/>
        <source>Playlist</source>
        <translation type="unfinished">Листа нумера</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="193"/>
        <source>&amp;Copy Selection To</source>
        <translation type="unfinished">&amp;Копирај избор у</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="218"/>
        <source>Sort List</source>
        <translation type="unfinished">Сортирај</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="221"/>
        <location filename="../skinnedplaylist.cpp" line="261"/>
        <source>By Title</source>
        <translation type="unfinished">по наслову</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="224"/>
        <location filename="../skinnedplaylist.cpp" line="264"/>
        <source>By Album</source>
        <translation type="unfinished">по албуму</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="227"/>
        <location filename="../skinnedplaylist.cpp" line="267"/>
        <source>By Artist</source>
        <translation type="unfinished">по извођачу</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="230"/>
        <location filename="../skinnedplaylist.cpp" line="270"/>
        <source>By Album Artist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="233"/>
        <location filename="../skinnedplaylist.cpp" line="273"/>
        <source>By Filename</source>
        <translation type="unfinished">по имену фајла</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="236"/>
        <location filename="../skinnedplaylist.cpp" line="276"/>
        <source>By Path + Filename</source>
        <translation type="unfinished">по путањи и имену</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="239"/>
        <location filename="../skinnedplaylist.cpp" line="279"/>
        <source>By Date</source>
        <translation type="unfinished">по датуму</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="242"/>
        <location filename="../skinnedplaylist.cpp" line="282"/>
        <source>By Track Number</source>
        <translation type="unfinished">по броју нумере</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="245"/>
        <location filename="../skinnedplaylist.cpp" line="285"/>
        <source>By Disc Number</source>
        <translation type="unfinished">по броју диска</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="248"/>
        <location filename="../skinnedplaylist.cpp" line="288"/>
        <source>By File Creation Date</source>
        <translation type="unfinished">по датуму фајла</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="251"/>
        <location filename="../skinnedplaylist.cpp" line="291"/>
        <source>By File Modification Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="254"/>
        <source>By Group</source>
        <translation type="unfinished">по групи</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="259"/>
        <source>Sort Selection</source>
        <translation type="unfinished">Сортирај избор</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="296"/>
        <source>Randomize List</source>
        <translation type="unfinished">Претумбај</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="298"/>
        <source>Reverse List</source>
        <translation type="unfinished">Обрни</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="306"/>
        <source>Actions</source>
        <translation type="unfinished">Радње</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="576"/>
        <source>Rename Playlist</source>
        <translation type="unfinished">Преименовање листе</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="576"/>
        <source>Playlist name:</source>
        <translation type="unfinished">Име листе:</translation>
    </message>
    <message>
        <location filename="../skinnedplaylist.cpp" line="597"/>
        <source>&amp;New PlayList</source>
        <translation type="unfinished">&amp;нову листу</translation>
    </message>
</context>
<context>
    <name>SkinnedPlayListBrowser</name>
    <message>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="14"/>
        <source>Playlist Browser</source>
        <translation type="unfinished">Прегледач листи нумера</translation>
    </message>
    <message>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="31"/>
        <source>Filter:</source>
        <translation type="unfinished">Филтер:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="47"/>
        <source>New</source>
        <translation type="unfinished">Нова</translation>
    </message>
    <message>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="54"/>
        <location filename="../skinnedplaylistbrowser.cpp" line="45"/>
        <source>Delete</source>
        <translation type="unfinished">Обриши</translation>
    </message>
    <message>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="61"/>
        <location filename="../forms/skinnedplaylistbrowser.ui" line="71"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../skinnedplaylistbrowser.cpp" line="44"/>
        <source>Rename</source>
        <translation type="unfinished">Преименуј</translation>
    </message>
</context>
<context>
    <name>SkinnedPlayListHeader</name>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="77"/>
        <source>Add Column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="78"/>
        <source>Edit Column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="79"/>
        <source>Show Queue/Protocol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="81"/>
        <source>Auto-resize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="84"/>
        <source>Alignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="85"/>
        <source>Left</source>
        <comment>alignment</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="86"/>
        <source>Right</source>
        <comment>alignment</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="87"/>
        <source>Center</source>
        <comment>alignment</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedplaylistheader.cpp" line="97"/>
        <source>Remove Column</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SkinnedPopupSettings</name>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="14"/>
        <source>Popup Information Settings</source>
        <translation type="unfinished">Поставке облачића података</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="29"/>
        <source>Template</source>
        <translation type="unfinished">Шаблон</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="58"/>
        <source>Reset</source>
        <translation type="unfinished">Ресетуј</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="65"/>
        <source>Insert</source>
        <translation type="unfinished">Уметни</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="75"/>
        <source>Show cover</source>
        <translation type="unfinished">Прикажи омот</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="89"/>
        <source>Cover size:</source>
        <translation type="unfinished">Величина омота:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="115"/>
        <source>Transparency:</source>
        <translation type="unfinished">Прозирност:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="145"/>
        <source>Delay:</source>
        <translation type="unfinished">Застој:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpopupsettings.ui" line="178"/>
        <source>ms</source>
        <translation type="unfinished">ms</translation>
    </message>
</context>
<context>
    <name>SkinnedPresetEditor</name>
    <message>
        <location filename="../forms/skinnedpreseteditor.ui" line="14"/>
        <source>Preset Editor</source>
        <translation type="unfinished">Уређивач поставки еквилајзера</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpreseteditor.ui" line="36"/>
        <source>Preset</source>
        <translation type="unfinished">Поставка</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpreseteditor.ui" line="61"/>
        <source>Auto-preset</source>
        <translation type="unfinished">Ауто-поставка</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpreseteditor.ui" line="95"/>
        <source>Load</source>
        <translation type="unfinished">Учитај</translation>
    </message>
    <message>
        <location filename="../forms/skinnedpreseteditor.ui" line="102"/>
        <source>Delete</source>
        <translation type="unfinished">Обриши</translation>
    </message>
</context>
<context>
    <name>SkinnedSettings</name>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="24"/>
        <source>Skins</source>
        <translation>Маске</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="55"/>
        <source>Add...</source>
        <translation>Додај...</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="72"/>
        <source>Refresh</source>
        <translation>Освјежи</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="103"/>
        <source>Main Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="109"/>
        <source>Hide on close</source>
        <translation>Сакриј по затварању</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="116"/>
        <source>Start hidden</source>
        <translation>Покрени скривено</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="123"/>
        <source>Use skin cursors</source>
        <translation>Користи показиваче маске</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="261"/>
        <source>Single Column Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="308"/>
        <source>Show splitters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="315"/>
        <source>Alternate splitter color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="415"/>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="421"/>
        <source>Playlist Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="427"/>
        <source>Use skin colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="545"/>
        <source>Background #2:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="609"/>
        <source>Highlighted background:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="500"/>
        <source>Normal text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="763"/>
        <source>Splitter:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="577"/>
        <source>Current text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="641"/>
        <source>Highlighted text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="813"/>
        <source>Current track background:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="871"/>
        <source>Override current track background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="778"/>
        <source>Group background:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="878"/>
        <source>Override group background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="700"/>
        <source>Group text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="455"/>
        <source>Background #1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="691"/>
        <source>Load skin colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="904"/>
        <source>Fonts</source>
        <translation>Фонтови</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="958"/>
        <source>Playlist:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="994"/>
        <source>Groups:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="1021"/>
        <source>Extra group row:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="1048"/>
        <source>Column headers:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="916"/>
        <source>Player:</source>
        <translation>Плејер:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="938"/>
        <location filename="../forms/skinnedsettings.ui" line="980"/>
        <location filename="../forms/skinnedsettings.ui" line="1061"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="142"/>
        <location filename="../forms/skinnedsettings.ui" line="945"/>
        <location filename="../forms/skinnedsettings.ui" line="987"/>
        <location filename="../forms/skinnedsettings.ui" line="1068"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="1103"/>
        <source>Reset fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="1112"/>
        <source>Use bitmap font if available</source>
        <translation>Користи битмапски фонт ако је доступан</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="132"/>
        <source>Window title format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="97"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="154"/>
        <source>Transparency</source>
        <translation>Прозирност</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="160"/>
        <source>Main window</source>
        <translation>Главни прозор</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="183"/>
        <location filename="../forms/skinnedsettings.ui" line="207"/>
        <location filename="../forms/skinnedsettings.ui" line="231"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="190"/>
        <source>Equalizer</source>
        <translation>Еквилајзер</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="214"/>
        <source>Playlist</source>
        <translation>Листа нумера</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="255"/>
        <source>Song Display</source>
        <translation>Приказ нумере</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="294"/>
        <source>Show protocol</source>
        <translation>Протокол</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="274"/>
        <source>Show song lengths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="267"/>
        <source>Show song numbers</source>
        <translation>Број нумере</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="284"/>
        <source>Align song numbers</source>
        <translation>Уравнај бројеве нумера</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="301"/>
        <source>Show anchor</source>
        <translation>Сидро</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="363"/>
        <source>Show popup information</source>
        <translation>Облачић с подацима</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="375"/>
        <source>Edit template</source>
        <translation>Уреди шаблон</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="331"/>
        <source>Playlist separator:</source>
        <translation>Раздвајач листи нумера:</translation>
    </message>
    <message>
        <location filename="../forms/skinnedsettings.ui" line="322"/>
        <source>Show &apos;New Playlist&apos; button</source>
        <translation>Дугме „Нова листа нумера“</translation>
    </message>
    <message>
        <location filename="../skinnedsettings.cpp" line="81"/>
        <source>Select Skin Files</source>
        <translation>Избор фајлова маске</translation>
    </message>
    <message>
        <location filename="../skinnedsettings.cpp" line="82"/>
        <source>Skin files</source>
        <translation>Фајлови маске</translation>
    </message>
    <message>
        <location filename="../skinnedsettings.cpp" line="165"/>
        <source>Default skin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedsettings.cpp" line="174"/>
        <source>Unarchived skin %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../skinnedsettings.cpp" line="174"/>
        <source>Archived skin %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SkinnedTextScroller</name>
    <message>
        <location filename="../skinnedtextscroller.cpp" line="54"/>
        <source>Autoscroll Songname</source>
        <translation type="unfinished">Ауто-клизање наслова нумере</translation>
    </message>
    <message>
        <location filename="../skinnedtextscroller.cpp" line="55"/>
        <source>Transparent Background</source>
        <translation type="unfinished">Прозирна позадина</translation>
    </message>
    <message>
        <location filename="../skinnedtextscroller.cpp" line="122"/>
        <source>Buffering: %1%</source>
        <translation type="unfinished">Баферовање: %1%</translation>
    </message>
</context>
<context>
    <name>SkinnedVisualization</name>
    <message>
        <location filename="../skinnedvisualization.cpp" line="211"/>
        <source>Visualization Mode</source>
        <translation type="unfinished">Визуелизације</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="214"/>
        <source>Analyzer</source>
        <translation type="unfinished">Анализатор</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="215"/>
        <source>Scope</source>
        <translation type="unfinished">Опсег</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="216"/>
        <source>Off</source>
        <translation type="unfinished">Угаси</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="223"/>
        <source>Analyzer Mode</source>
        <translation type="unfinished">Анализатор</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="226"/>
        <source>Normal</source>
        <translation type="unfinished">Нормално</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="227"/>
        <source>Fire</source>
        <translation type="unfinished">Ватра</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="228"/>
        <source>Vertical Lines</source>
        <translation type="unfinished">Вертикалне линије</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="229"/>
        <source>Lines</source>
        <translation type="unfinished">Линије</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="230"/>
        <source>Bars</source>
        <translation type="unfinished">Траке</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="243"/>
        <source>Peaks</source>
        <translation type="unfinished">Врхови</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="247"/>
        <source>Refresh Rate</source>
        <translation type="unfinished">Фреквенција</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="250"/>
        <source>50 fps</source>
        <translation type="unfinished">50 fps</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="251"/>
        <source>25 fps</source>
        <translation type="unfinished">25 fps</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="252"/>
        <source>10 fps</source>
        <translation type="unfinished">10 fps</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="253"/>
        <source>5 fps</source>
        <translation type="unfinished">5 fps</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="260"/>
        <source>Analyzer Falloff</source>
        <translation type="unfinished">Падање анализатора</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="263"/>
        <location filename="../skinnedvisualization.cpp" line="277"/>
        <source>Slowest</source>
        <translation type="unfinished">Најспорије</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="264"/>
        <location filename="../skinnedvisualization.cpp" line="278"/>
        <source>Slow</source>
        <translation type="unfinished">Споро</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="265"/>
        <location filename="../skinnedvisualization.cpp" line="279"/>
        <source>Medium</source>
        <translation type="unfinished">Умјерено</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="266"/>
        <location filename="../skinnedvisualization.cpp" line="280"/>
        <source>Fast</source>
        <translation type="unfinished">Брзо</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="267"/>
        <location filename="../skinnedvisualization.cpp" line="281"/>
        <source>Fastest</source>
        <translation type="unfinished">Најбрже</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="274"/>
        <source>Peaks Falloff</source>
        <translation type="unfinished">Падање врхова</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="287"/>
        <source>Background</source>
        <translation type="unfinished">Позадина</translation>
    </message>
    <message>
        <location filename="../skinnedvisualization.cpp" line="288"/>
        <source>Transparent</source>
        <translation type="unfinished">Прозирност</translation>
    </message>
</context>
</TS>
