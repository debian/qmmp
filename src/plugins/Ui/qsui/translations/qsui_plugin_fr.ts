<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>AboutQSUIDialog</name>
    <message>
        <location filename="../forms/aboutqsuidialog.ui" line="14"/>
        <source>About QSUI</source>
        <translation>À propos de QSUI</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="42"/>
        <source>Qmmp Simple User Interface (QSUI)</source>
        <translation>Qmmp Simple User Interface (QSUI)</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="43"/>
        <source>Qmmp version: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Version de Qmmp&#xa0;: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="45"/>
        <source>Developers:</source>
        <translation>Développeurs&#xa0;:</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="46"/>
        <source>Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="48"/>
        <source>Translators:</source>
        <translation>Traducteurs&#xa0;:</translation>
    </message>
    <message>
        <location filename="../aboutqsuidialog.cpp" line="44"/>
        <source>Simple user interface based on standard widgets set.</source>
        <translation>Interface utilisateur simple basée sur les widgets standards.</translation>
    </message>
</context>
<context>
    <name>FileSystemBrowser</name>
    <message>
        <location filename="../filesystembrowser.cpp" line="100"/>
        <source>Add to Playlist</source>
        <translation>Ajouter à la liste</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="101"/>
        <source>Replace Playlist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="102"/>
        <source>Change Directory</source>
        <translation>Changer le répertoire</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="105"/>
        <source>Tree View Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="107"/>
        <source>Quick Search</source>
        <translation>Recherche rapide</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="116"/>
        <source>Sort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="110"/>
        <source>By Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="111"/>
        <source>By Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="112"/>
        <source>By Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="113"/>
        <source>By Date</source>
        <translation>par date</translation>
    </message>
    <message>
        <location filename="../filesystembrowser.cpp" line="219"/>
        <source>Select Directory</source>
        <translation>Sélectionner le répertoire</translation>
    </message>
</context>
<context>
    <name>QSUISettings</name>
    <message>
        <location filename="../forms/qsuisettings.ui" line="24"/>
        <source>View</source>
        <translation>Vue</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="36"/>
        <source>Hide on close</source>
        <translation>Masquer à la fermeture</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="43"/>
        <source>Start hidden</source>
        <translation>Démarrer masqué</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="210"/>
        <source>Visualization Colors</source>
        <translation>Couleurs de visualisation</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="216"/>
        <source>Color #1:</source>
        <translation>Couleur #1:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="283"/>
        <source>Color #2:</source>
        <translation>Couleur #2:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="347"/>
        <source>Color #3:</source>
        <translation>Couleur 3#:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="436"/>
        <source>Reset colors</source>
        <translation>Réinitialiser les couleurs</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="703"/>
        <source>Override group colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="806"/>
        <source>Override current track colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="816"/>
        <source>Current track text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="915"/>
        <source>Waveform Seekbar Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="921"/>
        <source>Progress bar:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1010"/>
        <source>RMS:</source>
        <extracomment>Root mean square</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1055"/>
        <source>Waveform:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1096"/>
        <source>Fonts</source>
        <translation>Polices</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1102"/>
        <source>Use system fonts</source>
        <translation>Utiliser les polices systèmes</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1176"/>
        <source>Playlist:</source>
        <translation>Liste&#xa0;:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="62"/>
        <source>...</source>
        <translation>…</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="30"/>
        <source>Main Window</source>
        <translation>Fenêtre principale</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="52"/>
        <source>Window title format:</source>
        <translation>Format du titre de la fenêtre&#xa0;:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="74"/>
        <source>Song Display</source>
        <translation>Affichage du morceau</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="80"/>
        <source>Show protocol</source>
        <translation>Montrer le protocole</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="148"/>
        <source>Show song numbers</source>
        <translation>Montrer les numéros des morceaux</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="155"/>
        <source>Show song lengths</source>
        <translation>Montrer les durées des morceaux</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="165"/>
        <source>Align song numbers</source>
        <translation>Aligner les numéros des morceaux</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="87"/>
        <source>Show anchor</source>
        <translation>Montrer l&apos;ancre</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="101"/>
        <source>Show popup information</source>
        <translation>Montrer la fenêtre d&apos;information</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="113"/>
        <source>Edit template</source>
        <translation>Éditer le patron</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1243"/>
        <source>Reset fonts</source>
        <translation>Réinitialiser les polices</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1192"/>
        <source>Column headers:</source>
        <translation>Noms des colonnes&#xa0;:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1258"/>
        <source>Tab names:</source>
        <translation>Noms des onglets&#xa0;:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1339"/>
        <source>Miscellaneous</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="94"/>
        <source>Show splitters</source>
        <translation>Afficher les séparateurs</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="142"/>
        <source>Single Column Mode</source>
        <translation>Mode colonne unique</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="189"/>
        <source>Colors</source>
        <translation>Couleurs</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="251"/>
        <source>Peaks:</source>
        <translation>Pics&#xa0;:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="315"/>
        <location filename="../forms/qsuisettings.ui" line="1003"/>
        <source>Background:</source>
        <translation>Arrière-plan&#xa0;:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="448"/>
        <source>Playlist Colors</source>
        <translation>Couleurs des listes</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="470"/>
        <source>Background #1:</source>
        <translation>Arrière-plan n°1&#xa0;:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="601"/>
        <source>Normal text:</source>
        <translation>Texte normal&#xa0;:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="569"/>
        <source>Background #2:</source>
        <translation>Arrière-plan n°2 :</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="633"/>
        <source>Highlighted background:</source>
        <translation>Arrière-plan en surbrillance&#xa0;:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="665"/>
        <source>Highlighted text:</source>
        <translation>Texte en surbrillance&#xa0;:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="867"/>
        <source>Current track background:</source>
        <translation>Fond de la piste courante</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1268"/>
        <source>Groups:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1275"/>
        <source>Extra group row:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1372"/>
        <source>Tab position:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1409"/>
        <source>Toolbars</source>
        <translation>Barres d&apos;outils</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1417"/>
        <source>Icon size:</source>
        <translation>Taille d&apos;icône :</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1442"/>
        <source>Customize...</source>
        <translation>Personnaliser...</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="454"/>
        <source>Use system colors</source>
        <translation>Utiliser les couleurs systèmes</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="761"/>
        <source>Group background:</source>
        <translation>Arrière-plan de groupe&#xa0;:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="713"/>
        <source>Group text:</source>
        <translation>Texte de groupe&#xa0;:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="515"/>
        <source>Splitter:</source>
        <translation>Séparateur:</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1345"/>
        <source>Tabs</source>
        <translation>Onglets</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1351"/>
        <source>Show close buttons</source>
        <translation>Montrer les boutons de fermeture</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1358"/>
        <source>Show tab list menu</source>
        <translation>Montrer le menu de liste des onglets</translation>
    </message>
    <message>
        <location filename="../forms/qsuisettings.ui" line="1365"/>
        <source>Show &apos;New Playlist&apos; button</source>
        <translation>Montrer le bouton «&#xa0;Nouvelle liste&#xa0;»</translation>
    </message>
</context>
<context>
    <name>QSUIVisualization</name>
    <message>
        <location filename="../qsuivisualization.cpp" line="125"/>
        <source>Cover</source>
        <translation>Couverture</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="128"/>
        <source>Visualization Mode</source>
        <translation>Mode visualisation</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="131"/>
        <source>Analyzer</source>
        <translation>Analyseur</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="132"/>
        <source>Scope</source>
        <translation>Oscilloscope</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="139"/>
        <source>Analyzer Mode</source>
        <translation>Mode analyseur</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="141"/>
        <source>Cells</source>
        <translation>Cellules</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="142"/>
        <source>Lines</source>
        <translation>Lignes</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="150"/>
        <source>Peaks</source>
        <translation>Pics</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="153"/>
        <source>Refresh Rate</source>
        <translation>Taux de rafraîchissement</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="156"/>
        <source>50 fps</source>
        <translation>50 ips</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="157"/>
        <source>25 fps</source>
        <translation>25 ips</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="158"/>
        <source>10 fps</source>
        <translation>10 ips</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="159"/>
        <source>5 fps</source>
        <translation>5 ips</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="166"/>
        <source>Analyzer Falloff</source>
        <translation>Retombée de l&apos;analyseur</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="169"/>
        <location filename="../qsuivisualization.cpp" line="183"/>
        <source>Slowest</source>
        <translation>Très lente</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="170"/>
        <location filename="../qsuivisualization.cpp" line="184"/>
        <source>Slow</source>
        <translation>Lente</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="171"/>
        <location filename="../qsuivisualization.cpp" line="185"/>
        <source>Medium</source>
        <translation>Moyenne</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="172"/>
        <location filename="../qsuivisualization.cpp" line="186"/>
        <source>Fast</source>
        <translation>Rapide</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="173"/>
        <location filename="../qsuivisualization.cpp" line="187"/>
        <source>Fastest</source>
        <translation>Très rapide</translation>
    </message>
    <message>
        <location filename="../qsuivisualization.cpp" line="180"/>
        <source>Peaks Falloff</source>
        <translation>Retombée des pics</translation>
    </message>
</context>
<context>
    <name>QSUiActionManager</name>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="43"/>
        <source>&amp;Play</source>
        <translation>&amp;Jouer</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="43"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="44"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="44"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="45"/>
        <source>&amp;Stop</source>
        <translation>&amp;Arrêter</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="45"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="46"/>
        <source>&amp;Previous</source>
        <translation>&amp;Précédent</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="46"/>
        <source>Z</source>
        <translation>Z</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="47"/>
        <source>&amp;Next</source>
        <translation>&amp;Suivant</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="47"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="48"/>
        <source>&amp;Play/Pause</source>
        <translation>&amp;Jouer / Arrêter</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="48"/>
        <source>Space</source>
        <translation>Espace</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="49"/>
        <source>&amp;Jump to Track</source>
        <translation>&amp;Sauter à la piste</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="49"/>
        <source>J</source>
        <translation>J</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="50"/>
        <source>&amp;Play Files</source>
        <translation>&amp;Jouer les fichiers</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="50"/>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="51"/>
        <source>&amp;Record</source>
        <translation>&amp;Enregistrer</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="52"/>
        <source>&amp;Repeat Playlist</source>
        <translation>&amp;Répéter la liste</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="52"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="53"/>
        <source>&amp;Repeat Track</source>
        <translation>&amp;Répéter la piste</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="53"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="54"/>
        <source>&amp;Shuffle</source>
        <translation>&amp;Mélanger</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="54"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="55"/>
        <source>&amp;No Playlist Advance</source>
        <translation>&amp;Ne pas avancer dans la liste</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="55"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="56"/>
        <source>&amp;Transit between playlists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="57"/>
        <source>&amp;Stop After Selected</source>
        <translation>&amp;Arrêter après la sélection</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="57"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="58"/>
        <source>&amp;Clear Queue</source>
        <translation>&amp;Effacer la file</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="58"/>
        <source>Alt+Q</source>
        <translation>Alt+Q</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="60"/>
        <source>Always on Top</source>
        <translation>Toujours au dessus</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="61"/>
        <source>Put on All Workspaces</source>
        <translation>Placer sur tous les bureaux</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="67"/>
        <source>Show Tabs</source>
        <translation>Montrer les onglets</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="68"/>
        <source>Block Floating Panels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="69"/>
        <source>Block Toolbars</source>
        <translation>Bloquer les barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="71"/>
        <source>Volume &amp;+</source>
        <translation>Volume &amp;+</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="71"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="72"/>
        <source>Volume &amp;-</source>
        <translation>Volume &amp;-</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="72"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="73"/>
        <source>&amp;Mute</source>
        <translation>&amp;Couper le son</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="73"/>
        <source>M</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="75"/>
        <source>&amp;Add File</source>
        <translation>@Ajouter un fichier</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="75"/>
        <source>F</source>
        <translation>F</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="76"/>
        <source>&amp;Add Directory</source>
        <translation>&amp;Ajouter un dossier</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="76"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="77"/>
        <source>&amp;Add Url</source>
        <translation>&amp;Ajouter une adresse internet (URL)</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="77"/>
        <source>U</source>
        <translation>U</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="78"/>
        <source>&amp;Remove Selected</source>
        <translation>&amp;Supprimer la sélection</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="78"/>
        <source>Del</source>
        <translation>Suppr</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="79"/>
        <source>&amp;Remove All</source>
        <translation>&amp;Tout supprimer</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="80"/>
        <source>&amp;Remove Unselected</source>
        <translation>&amp;Ne garder que la sélection</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="81"/>
        <source>Remove unavailable files</source>
        <translation>Supprimer les fichiers non disponibles</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="82"/>
        <source>Remove duplicates</source>
        <translation>Supprimer les fichiers en double</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="83"/>
        <source>Refresh</source>
        <translation>Rafraîchir</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="84"/>
        <source>&amp;Queue Toggle</source>
        <translation>&amp;Activer mettre en file</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="84"/>
        <source>Q</source>
        <translation>Q</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="85"/>
        <source>Invert Selection</source>
        <translation>Inverser la sélection</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="86"/>
        <source>&amp;Select None</source>
        <translation>&amp;Ne rien sélectionner</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="87"/>
        <source>&amp;Select All</source>
        <translation>&amp;Tout sélectionner</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="87"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="88"/>
        <source>&amp;View Track Details</source>
        <translation>&amp;Voir les détails du morceau</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="88"/>
        <source>Alt+I</source>
        <translation>Alt+I</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="89"/>
        <source>&amp;New List</source>
        <translation>&amp;Nouvelle liste</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="89"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="90"/>
        <source>&amp;Delete List</source>
        <translation>&amp;Supprimer la liste</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="90"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="91"/>
        <source>&amp;Load List</source>
        <translation>&amp;Charger la lsite</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="91"/>
        <source>O</source>
        <translation>O</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="92"/>
        <source>&amp;Save List</source>
        <translation>&amp;Enregistrer la liste</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="92"/>
        <source>Shift+S</source>
        <translation>Shift+S</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="93"/>
        <source>&amp;Rename List</source>
        <translation>&amp;Renommer la liste</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="93"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="94"/>
        <source>&amp;Select Next Playlist</source>
        <translation>&amp;Sélectionner la liste suivante</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="94"/>
        <source>Ctrl+PgDown</source>
        <translation>Ctrl+PageBas</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="95"/>
        <source>&amp;Select Previous Playlist</source>
        <translation>&amp;Sélectionner la liste précédente</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="95"/>
        <source>Ctrl+PgUp</source>
        <translation>Ctrl+PageHaut</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="96"/>
        <source>&amp;Group Tracks</source>
        <translation>&amp;Grouper les pistes</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="96"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="97"/>
        <source>&amp;Show Column Headers</source>
        <translation>&amp;Montrer les noms de colonnes</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="97"/>
        <source>Ctrl+H</source>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="99"/>
        <source>&amp;Equalizer</source>
        <translation>&amp;Égaliseur</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="99"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="100"/>
        <source>&amp;Settings</source>
        <translation>&amp;Configuration</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="100"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="101"/>
        <source>Application Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="102"/>
        <source>&amp;About Ui</source>
        <translation>&amp;À propos de l&apos;interface utilisateur</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="103"/>
        <source>&amp;About</source>
        <translation>&amp;À propos</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="104"/>
        <source>&amp;About Qt</source>
        <translation>&amp;À propos de Qt</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="105"/>
        <source>&amp;Exit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="105"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../qsuiactionmanager.cpp" line="310"/>
        <source>Toolbar</source>
        <translation>Barre d&apos;outils</translation>
    </message>
</context>
<context>
    <name>QSUiCoverWidget</name>
    <message>
        <location filename="../qsuicoverwidget.cpp" line="32"/>
        <source>&amp;Save As...</source>
        <translation>&amp;Enregistrer sous…</translation>
    </message>
    <message>
        <location filename="../qsuicoverwidget.cpp" line="65"/>
        <source>Save Cover As</source>
        <translation>Enregistrer la couverture sous…</translation>
    </message>
    <message>
        <location filename="../qsuicoverwidget.cpp" line="67"/>
        <source>Images</source>
        <translation>Images</translation>
    </message>
</context>
<context>
    <name>QSUiEqualizer</name>
    <message>
        <location filename="../qsuiequalizer.cpp" line="39"/>
        <source>Equalizer</source>
        <translation>Égaliseur</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="48"/>
        <source>Enable equalizer</source>
        <translation>Activer l&apos;égaliseur</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="54"/>
        <source>Preset:</source>
        <translation>Préréglage&#xa0;:</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="62"/>
        <source>Save</source>
        <translation>Sauver</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="66"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="70"/>
        <source>Reset</source>
        <translation>Rétablir</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="83"/>
        <source>Preamp</source>
        <translation>Préampli</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="100"/>
        <location filename="../qsuiequalizer.cpp" line="195"/>
        <source>%1dB</source>
        <translation>%1dB</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="102"/>
        <location filename="../qsuiequalizer.cpp" line="193"/>
        <source>+%1dB</source>
        <translation>+%1dB</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="148"/>
        <source>preset</source>
        <translation>préréglage</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="218"/>
        <source>Overwrite Request</source>
        <translation>Demande d&apos;écriture</translation>
    </message>
    <message>
        <location filename="../qsuiequalizer.cpp" line="219"/>
        <source>Preset &apos;%1&apos; already exists. Overwrite?</source>
        <translation>Le préréglage «&#xa0;%1&#xa0;» existe déjà. Écrire par dessus&#xa0;?</translation>
    </message>
</context>
<context>
    <name>QSUiFactory</name>
    <message>
        <location filename="../qsuifactory.cpp" line="32"/>
        <source>Simple User Interface</source>
        <translation>Interface utilisateur simple</translation>
    </message>
</context>
<context>
    <name>QSUiHotkeyEditor</name>
    <message>
        <location filename="../forms/qsuihotkeyeditor.ui" line="33"/>
        <source>Change shortcut...</source>
        <translation>Changer le raccourci</translation>
    </message>
    <message>
        <location filename="../forms/qsuihotkeyeditor.ui" line="40"/>
        <source>Reset</source>
        <translation>Rétablir</translation>
    </message>
    <message>
        <location filename="../forms/qsuihotkeyeditor.ui" line="54"/>
        <source>Action</source>
        <translation>Action</translation>
    </message>
    <message>
        <location filename="../forms/qsuihotkeyeditor.ui" line="59"/>
        <source>Shortcut</source>
        <translation>Raccourci</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="56"/>
        <source>Reset Shortcuts</source>
        <translation>Réinitialiser les raccourcis</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="57"/>
        <source>Do you want to restore default shortcuts?</source>
        <translation>Voulez-vous restaurer les raccourcis par défaut&#xa0;?</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="69"/>
        <source>Playback</source>
        <translation>Lecture</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="75"/>
        <source>View</source>
        <translation>Vue</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="81"/>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="87"/>
        <source>Playlist</source>
        <translation>Playlist</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="93"/>
        <source>Misc</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../qsuihotkeyeditor.cpp" line="101"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
</context>
<context>
    <name>QSUiMainWindow</name>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="14"/>
        <location filename="../qsuimainwindow.cpp" line="917"/>
        <source>Qmmp</source>
        <translation>Qmmp</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="35"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="40"/>
        <source>&amp;Tools</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="45"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="50"/>
        <source>&amp;Edit</source>
        <translation>&amp;Éditer</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="55"/>
        <source>&amp;Playback</source>
        <translation>&amp;Rejouer</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="60"/>
        <source>&amp;View</source>
        <translation>&amp;Vue</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="84"/>
        <location filename="../forms/qsuimainwindow.ui" line="249"/>
        <source>Visualization</source>
        <translation>Visualisation</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="99"/>
        <source>Files</source>
        <translation>Fichiers</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="114"/>
        <source>Cover</source>
        <translation>Couverture</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="123"/>
        <source>Playlists</source>
        <translation>Listes</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="135"/>
        <source>Waveform Seek Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="149"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="159"/>
        <source>Play</source>
        <translation>Jouer</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="169"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="179"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="189"/>
        <source>Stop</source>
        <translation>Arrêter</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="194"/>
        <source>&amp;Add File</source>
        <translation>@Ajouter un fichier</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="199"/>
        <source>&amp;Remove All</source>
        <translation>&amp;Tout supprimer</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="204"/>
        <source>New Playlist</source>
        <translation>Nouvelle liste</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="209"/>
        <source>Remove Playlist</source>
        <translation>Supprimer la liste</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="214"/>
        <source>&amp;Add Directory</source>
        <translation>&amp;Ajouter un dossier</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="219"/>
        <source>&amp;Exit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="224"/>
        <source>About</source>
        <translation>A propos</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="229"/>
        <source>About Qt</source>
        <translation>À propos de Qt</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="234"/>
        <source>&amp;Select All</source>
        <translation>&amp;Tout sélectionner</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="239"/>
        <source>&amp;Remove Selected</source>
        <translation>&amp;Supprimer la sélection</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="244"/>
        <source>&amp;Remove Unselected</source>
        <translation>&amp;Ne garder que la sélection</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="254"/>
        <source>Settings</source>
        <translation>&amp;Réglages</translation>
    </message>
    <message>
        <location filename="../forms/qsuimainwindow.ui" line="259"/>
        <location filename="../qsuimainwindow.cpp" line="295"/>
        <source>Rename Playlist</source>
        <translation>Renommer la liste</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="86"/>
        <source>&amp;Copy Selection To</source>
        <translation>&amp;Copier la sélection vers</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="295"/>
        <source>Playlist name:</source>
        <translation>Nom de la liste : </translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="335"/>
        <source>Appearance</source>
        <translation>Apparence</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="336"/>
        <source>Shortcuts</source>
        <translation>Raccourcis</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="415"/>
        <source>Menu Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="434"/>
        <source>Add new playlist</source>
        <translation>Ajouter une nouvelle liste</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="440"/>
        <source>Show all tabs</source>
        <translation>Montrer tous les onglets</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="467"/>
        <source>Ctrl+0</source>
        <translation>Ctrl+0</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="473"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="478"/>
        <source>Position</source>
        <translation>Position</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="480"/>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="482"/>
        <source>Balance</source>
        <translation>Balance</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="485"/>
        <source>Quick Search</source>
        <translation>Recherche rapide</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="543"/>
        <source>Edit Toolbars</source>
        <translation>Editer la barre des outils</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="545"/>
        <source>Sort List</source>
        <translation>Trier la liste</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="547"/>
        <location filename="../qsuimainwindow.cpp" line="563"/>
        <source>By Title</source>
        <translation>par titre</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="548"/>
        <location filename="../qsuimainwindow.cpp" line="564"/>
        <source>By Album</source>
        <translation>par album</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="549"/>
        <location filename="../qsuimainwindow.cpp" line="565"/>
        <source>By Artist</source>
        <translation>par artiste</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="550"/>
        <location filename="../qsuimainwindow.cpp" line="566"/>
        <source>By Album Artist</source>
        <translation>par artiste d&apos;album</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="551"/>
        <location filename="../qsuimainwindow.cpp" line="567"/>
        <source>By Filename</source>
        <translation>par nom de fichier</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="552"/>
        <location filename="../qsuimainwindow.cpp" line="568"/>
        <source>By Path + Filename</source>
        <translation>par emplacement + nom de fichier</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="553"/>
        <location filename="../qsuimainwindow.cpp" line="569"/>
        <source>By Date</source>
        <translation>par date</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="554"/>
        <location filename="../qsuimainwindow.cpp" line="570"/>
        <source>By Track Number</source>
        <translation>par numéro de piste</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="555"/>
        <location filename="../qsuimainwindow.cpp" line="571"/>
        <source>By Disc Number</source>
        <translation>par numéro de disque</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="556"/>
        <location filename="../qsuimainwindow.cpp" line="572"/>
        <source>By File Creation Date</source>
        <translation>par date de création du fichier</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="557"/>
        <location filename="../qsuimainwindow.cpp" line="573"/>
        <source>By File Modification Date</source>
        <translation>par date de modification du fichier</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="558"/>
        <source>By Group</source>
        <translation>par groupe</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="561"/>
        <source>Sort Selection</source>
        <translation>trier la sélection</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="577"/>
        <source>Randomize List</source>
        <translation>Mélanger la liste</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="579"/>
        <source>Reverse List</source>
        <translation>Inverser la lsite</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="620"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../qsuimainwindow.cpp" line="938"/>
        <source>&amp;New PlayList</source>
        <translation>&amp;Nouvelle liste</translation>
    </message>
</context>
<context>
    <name>QSUiPlayListBrowser</name>
    <message>
        <location filename="../qsuiplaylistbrowser.cpp" line="62"/>
        <source>Quick Search</source>
        <translation>Recherche rapide</translation>
    </message>
</context>
<context>
    <name>QSUiPlayListHeader</name>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="54"/>
        <source>Add Column</source>
        <translation>Ajouter une colonne</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="55"/>
        <source>Edit Column</source>
        <translation>Éditer la colonne</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="56"/>
        <source>Show Queue/Protocol</source>
        <translation>Montrer la file / le protocole</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="58"/>
        <source>Auto-resize</source>
        <translation>Redimensionnement automatique</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="61"/>
        <source>Alignment</source>
        <translation>Alignement</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="62"/>
        <source>Left</source>
        <comment>alignment</comment>
        <translation>Gauche</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="63"/>
        <source>Right</source>
        <comment>alignment</comment>
        <translation>Droite</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="64"/>
        <source>Center</source>
        <comment>alignment</comment>
        <translation>Centre</translation>
    </message>
    <message>
        <location filename="../qsuiplaylistheader.cpp" line="74"/>
        <source>Remove Column</source>
        <translation>Supprimer la colonne</translation>
    </message>
</context>
<context>
    <name>QSUiPopupSettings</name>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="14"/>
        <source>Popup Information Settings</source>
        <translation>Fenêtre d&apos;information de la configuration</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="29"/>
        <source>Template</source>
        <translation>Patron</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="58"/>
        <source>Reset</source>
        <translation>Rétablir</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="65"/>
        <source>Insert</source>
        <translation>Insérer</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="75"/>
        <source>Show cover</source>
        <translation>Montrer la couverture</translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="89"/>
        <source>Cover size:</source>
        <translation>Taille de la couverture : </translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="115"/>
        <source>Transparency:</source>
        <translation>Transparence : </translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="145"/>
        <source>Delay:</source>
        <translation>Délai : </translation>
    </message>
    <message>
        <location filename="../forms/qsuipopupsettings.ui" line="165"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
</context>
<context>
    <name>QSUiSettings</name>
    <message>
        <location filename="../qsuisettings.cpp" line="41"/>
        <source>Default</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="42"/>
        <source>16x16</source>
        <translation>16x16</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="43"/>
        <source>22x22</source>
        <translation>22x22</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="44"/>
        <source>32x32</source>
        <translation>32x32</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="45"/>
        <source>48x48</source>
        <translation>48x48</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="46"/>
        <source>64x64</source>
        <translation>64x64</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="48"/>
        <source>Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="49"/>
        <source>Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="50"/>
        <source>Left</source>
        <translation>Gauche</translation>
    </message>
    <message>
        <location filename="../qsuisettings.cpp" line="51"/>
        <source>Right</source>
        <translation>Droite</translation>
    </message>
</context>
<context>
    <name>QSUiStatusBar</name>
    <message>
        <location filename="../qsuistatusbar.cpp" line="68"/>
        <source>tracks: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="69"/>
        <source>total time: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="87"/>
        <source>Playing</source>
        <translation>En cours</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="87"/>
        <source>Paused</source>
        <translation>En pause</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="102"/>
        <source>Buffering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="127"/>
        <source>Stopped</source>
        <translation>Arrêté</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="139"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="147"/>
        <source>Buffering: %1%</source>
        <translation>Mise en tampon : %1 %</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="152"/>
        <source>%1 bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="154"/>
        <source>mono</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="156"/>
        <source>stereo</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../qsuistatusbar.cpp" line="158"/>
        <source>%n channels</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="159"/>
        <source>%1 Hz</source>
        <translation>%1 Hz</translation>
    </message>
    <message>
        <location filename="../qsuistatusbar.cpp" line="164"/>
        <source>%1 kbps</source>
        <translation>%1 Kbps</translation>
    </message>
</context>
<context>
    <name>QSUiWaveformSeekBar</name>
    <message>
        <location filename="../qsuiwaveformseekbar.cpp" line="335"/>
        <source>2 Channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsuiwaveformseekbar.cpp" line="338"/>
        <source>RMS</source>
        <extracomment>Root mean square</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBarEditor</name>
    <message>
        <location filename="../forms/toolbareditor.ui" line="14"/>
        <source>ToolBar Editor</source>
        <translation>Éditeur de barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="62"/>
        <source>Reset</source>
        <translation>Réinitialiser</translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="199"/>
        <source>Toolbar:</source>
        <translation>Barre d&apos;outils:</translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="222"/>
        <source>&amp;Create</source>
        <translation>&amp;Créer</translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="238"/>
        <source>Re&amp;name</source>
        <translation>Re&amp;nommer</translation>
    </message>
    <message>
        <location filename="../forms/toolbareditor.ui" line="254"/>
        <source>&amp;Remove</source>
        <translation>&amp;Enlever</translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="100"/>
        <location filename="../toolbareditor.cpp" line="198"/>
        <source>Separator</source>
        <translation>Séparateur</translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="248"/>
        <source>Toolbar</source>
        <translation>Barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="250"/>
        <source>Toolbar %1</source>
        <translation>Barre des outils %&amp;</translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="264"/>
        <source>Rename Toolbar</source>
        <translation>Renommer la barre des outils</translation>
    </message>
    <message>
        <location filename="../toolbareditor.cpp" line="264"/>
        <source>Toolbar name:</source>
        <translation>Nom de la barre des outils:</translation>
    </message>
</context>
<context>
    <name>VolumeSlider</name>
    <message>
        <location filename="../volumeslider.cpp" line="110"/>
        <source>%1: %2%</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
