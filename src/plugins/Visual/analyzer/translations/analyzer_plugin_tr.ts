<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>Analyzer</name>
    <message>
        <location filename="../analyzer.cpp" line="36"/>
        <source>Qmmp Analyzer</source>
        <translation>Qmmp Çözümleyici</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="320"/>
        <source>Peaks</source>
        <translation>Tepeler</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="323"/>
        <source>Refresh Rate</source>
        <translation>Tazeleme Oranı</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="326"/>
        <source>50 fps</source>
        <translation>50 fps</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="327"/>
        <source>25 fps</source>
        <translation>25 fps</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="328"/>
        <source>10 fps</source>
        <translation>10 fps</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="329"/>
        <source>5 fps</source>
        <translation>5 fps</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="336"/>
        <source>Analyzer Falloff</source>
        <translation>Çözümleyici Düşüşü</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="339"/>
        <location filename="../analyzer.cpp" line="353"/>
        <source>Slowest</source>
        <translation>En yavaş</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="340"/>
        <location filename="../analyzer.cpp" line="354"/>
        <source>Slow</source>
        <translation>Yavaş</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="341"/>
        <location filename="../analyzer.cpp" line="355"/>
        <source>Medium</source>
        <translation>Orta</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="342"/>
        <location filename="../analyzer.cpp" line="356"/>
        <source>Fast</source>
        <translation>Hızlı</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="343"/>
        <location filename="../analyzer.cpp" line="357"/>
        <source>Fastest</source>
        <translation>En  hızlı</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="350"/>
        <source>Peaks Falloff</source>
        <translation>Tepe Düşüşü</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="365"/>
        <location filename="../analyzer.cpp" line="367"/>
        <source>&amp;Full Screen</source>
        <translation>&amp;Tam Ekran</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="365"/>
        <location filename="../analyzer.cpp" line="367"/>
        <source>F</source>
        <translation>F</translation>
    </message>
</context>
<context>
    <name>AnalyzerColorWidget</name>
    <message>
        <location filename="../analyzercolorwidget.cpp" line="37"/>
        <source>Select Color</source>
        <translation>Renk Seçin</translation>
    </message>
</context>
<context>
    <name>AnalyzerSettingsDialog</name>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="14"/>
        <source>Analyzer Plugin Settings</source>
        <translation>Çözümleyici Eklentisi Ayarları</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="35"/>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="41"/>
        <source>Cells size:</source>
        <translation>Hücre boyutu:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="101"/>
        <source>Colors</source>
        <translation>Renkler</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="107"/>
        <source>Peaks:</source>
        <translation>Tepeler:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="139"/>
        <source>Analyzer #1:</source>
        <translation>Çözümleyici #1:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="171"/>
        <source>Background:</source>
        <translation>Arka zemin:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="203"/>
        <source>Analyzer #2:</source>
        <translation>Çözümleyici #2:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="248"/>
        <source>Analyzer #3:</source>
        <translation>Çözümleyici #2:</translation>
    </message>
</context>
<context>
    <name>VisualAnalyzerFactory</name>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="30"/>
        <source>Analyzer Plugin</source>
        <translation>Çözümleyici Eklentisi</translation>
    </message>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="49"/>
        <source>About Analyzer Visual Plugin</source>
        <translation>Çözümleyici Görsel Eklentisi Hakkında</translation>
    </message>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="50"/>
        <source>Qmmp Analyzer Visual Plugin</source>
        <translation>Qmmp Çözümleyici Görsel Eklentisi</translation>
    </message>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Yazan: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
