<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>Analyzer</name>
    <message>
        <location filename="../analyzer.cpp" line="36"/>
        <source>Qmmp Analyzer</source>
        <translation>Frekvenční analyzátor Qmmp</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="320"/>
        <source>Peaks</source>
        <translation>Špičky</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="323"/>
        <source>Refresh Rate</source>
        <translation>Obnovovací frekvence</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="326"/>
        <source>50 fps</source>
        <translation>50 s/s</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="327"/>
        <source>25 fps</source>
        <translation>25 s/s</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="328"/>
        <source>10 fps</source>
        <translation>10 s/s</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="329"/>
        <source>5 fps</source>
        <translation>5 s/s</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="336"/>
        <source>Analyzer Falloff</source>
        <translation>Pokles analyzéru</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="339"/>
        <location filename="../analyzer.cpp" line="353"/>
        <source>Slowest</source>
        <translation>Nejpomalejší</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="340"/>
        <location filename="../analyzer.cpp" line="354"/>
        <source>Slow</source>
        <translation>Pomalý</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="341"/>
        <location filename="../analyzer.cpp" line="355"/>
        <source>Medium</source>
        <translation>Střední</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="342"/>
        <location filename="../analyzer.cpp" line="356"/>
        <source>Fast</source>
        <translation>Rychlý</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="343"/>
        <location filename="../analyzer.cpp" line="357"/>
        <source>Fastest</source>
        <translation>Nejrychlejší</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="350"/>
        <source>Peaks Falloff</source>
        <translation>Pokles špiček</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="365"/>
        <location filename="../analyzer.cpp" line="367"/>
        <source>&amp;Full Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="365"/>
        <location filename="../analyzer.cpp" line="367"/>
        <source>F</source>
        <translation>F</translation>
    </message>
</context>
<context>
    <name>AnalyzerColorWidget</name>
    <message>
        <location filename="../analyzercolorwidget.cpp" line="37"/>
        <source>Select Color</source>
        <translation type="unfinished">Vyberte barvu</translation>
    </message>
</context>
<context>
    <name>AnalyzerSettingsDialog</name>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="14"/>
        <source>Analyzer Plugin Settings</source>
        <translation type="unfinished">Nastavení modulu frekvenčního analyzátoru</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="35"/>
        <source>General</source>
        <translation type="unfinished">Obecné</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="41"/>
        <source>Cells size:</source>
        <translation type="unfinished">Velikost polí:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="101"/>
        <source>Colors</source>
        <translation type="unfinished">Barvy</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="107"/>
        <source>Peaks:</source>
        <translation type="unfinished">Špičky:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="139"/>
        <source>Analyzer #1:</source>
        <translation type="unfinished">Analyzátor #1:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="171"/>
        <source>Background:</source>
        <translation type="unfinished">Pozadí:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="203"/>
        <source>Analyzer #2:</source>
        <translation type="unfinished">Analyzátor #2:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="248"/>
        <source>Analyzer #3:</source>
        <translation type="unfinished">Analyzátor #3:</translation>
    </message>
</context>
<context>
    <name>VisualAnalyzerFactory</name>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="30"/>
        <source>Analyzer Plugin</source>
        <translation>Modul frekvenčního analyzátoru</translation>
    </message>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="49"/>
        <source>About Analyzer Visual Plugin</source>
        <translation>O modulu frekvenčního analyzátoru</translation>
    </message>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="50"/>
        <source>Qmmp Analyzer Visual Plugin</source>
        <translation>Modul frekvenčního analyzátoru pro Qmmp</translation>
    </message>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
