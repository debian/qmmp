<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sr_RS">
<context>
    <name>Analyzer</name>
    <message>
        <location filename="../analyzer.cpp" line="36"/>
        <source>Qmmp Analyzer</source>
        <translation>Кумп Анализатор</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="320"/>
        <source>Peaks</source>
        <translation>Врхови</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="323"/>
        <source>Refresh Rate</source>
        <translation>Фреквенција</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="326"/>
        <source>50 fps</source>
        <translation>50 fps</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="327"/>
        <source>25 fps</source>
        <translation>25 fps</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="328"/>
        <source>10 fps</source>
        <translation>10 fps</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="329"/>
        <source>5 fps</source>
        <translation>5 fps</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="336"/>
        <source>Analyzer Falloff</source>
        <translation>Падање анализатора</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="339"/>
        <location filename="../analyzer.cpp" line="353"/>
        <source>Slowest</source>
        <translation>Најспорије</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="340"/>
        <location filename="../analyzer.cpp" line="354"/>
        <source>Slow</source>
        <translation>Споро</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="341"/>
        <location filename="../analyzer.cpp" line="355"/>
        <source>Medium</source>
        <translation>Умерено</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="342"/>
        <location filename="../analyzer.cpp" line="356"/>
        <source>Fast</source>
        <translation>Брзо</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="343"/>
        <location filename="../analyzer.cpp" line="357"/>
        <source>Fastest</source>
        <translation>Најбрже</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="350"/>
        <source>Peaks Falloff</source>
        <translation>Падање врхова</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="365"/>
        <location filename="../analyzer.cpp" line="367"/>
        <source>&amp;Full Screen</source>
        <translation>&amp;Цео екран</translation>
    </message>
    <message>
        <location filename="../analyzer.cpp" line="365"/>
        <location filename="../analyzer.cpp" line="367"/>
        <source>F</source>
        <translation>F</translation>
    </message>
</context>
<context>
    <name>AnalyzerColorWidget</name>
    <message>
        <location filename="../analyzercolorwidget.cpp" line="37"/>
        <source>Select Color</source>
        <translation type="unfinished">Изабери боју</translation>
    </message>
</context>
<context>
    <name>AnalyzerSettingsDialog</name>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="14"/>
        <source>Analyzer Plugin Settings</source>
        <translation type="unfinished">Поставке анализатора</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="35"/>
        <source>General</source>
        <translation type="unfinished">Опште</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="41"/>
        <source>Cells size:</source>
        <translation type="unfinished">Величина ћелија:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="101"/>
        <source>Colors</source>
        <translation type="unfinished">Боје</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="107"/>
        <source>Peaks:</source>
        <translation type="unfinished">Врхови:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="139"/>
        <source>Analyzer #1:</source>
        <translation type="unfinished">Анализатор #1:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="171"/>
        <source>Background:</source>
        <translation type="unfinished">Позадина:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="203"/>
        <source>Analyzer #2:</source>
        <translation type="unfinished">Анализатор #2:</translation>
    </message>
    <message>
        <location filename="../analyzersettingsdialog.ui" line="248"/>
        <source>Analyzer #3:</source>
        <translation type="unfinished">Анализатор #3:</translation>
    </message>
</context>
<context>
    <name>VisualAnalyzerFactory</name>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="30"/>
        <source>Analyzer Plugin</source>
        <translation>Анализатор</translation>
    </message>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="49"/>
        <source>About Analyzer Visual Plugin</source>
        <translation>О визуелном анализатору</translation>
    </message>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="50"/>
        <source>Qmmp Analyzer Visual Plugin</source>
        <translation>Кумп визелни анализатор</translation>
    </message>
    <message>
        <location filename="../visualanalyzerfactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Аутор: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
