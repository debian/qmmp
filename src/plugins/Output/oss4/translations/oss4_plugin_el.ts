<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="el">
<context>
    <name>Oss4SettingsDialog</name>
    <message>
        <location filename="../oss4settingsdialog.ui" line="14"/>
        <source>OSS4 Plugin Settings</source>
        <translation>Ρυθμίσεις πρόσθετου OSS4</translation>
    </message>
    <message>
        <location filename="../oss4settingsdialog.ui" line="61"/>
        <source>Device:</source>
        <translation>Συσκευή:</translation>
    </message>
    <message>
        <location filename="../oss4settingsdialog.cpp" line="69"/>
        <source>Default (%1)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutputOSS4Factory</name>
    <message>
        <location filename="../outputoss4factory.cpp" line="36"/>
        <source>OSS4 Plugin</source>
        <translation>Πρόσθετο OSS4</translation>
    </message>
    <message>
        <location filename="../outputoss4factory.cpp" line="55"/>
        <source>About OSS4 Output Plugin</source>
        <translation>Σχετικά με το πρόσθετο OSS4</translation>
    </message>
    <message>
        <location filename="../outputoss4factory.cpp" line="56"/>
        <source>Qmmp OSS4 Output Plugin</source>
        <translation>Qmmp σχετικά με το πρόσθετο OSS4</translation>
    </message>
    <message>
        <location filename="../outputoss4factory.cpp" line="57"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Γράφτηκε από τον: Ilya Kotov &lt;forkotov02@hotmail.ru&gt;</translation>
    </message>
</context>
</TS>
