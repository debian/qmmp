<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>Oss4SettingsDialog</name>
    <message>
        <location filename="../oss4settingsdialog.ui" line="14"/>
        <source>OSS4 Plugin Settings</source>
        <translation>Ustawienia wtyczki OSS4</translation>
    </message>
    <message>
        <location filename="../oss4settingsdialog.ui" line="61"/>
        <source>Device:</source>
        <translation>Urządzenie:</translation>
    </message>
    <message>
        <location filename="../oss4settingsdialog.cpp" line="69"/>
        <source>Default (%1)</source>
        <translation>Domyślne (%1)</translation>
    </message>
</context>
<context>
    <name>OutputOSS4Factory</name>
    <message>
        <location filename="../outputoss4factory.cpp" line="36"/>
        <source>OSS4 Plugin</source>
        <translation>Wtyczka OSS4</translation>
    </message>
    <message>
        <location filename="../outputoss4factory.cpp" line="55"/>
        <source>About OSS4 Output Plugin</source>
        <translation>O wtyczce wyjściowej OSS4</translation>
    </message>
    <message>
        <location filename="../outputoss4factory.cpp" line="56"/>
        <source>Qmmp OSS4 Output Plugin</source>
        <translation>Wtyczka wyjściowa OSS4 dla Qmmp</translation>
    </message>
    <message>
        <location filename="../outputoss4factory.cpp" line="57"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Napisana przez: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
