<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>OutputWASAPIFactory</name>
    <message>
        <location filename="../outputwasapifactory.cpp" line="29"/>
        <source>WASAPI Plugin</source>
        <translation>WASAPI-plug-in</translation>
    </message>
    <message>
        <location filename="../outputwasapifactory.cpp" line="53"/>
        <source>About WASAPI Output Plugin</source>
        <translation>Over de WASAPI-uitvoerplug-in</translation>
    </message>
    <message>
        <location filename="../outputwasapifactory.cpp" line="54"/>
        <source>Qmmp WASAPI Output Plugin</source>
        <translation>WASAPI-uitvoerplug-in voor Qmmp</translation>
    </message>
    <message>
        <location filename="../outputwasapifactory.cpp" line="55"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Auteur: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>WASAPISettingsDialog</name>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="14"/>
        <source>WASAPI Plugin Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="35"/>
        <source>Device:</source>
        <translation>Apparaat:</translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="55"/>
        <source>Buffer size:</source>
        <translation>Buffergrootte:</translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="62"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="78"/>
        <source>Exclusive mode</source>
        <translation>Exclusieve modus</translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.cpp" line="65"/>
        <source>Default</source>
        <translation>Standaard</translation>
    </message>
</context>
</TS>
