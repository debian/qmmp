<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fi">
<context>
    <name>OutputWASAPIFactory</name>
    <message>
        <location filename="../outputwasapifactory.cpp" line="29"/>
        <source>WASAPI Plugin</source>
        <translation>WASAPI Plugin</translation>
    </message>
    <message>
        <location filename="../outputwasapifactory.cpp" line="53"/>
        <source>About WASAPI Output Plugin</source>
        <translation>Tietoja: WASAPI Output Plugin</translation>
    </message>
    <message>
        <location filename="../outputwasapifactory.cpp" line="54"/>
        <source>Qmmp WASAPI Output Plugin</source>
        <translation>Qmmp WASAPI Output Plugin</translation>
    </message>
    <message>
        <location filename="../outputwasapifactory.cpp" line="55"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Kirjoittanut: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>WASAPISettingsDialog</name>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="14"/>
        <source>WASAPI Plugin Settings</source>
        <translation>Asetukset WASAPI Plugin</translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="35"/>
        <source>Device:</source>
        <translation>Laite:</translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="55"/>
        <source>Buffer size:</source>
        <translation>Puskurin koko:</translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="62"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.ui" line="78"/>
        <source>Exclusive mode</source>
        <translation>Varattu tila</translation>
    </message>
    <message>
        <location filename="../wasapisettingsdialog.cpp" line="65"/>
        <source>Default</source>
        <translation>Oletus</translation>
    </message>
</context>
</TS>
