<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>OutputDirectSoundFactory</name>
    <message>
        <location filename="../outputdirectsoundfactory.cpp" line="28"/>
        <source>DirectSound Plugin</source>
        <translation>Втулок DirectSound</translation>
    </message>
    <message>
        <location filename="../outputdirectsoundfactory.cpp" line="53"/>
        <source>About DirectSound Output Plugin</source>
        <translation>Про втулок виводу DirectSound</translation>
    </message>
    <message>
        <location filename="../outputdirectsoundfactory.cpp" line="54"/>
        <source>Qmmp DirectSound Output Plugin</source>
        <translation>Втулок виводу DirectSound для Qmmp</translation>
    </message>
    <message>
        <location filename="../outputdirectsoundfactory.cpp" line="55"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
