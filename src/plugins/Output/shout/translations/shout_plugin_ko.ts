<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko">
<context>
    <name>OutputShoutFactory</name>
    <message>
        <location filename="../outputshoutfactory.cpp" line="39"/>
        <source>Icecast Plugin</source>
        <translation>Icecast 플러그인</translation>
    </message>
    <message>
        <location filename="../outputshoutfactory.cpp" line="63"/>
        <source>About Icecast Output Plugin</source>
        <translation>Icecast 출력 플러그인 정보</translation>
    </message>
    <message>
        <location filename="../outputshoutfactory.cpp" line="64"/>
        <source>Qmmp Icecast Output Plugin</source>
        <translation>Qmmp Icecast 출력 플러그인</translation>
    </message>
    <message>
        <location filename="../outputshoutfactory.cpp" line="65"/>
        <source>Compiled against libshout-%1</source>
        <translation>libshout-%1에 대해 컴파일됨</translation>
    </message>
    <message>
        <location filename="../outputshoutfactory.cpp" line="66"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>작성자: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>ShoutSettingsDialog</name>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="14"/>
        <source>Connection Settings</source>
        <translation>연결 설정</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="32"/>
        <source>Host:</source>
        <translation>호스트:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="42"/>
        <source>Port:</source>
        <translation>포트:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="56"/>
        <source>Mount point:</source>
        <translation>마운트 포인트:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="66"/>
        <source>User:</source>
        <translation>사용자:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="76"/>
        <source>Password:</source>
        <translation>암호:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="86"/>
        <source>Quality:</source>
        <translation>품질:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="106"/>
        <source>Sample rate:</source>
        <translation>샘플 레이트:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="136"/>
        <source>Public</source>
        <translation>공개</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="143"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
</context>
</TS>
