<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>OutputShoutFactory</name>
    <message>
        <location filename="../outputshoutfactory.cpp" line="39"/>
        <source>Icecast Plugin</source>
        <translation>Icecast 插件</translation>
    </message>
    <message>
        <location filename="../outputshoutfactory.cpp" line="63"/>
        <source>About Icecast Output Plugin</source>
        <translation>关于 Icecast 输出插件</translation>
    </message>
    <message>
        <location filename="../outputshoutfactory.cpp" line="64"/>
        <source>Qmmp Icecast Output Plugin</source>
        <translation>Qmmp Icecast 输出插件</translation>
    </message>
    <message>
        <location filename="../outputshoutfactory.cpp" line="65"/>
        <source>Compiled against libshout-%1</source>
        <translation>编译依赖 libshout-%1</translation>
    </message>
    <message>
        <location filename="../outputshoutfactory.cpp" line="66"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShoutSettingsDialog</name>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="14"/>
        <source>Connection Settings</source>
        <translation>连接设置</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="32"/>
        <source>Host:</source>
        <translation>主机：</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="42"/>
        <source>Port:</source>
        <translation>端口：</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="56"/>
        <source>Mount point:</source>
        <translation>挂载点：</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="66"/>
        <source>User:</source>
        <translation>用户：</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="76"/>
        <source>Password:</source>
        <translation>密码：</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="86"/>
        <source>Quality:</source>
        <translation>质量：</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="106"/>
        <source>Sample rate:</source>
        <translation>取样率：</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="136"/>
        <source>Public</source>
        <translation>公用</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="143"/>
        <source>Hz</source>
        <translation>赫兹</translation>
    </message>
</context>
</TS>
