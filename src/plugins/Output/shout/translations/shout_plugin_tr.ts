<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>OutputShoutFactory</name>
    <message>
        <location filename="../outputshoutfactory.cpp" line="39"/>
        <source>Icecast Plugin</source>
        <translation>Icecast Eklentisi</translation>
    </message>
    <message>
        <location filename="../outputshoutfactory.cpp" line="63"/>
        <source>About Icecast Output Plugin</source>
        <translation>Icecast Çıktı Eklentisi</translation>
    </message>
    <message>
        <location filename="../outputshoutfactory.cpp" line="64"/>
        <source>Qmmp Icecast Output Plugin</source>
        <translation>Qmmp Icecast Çıktı Eklentisi</translation>
    </message>
    <message>
        <location filename="../outputshoutfactory.cpp" line="65"/>
        <source>Compiled against libshout-%1</source>
        <translation>libshout-%1&apos;e  dayanarak derlendi</translation>
    </message>
    <message>
        <location filename="../outputshoutfactory.cpp" line="66"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Yazan: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>ShoutSettingsDialog</name>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="14"/>
        <source>Connection Settings</source>
        <translation>Bağlantı Ayarları</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="32"/>
        <source>Host:</source>
        <translation>Ev sahibi:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="42"/>
        <source>Port:</source>
        <translation>Giriş kapısı:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="56"/>
        <source>Mount point:</source>
        <translation>Takma noktası:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="66"/>
        <source>User:</source>
        <translation>Kullanıcı:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="76"/>
        <source>Password:</source>
        <translation>Parola:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="86"/>
        <source>Quality:</source>
        <translation>Nitelik:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="106"/>
        <source>Sample rate:</source>
        <translation>Örnekleme oranı:</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="136"/>
        <source>Public</source>
        <translation>Halka açık</translation>
    </message>
    <message>
        <location filename="../shoutsettingsdialog.ui" line="143"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
</context>
</TS>
