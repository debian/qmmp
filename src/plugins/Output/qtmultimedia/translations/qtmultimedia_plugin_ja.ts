<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>OutputQtMultimediaFactory</name>
    <message>
        <location filename="../outputqtmultimediafactory.cpp" line="32"/>
        <source>Qt Multimedia Plugin</source>
        <translation>Qt Multimedia プラグイン</translation>
    </message>
    <message>
        <location filename="../outputqtmultimediafactory.cpp" line="56"/>
        <source>About Qt Multimedia Output Plugin</source>
        <translation>Qt Multimedia 出力プラグインについて</translation>
    </message>
    <message>
        <location filename="../outputqtmultimediafactory.cpp" line="57"/>
        <source>Qmmp Qt Multimedia Output Plugin</source>
        <translation>QMMP Qt Multimedia 出力プラグイン</translation>
    </message>
    <message>
        <location filename="../outputqtmultimediafactory.cpp" line="58"/>
        <source>Written by: Ivan Ponomarev &lt;ivantrue@gmail.com&gt;</source>
        <translation>制作: Ivan Ponomarev &lt;ivantrue@gmail.com&gt;</translation>
    </message>
</context>
<context>
    <name>QtMultimediaSettingsDialog</name>
    <message>
        <location filename="../qtmultimediasettingsdialog.ui" line="14"/>
        <source>Qt Multimedia Plugin Settings</source>
        <translation>Qt Multimedia プラグイン設定</translation>
    </message>
    <message>
        <location filename="../qtmultimediasettingsdialog.ui" line="46"/>
        <source>Device:</source>
        <translation>出力デバイス:</translation>
    </message>
    <message>
        <location filename="../qtmultimediasettingsdialog.cpp" line="36"/>
        <source>Default</source>
        <translation>標準</translation>
    </message>
</context>
</TS>
