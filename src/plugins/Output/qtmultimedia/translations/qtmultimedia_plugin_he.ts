<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="he">
<context>
    <name>OutputQtMultimediaFactory</name>
    <message>
        <location filename="../outputqtmultimediafactory.cpp" line="32"/>
        <source>Qt Multimedia Plugin</source>
        <translation>תוסף מולטימדיה Qt</translation>
    </message>
    <message>
        <location filename="../outputqtmultimediafactory.cpp" line="56"/>
        <source>About Qt Multimedia Output Plugin</source>
        <translation>אודות תוסף פלט מולטימדיה Qt</translation>
    </message>
    <message>
        <location filename="../outputqtmultimediafactory.cpp" line="57"/>
        <source>Qmmp Qt Multimedia Output Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../outputqtmultimediafactory.cpp" line="58"/>
        <source>Written by: Ivan Ponomarev &lt;ivantrue@gmail.com&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QtMultimediaSettingsDialog</name>
    <message>
        <location filename="../qtmultimediasettingsdialog.ui" line="14"/>
        <source>Qt Multimedia Plugin Settings</source>
        <translation type="unfinished">הגדרות תוסף מולטימדיה Qt</translation>
    </message>
    <message>
        <location filename="../qtmultimediasettingsdialog.ui" line="46"/>
        <source>Device:</source>
        <translation type="unfinished">התקן:</translation>
    </message>
    <message>
        <location filename="../qtmultimediasettingsdialog.cpp" line="36"/>
        <source>Default</source>
        <translation type="unfinished">משתמט</translation>
    </message>
</context>
</TS>
