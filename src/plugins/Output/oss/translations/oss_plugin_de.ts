<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>OssSettingsDialog</name>
    <message>
        <location filename="../osssettingsdialog.ui" line="14"/>
        <source>OSS Plugin Settings</source>
        <translation>Einstellungen OSS-Modul</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="33"/>
        <source>Device Settings</source>
        <translation>Geräteinstellungen</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="54"/>
        <source>Audio device</source>
        <translation>Audiogerät</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="70"/>
        <source>Mixer device</source>
        <translation>Mixergerät</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="102"/>
        <source>Advanced Settings</source>
        <translation>Erweiterte Einstellungen</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="123"/>
        <source>Soundcard</source>
        <translation>Soundkarte</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="196"/>
        <source>Buffer time (ms):</source>
        <translation>Pufferzeit (ms):</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="206"/>
        <source>Period time (ms):</source>
        <translation>Zeitraster (ms):</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="236"/>
        <source>PCM over Master</source>
        <translation>PCM over Master</translation>
    </message>
</context>
<context>
    <name>OutputOSSFactory</name>
    <message>
        <location filename="../outputossfactory.cpp" line="36"/>
        <source>OSS Plugin</source>
        <translation>OSS-Modul</translation>
    </message>
    <message>
        <location filename="../outputossfactory.cpp" line="55"/>
        <source>About OSS Output Plugin</source>
        <translation>Über OSS-Ausgabemodul</translation>
    </message>
    <message>
        <location filename="../outputossfactory.cpp" line="56"/>
        <source>Qmmp OSS Output Plugin</source>
        <translation>Qmmp OSS-Ausgabemodul</translation>
    </message>
    <message>
        <location filename="../outputossfactory.cpp" line="57"/>
        <source>Written by: Yuriy Zhuravlev &lt;slalkerg@gmail.com&gt;</source>
        <translation>Geschrieben von: Yuriy Zhuravlev &lt;slalkerg@gmail.com&gt;</translation>
    </message>
    <message>
        <location filename="../outputossfactory.cpp" line="58"/>
        <source>Based on code by: Brad Hughes &lt;bhughes@trolltech.com&gt;</source>
        <translation>Basiert auf Code von: Brad Hughes &lt;bhughes@trolltech.com&gt;</translation>
    </message>
</context>
</TS>
