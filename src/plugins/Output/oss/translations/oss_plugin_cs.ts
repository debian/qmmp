<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>OssSettingsDialog</name>
    <message>
        <location filename="../osssettingsdialog.ui" line="14"/>
        <source>OSS Plugin Settings</source>
        <translation type="unfinished">Nastavení pluginu OSS</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="33"/>
        <source>Device Settings</source>
        <translation type="unfinished">Nastavení zařízení</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="54"/>
        <source>Audio device</source>
        <translation type="unfinished">Zvukové zařízení</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="70"/>
        <source>Mixer device</source>
        <translation type="unfinished">Ovládání hlasitosti</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="102"/>
        <source>Advanced Settings</source>
        <translation type="unfinished">Pokročilá nastavení</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="123"/>
        <source>Soundcard</source>
        <translation type="unfinished">Zvuková karta</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="196"/>
        <source>Buffer time (ms):</source>
        <translation type="unfinished">Velikost bufferu (ms):</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="206"/>
        <source>Period time (ms):</source>
        <translation type="unfinished">Délka periody (ms):</translation>
    </message>
    <message>
        <location filename="../osssettingsdialog.ui" line="236"/>
        <source>PCM over Master</source>
        <translation type="unfinished">PCM přes Master</translation>
    </message>
</context>
<context>
    <name>OutputOSSFactory</name>
    <message>
        <location filename="../outputossfactory.cpp" line="36"/>
        <source>OSS Plugin</source>
        <translation>Modul OSS</translation>
    </message>
    <message>
        <location filename="../outputossfactory.cpp" line="55"/>
        <source>About OSS Output Plugin</source>
        <translation>O modulu OSS</translation>
    </message>
    <message>
        <location filename="../outputossfactory.cpp" line="56"/>
        <source>Qmmp OSS Output Plugin</source>
        <translation>Výstupní modul Qmmp OSS</translation>
    </message>
    <message>
        <location filename="../outputossfactory.cpp" line="57"/>
        <source>Written by: Yuriy Zhuravlev &lt;slalkerg@gmail.com&gt;</source>
        <translation>Autor: Jurij Žuravljov &lt;slalkerg@gmail.com&gt;</translation>
    </message>
    <message>
        <location filename="../outputossfactory.cpp" line="58"/>
        <source>Based on code by: Brad Hughes &lt;bhughes@trolltech.com&gt;</source>
        <translation>Založeno na kódu Brada Hughese &lt;bhughes@trolltech.com&gt;</translation>
    </message>
</context>
</TS>
