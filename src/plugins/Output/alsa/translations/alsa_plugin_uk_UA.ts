<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>AlsaSettingsDialog</name>
    <message>
        <location filename="../alsasettingsdialog.ui" line="14"/>
        <source>ALSA Plugin Settings</source>
        <translation>Налаштування втулка ALSA</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="33"/>
        <source>Device Settings</source>
        <translation>Параметри пристроїв</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="54"/>
        <source>Audio device</source>
        <translation>Авдіопристрій</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="70"/>
        <source>Mixer</source>
        <translation>Мікшер</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="94"/>
        <source>Mixer card:</source>
        <translation>Карта мікшера:</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="104"/>
        <source>Mixer device:</source>
        <translation>Пристрій мікшера:</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="121"/>
        <source>Advanced Settings</source>
        <translation>Додаткові налаштування</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="142"/>
        <source>Soundcard</source>
        <translation>Звукова карта</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="148"/>
        <source>Buffer time (ms):</source>
        <translation>Час буферування (мс):</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="184"/>
        <source>Period time (ms):</source>
        <translation>Час періоду (мс):</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="220"/>
        <source>Use mmap access</source>
        <translation>Використовувати доступ mmap</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="240"/>
        <source>Use snd_pcm_pause function</source>
        <translation>Використовувати функцію snd_pcm_pause</translation>
    </message>
</context>
<context>
    <name>OutputALSAFactory</name>
    <message>
        <location filename="../outputalsafactory.cpp" line="31"/>
        <source>ALSA Plugin</source>
        <translation>Втулок ALSA</translation>
    </message>
    <message>
        <location filename="../outputalsafactory.cpp" line="55"/>
        <source>About ALSA Output Plugin</source>
        <translation>Про втулок виводу ALSA</translation>
    </message>
    <message>
        <location filename="../outputalsafactory.cpp" line="56"/>
        <source>Qmmp ALSA Output Plugin</source>
        <translation>Втулок виводу ALSA для Qmmp</translation>
    </message>
    <message>
        <location filename="../outputalsafactory.cpp" line="57"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
