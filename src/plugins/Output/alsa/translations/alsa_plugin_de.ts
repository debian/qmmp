<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>AlsaSettingsDialog</name>
    <message>
        <location filename="../alsasettingsdialog.ui" line="14"/>
        <source>ALSA Plugin Settings</source>
        <translation>Einstellungen ALSA-Modul</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="33"/>
        <source>Device Settings</source>
        <translation>Geräteinstellungen</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="54"/>
        <source>Audio device</source>
        <translation>Audiogerät</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="70"/>
        <source>Mixer</source>
        <translation>Mixer</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="94"/>
        <source>Mixer card:</source>
        <translation>Mixerkarte:</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="104"/>
        <source>Mixer device:</source>
        <translation>Mixergerät:</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="121"/>
        <source>Advanced Settings</source>
        <translation>Erweiterte Einstellungen</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="142"/>
        <source>Soundcard</source>
        <translation>Soundkarte</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="148"/>
        <source>Buffer time (ms):</source>
        <translation>Pufferzeit (ms):</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="184"/>
        <source>Period time (ms):</source>
        <translation>Zeitraster (ms):</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="220"/>
        <source>Use mmap access</source>
        <translation>Mmap-Unterstützung aktivieren</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="240"/>
        <source>Use snd_pcm_pause function</source>
        <translation>„snd_pcm_pause“-Funktion verwenden</translation>
    </message>
</context>
<context>
    <name>OutputALSAFactory</name>
    <message>
        <location filename="../outputalsafactory.cpp" line="31"/>
        <source>ALSA Plugin</source>
        <translation>ALSA-Modul</translation>
    </message>
    <message>
        <location filename="../outputalsafactory.cpp" line="55"/>
        <source>About ALSA Output Plugin</source>
        <translation>Über ALSA-Ausgabemodul</translation>
    </message>
    <message>
        <location filename="../outputalsafactory.cpp" line="56"/>
        <source>Qmmp ALSA Output Plugin</source>
        <translation>Qmmp ALSA-Ausgabemodul</translation>
    </message>
    <message>
        <location filename="../outputalsafactory.cpp" line="57"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Geschrieben von: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
