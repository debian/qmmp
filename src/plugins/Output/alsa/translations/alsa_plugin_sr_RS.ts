<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sr_RS">
<context>
    <name>AlsaSettingsDialog</name>
    <message>
        <location filename="../alsasettingsdialog.ui" line="14"/>
        <source>ALSA Plugin Settings</source>
        <translation type="unfinished">Поставке Алса прикључка</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="33"/>
        <source>Device Settings</source>
        <translation type="unfinished">Подешавање уређаја</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="54"/>
        <source>Audio device</source>
        <translation type="unfinished">Звучни уређај</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="70"/>
        <source>Mixer</source>
        <translation type="unfinished">Миксета</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="94"/>
        <source>Mixer card:</source>
        <translation type="unfinished">Карта миксете:</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="104"/>
        <source>Mixer device:</source>
        <translation type="unfinished">Уређај миксете:</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="121"/>
        <source>Advanced Settings</source>
        <translation type="unfinished">Напредно подешавање</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="142"/>
        <source>Soundcard</source>
        <translation type="unfinished">Звучна карта</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="148"/>
        <source>Buffer time (ms):</source>
        <translation type="unfinished">Бафер (ms):</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="184"/>
        <source>Period time (ms):</source>
        <translation type="unfinished">Период (ms):</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="220"/>
        <source>Use mmap access</source>
        <translation type="unfinished">Користи mmap приступ</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="240"/>
        <source>Use snd_pcm_pause function</source>
        <translation type="unfinished">Користи snd_pcm_pause функцију</translation>
    </message>
</context>
<context>
    <name>OutputALSAFactory</name>
    <message>
        <location filename="../outputalsafactory.cpp" line="31"/>
        <source>ALSA Plugin</source>
        <translation>Алса прикључак</translation>
    </message>
    <message>
        <location filename="../outputalsafactory.cpp" line="55"/>
        <source>About ALSA Output Plugin</source>
        <translation>О Алса прикључку излаза</translation>
    </message>
    <message>
        <location filename="../outputalsafactory.cpp" line="56"/>
        <source>Qmmp ALSA Output Plugin</source>
        <translation>Кумп Алса прикључак излаза</translation>
    </message>
    <message>
        <location filename="../outputalsafactory.cpp" line="57"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Аутор: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
