<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>AlsaSettingsDialog</name>
    <message>
        <location filename="../alsasettingsdialog.ui" line="14"/>
        <source>ALSA Plugin Settings</source>
        <translation>Configuration du greffon ALSA</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="33"/>
        <source>Device Settings</source>
        <translation>Configuration du périphérique</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="54"/>
        <source>Audio device</source>
        <translation>Périphérique audio</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="70"/>
        <source>Mixer</source>
        <translation>Mixeur</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="94"/>
        <source>Mixer card:</source>
        <translation>Carte de mixage : </translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="104"/>
        <source>Mixer device:</source>
        <translation>Périphérique de mixage : </translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="121"/>
        <source>Advanced Settings</source>
        <translation>Configuration avancée</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="142"/>
        <source>Soundcard</source>
        <translation>Carte son</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="148"/>
        <source>Buffer time (ms):</source>
        <translation>Taille du tampon (ms) : </translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="184"/>
        <source>Period time (ms):</source>
        <translation>Durée de la période (ms) : </translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="220"/>
        <source>Use mmap access</source>
        <translation>Utiliser l&apos;accès mmap</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="240"/>
        <source>Use snd_pcm_pause function</source>
        <translation>Utiliser la fonction snd_pcm_pause</translation>
    </message>
</context>
<context>
    <name>OutputALSAFactory</name>
    <message>
        <location filename="../outputalsafactory.cpp" line="31"/>
        <source>ALSA Plugin</source>
        <translation>Greffon ALSA</translation>
    </message>
    <message>
        <location filename="../outputalsafactory.cpp" line="55"/>
        <source>About ALSA Output Plugin</source>
        <translation>À propos du greffon de sortie ALSA</translation>
    </message>
    <message>
        <location filename="../outputalsafactory.cpp" line="56"/>
        <source>Qmmp ALSA Output Plugin</source>
        <translation>Greffon de sortie ALSA pour Qmmp</translation>
    </message>
    <message>
        <location filename="../outputalsafactory.cpp" line="57"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Écrit par : Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
