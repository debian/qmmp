<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW">
<context>
    <name>AlsaSettingsDialog</name>
    <message>
        <location filename="../alsasettingsdialog.ui" line="14"/>
        <source>ALSA Plugin Settings</source>
        <translation>ALSA 插件設定</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="33"/>
        <source>Device Settings</source>
        <translation>裝置設定</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="54"/>
        <source>Audio device</source>
        <translation>聲訊裝置</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="70"/>
        <source>Mixer</source>
        <translation>混頻</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="94"/>
        <source>Mixer card:</source>
        <translation>混頻卡：</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="104"/>
        <source>Mixer device:</source>
        <translation>混頻裝置：</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="121"/>
        <source>Advanced Settings</source>
        <translation>進階設定</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="142"/>
        <source>Soundcard</source>
        <translation>音效卡</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="148"/>
        <source>Buffer time (ms):</source>
        <translation>緩衝時間(ms)：</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="184"/>
        <source>Period time (ms):</source>
        <translation>周期時間(ms)：</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="220"/>
        <source>Use mmap access</source>
        <translation>使用內存對映</translation>
    </message>
    <message>
        <location filename="../alsasettingsdialog.ui" line="240"/>
        <source>Use snd_pcm_pause function</source>
        <translation>使用 snd_pcm_pause 功能</translation>
    </message>
</context>
<context>
    <name>OutputALSAFactory</name>
    <message>
        <location filename="../outputalsafactory.cpp" line="31"/>
        <source>ALSA Plugin</source>
        <translation>ALSA 外掛</translation>
    </message>
    <message>
        <location filename="../outputalsafactory.cpp" line="55"/>
        <source>About ALSA Output Plugin</source>
        <translation>關於 ALSA 匯出插件</translation>
    </message>
    <message>
        <location filename="../outputalsafactory.cpp" line="56"/>
        <source>Qmmp ALSA Output Plugin</source>
        <translation>Qmmp ALSA 匯出插件</translation>
    </message>
    <message>
        <location filename="../outputalsafactory.cpp" line="57"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>撰寫：Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
