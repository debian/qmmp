<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>OutputJACKFactory</name>
    <message>
        <location filename="../outputjackfactory.cpp" line="30"/>
        <source>JACK Plugin</source>
        <translation>Втулок JACK</translation>
    </message>
    <message>
        <location filename="../outputjackfactory.cpp" line="54"/>
        <source>About Jack Output Plugin</source>
        <translation>Про втулок виводу Jack</translation>
    </message>
    <message>
        <location filename="../outputjackfactory.cpp" line="55"/>
        <source>Qmmp Jack Output Plugin</source>
        <translation>Втулок виводу Jack для Qmmp</translation>
    </message>
    <message>
        <location filename="../outputjackfactory.cpp" line="56"/>
        <source>Written by: Yuriy Zhuravlev &lt;slalkerg@gmail.com&gt;</source>
        <translation>Розробник: Юрій Журавльов &lt;slalkerg@gmail.com&gt;</translation>
    </message>
</context>
</TS>
