/***************************************************************************
 *   Copyright (C) 2010-2025 by Ilya Kotov                                 *
 *   forkotov02@ya.ru                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/

#include <QSettings>
#include <qmmp/qmmp.h>
#include "ui_crossfadesettingsdialog.h"
#include "crossfadesettingsdialog.h"

CrossfadeSettingsDialog::CrossfadeSettingsDialog(QWidget *parent)
 : QDialog(parent), m_ui(new Ui::CrossfadeSettingsDialog)
{
    m_ui->setupUi(this);
    QSettings settings;
    m_ui->overlapSpinBox->setValue(settings.value(u"Crossfade/overlap"_s, 6000).toInt());
}


CrossfadeSettingsDialog::~CrossfadeSettingsDialog()
{
    delete m_ui;
}

void CrossfadeSettingsDialog::accept()
{
    QSettings settings;
    settings.setValue(u"Crossfade/overlap"_s, m_ui->overlapSpinBox->value());
    QDialog::accept();
}
