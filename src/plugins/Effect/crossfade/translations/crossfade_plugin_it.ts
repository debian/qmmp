<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>CrossfadeSettingsDialog</name>
    <message>
        <location filename="../crossfadesettingsdialog.ui" line="14"/>
        <source>Crossfade Plugin Settings</source>
        <translation>Impostazioni estensione dissolvenza incrociata</translation>
    </message>
    <message>
        <location filename="../crossfadesettingsdialog.ui" line="29"/>
        <source>Overlap:</source>
        <translation>Sovrapposizione:</translation>
    </message>
    <message>
        <location filename="../crossfadesettingsdialog.ui" line="42"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
</context>
<context>
    <name>EffectCrossfadeFactory</name>
    <message>
        <location filename="../effectcrossfadefactory.cpp" line="30"/>
        <source>Crossfade Plugin</source>
        <translation>Estensione dissolvenza incrociata</translation>
    </message>
    <message>
        <location filename="../effectcrossfadefactory.cpp" line="50"/>
        <source>About Crossfade Plugin</source>
        <translation>Informazioni sull&apos;estensione dissolvenza incrociata</translation>
    </message>
    <message>
        <location filename="../effectcrossfadefactory.cpp" line="51"/>
        <source>Qmmp Crossfade Plugin</source>
        <translation>Estensione dissolvenza incrociata per Qmmp</translation>
    </message>
    <message>
        <location filename="../effectcrossfadefactory.cpp" line="52"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Autori: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
