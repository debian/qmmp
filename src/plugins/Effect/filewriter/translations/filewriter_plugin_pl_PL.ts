<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>EffectFileWriterFactory</name>
    <message>
        <location filename="../effectfilewriterfactory.cpp" line="30"/>
        <source>File Writer Plugin</source>
        <translation>Wtyczka Zapis pliku</translation>
    </message>
    <message>
        <location filename="../effectfilewriterfactory.cpp" line="50"/>
        <source>About File Writer Plugin</source>
        <translation>O wtyczce Zapis pliku</translation>
    </message>
    <message>
        <location filename="../effectfilewriterfactory.cpp" line="51"/>
        <source>Qmmp File Writer Plugin</source>
        <translation>Wtyczka Zapis pliku dla Qmmp</translation>
    </message>
    <message>
        <location filename="../effectfilewriterfactory.cpp" line="52"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Napisana przez: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>FileWriterSettingsDialog</name>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="14"/>
        <source>File Writer Plugin Settings</source>
        <translation>Ustawienia wtyczki Zapis pliku</translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="29"/>
        <source>Output directory:</source>
        <translation>Katalog wyjściowy:</translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="36"/>
        <source>Quality:</source>
        <translation>Jakość:</translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="95"/>
        <source>Output file name:</source>
        <translation>Nazwa pliku wyjściowego:</translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="109"/>
        <source>Write to single file if possible.</source>
        <translation>Zapisz w jednym pliku, jeśli to możliwe.</translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.cpp" line="72"/>
        <source>Choose a directory</source>
        <translation>Wybierz katalog</translation>
    </message>
</context>
</TS>
