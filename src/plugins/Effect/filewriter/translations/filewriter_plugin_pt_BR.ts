<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>EffectFileWriterFactory</name>
    <message>
        <location filename="../effectfilewriterfactory.cpp" line="30"/>
        <source>File Writer Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effectfilewriterfactory.cpp" line="50"/>
        <source>About File Writer Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effectfilewriterfactory.cpp" line="51"/>
        <source>Qmmp File Writer Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effectfilewriterfactory.cpp" line="52"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileWriterSettingsDialog</name>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="14"/>
        <source>File Writer Plugin Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="29"/>
        <source>Output directory:</source>
        <translation>Pasta de destino:</translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="36"/>
        <source>Quality:</source>
        <translation>Qualidade:</translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="95"/>
        <source>Output file name:</source>
        <translation>Nome do arquivo de destino:</translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.ui" line="109"/>
        <source>Write to single file if possible.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filewritersettingsdialog.cpp" line="72"/>
        <source>Choose a directory</source>
        <translation>Escolha a pasta</translation>
    </message>
</context>
</TS>
