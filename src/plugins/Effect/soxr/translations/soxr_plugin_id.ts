<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id">
<context>
    <name>EffectSoXRFactory</name>
    <message>
        <location filename="../effectsoxrfactory.cpp" line="30"/>
        <source>SoX Resampler Plugin</source>
        <translation>Plugin Resampler SoX</translation>
    </message>
    <message>
        <location filename="../effectsoxrfactory.cpp" line="50"/>
        <source>About SoX Resampler Plugin</source>
        <translation>Tentang Plugin Resampler SoX</translation>
    </message>
    <message>
        <location filename="../effectsoxrfactory.cpp" line="51"/>
        <source>Qmmp SoX Resampler Plugin</source>
        <translation>Plugin Resampler SoX Qmmp</translation>
    </message>
    <message>
        <location filename="../effectsoxrfactory.cpp" line="52"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Ditulis oleh: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>SoXRSettingsDialog</name>
    <message>
        <location filename="../soxrsettingsdialog.ui" line="14"/>
        <source>Sample Rate Converter Plugin Settings</source>
        <translation type="unfinished">Setelan Plugin Pengkonversi Laju Sample</translation>
    </message>
    <message>
        <location filename="../soxrsettingsdialog.ui" line="20"/>
        <source>Sample Rate (Hz):</source>
        <translation type="unfinished">Sample Rate (Hz):</translation>
    </message>
    <message>
        <location filename="../soxrsettingsdialog.ui" line="40"/>
        <source>Quality:</source>
        <translation type="unfinished">Kualitas:</translation>
    </message>
    <message>
        <location filename="../soxrsettingsdialog.cpp" line="34"/>
        <source>Quick</source>
        <translation type="unfinished">Cepat</translation>
    </message>
    <message>
        <location filename="../soxrsettingsdialog.cpp" line="35"/>
        <source>Low</source>
        <translation type="unfinished">Rendah</translation>
    </message>
    <message>
        <location filename="../soxrsettingsdialog.cpp" line="36"/>
        <source>Medium</source>
        <translation type="unfinished">Sedang</translation>
    </message>
    <message>
        <location filename="../soxrsettingsdialog.cpp" line="37"/>
        <source>High</source>
        <translation type="unfinished">Tinggi</translation>
    </message>
    <message>
        <location filename="../soxrsettingsdialog.cpp" line="38"/>
        <source>Very High</source>
        <translation type="unfinished">Sangat Tinggi</translation>
    </message>
</context>
</TS>
