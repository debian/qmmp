<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>EffectStereoFactory</name>
    <message>
        <location filename="../effectstereofactory.cpp" line="30"/>
        <source>Extra Stereo Plugin</source>
        <translation>Plugin Extra Stereo</translation>
    </message>
    <message>
        <location filename="../effectstereofactory.cpp" line="49"/>
        <source>About Extra Stereo Plugin</source>
        <translation>Sobre o plugin Extra Stereo</translation>
    </message>
    <message>
        <location filename="../effectstereofactory.cpp" line="50"/>
        <source>Qmmp Extra Stereo Plugin</source>
        <translation>Plugin Qmmp Extra Stereo</translation>
    </message>
    <message>
        <location filename="../effectstereofactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effectstereofactory.cpp" line="52"/>
        <source>Based on the Extra Stereo Plugin for Xmms by Johan Levin</source>
        <translation>Baseado no Extra Stereo Plugin for Xmms de Johan Levin</translation>
    </message>
</context>
<context>
    <name>StereoSettingsDialog</name>
    <message>
        <location filename="../stereosettingsdialog.ui" line="14"/>
        <source>Extra Stereo Plugin Settings</source>
        <translation>Preferências do plugin Extra Stereo</translation>
    </message>
    <message>
        <location filename="../stereosettingsdialog.ui" line="31"/>
        <source>Effect intensity:</source>
        <translation>Intensidade do efeito:</translation>
    </message>
    <message>
        <location filename="../stereosettingsdialog.ui" line="54"/>
        <source>-</source>
        <translation>-</translation>
    </message>
</context>
</TS>
