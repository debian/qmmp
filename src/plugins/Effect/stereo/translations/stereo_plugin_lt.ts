<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="lt" sourcelanguage="lt">
<context>
    <name>EffectStereoFactory</name>
    <message>
        <location filename="../effectstereofactory.cpp" line="30"/>
        <source>Extra Stereo Plugin</source>
        <translation>Ekstra Stereo įskiepis</translation>
    </message>
    <message>
        <location filename="../effectstereofactory.cpp" line="49"/>
        <source>About Extra Stereo Plugin</source>
        <translation>Apie Ekstra Stereo įskiepį</translation>
    </message>
    <message>
        <location filename="../effectstereofactory.cpp" line="50"/>
        <source>Qmmp Extra Stereo Plugin</source>
        <translation>Qmmp Ekstra tereo įskiepis</translation>
    </message>
    <message>
        <location filename="../effectstereofactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Sukūrė: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../effectstereofactory.cpp" line="52"/>
        <source>Based on the Extra Stereo Plugin for Xmms by Johan Levin</source>
        <translation>Sukurta Xmms Ekstra Stereo įskiepio pagrindu, kurio autorius Johan Levin</translation>
    </message>
</context>
<context>
    <name>StereoSettingsDialog</name>
    <message>
        <location filename="../stereosettingsdialog.ui" line="14"/>
        <source>Extra Stereo Plugin Settings</source>
        <translation type="unfinished">Extra stereo įskiepio nustatymai</translation>
    </message>
    <message>
        <location filename="../stereosettingsdialog.ui" line="31"/>
        <source>Effect intensity:</source>
        <translation type="unfinished">Efekto stiprumas:</translation>
    </message>
    <message>
        <location filename="../stereosettingsdialog.ui" line="54"/>
        <source>-</source>
        <translation type="unfinished">-</translation>
    </message>
</context>
</TS>
