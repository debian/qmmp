<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="gl_ES">
<context>
    <name>MplayerEngineFactory</name>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="33"/>
        <source>Mplayer Plugin</source>
        <translation>Engadido Mplayer</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="36"/>
        <source>Video Files</source>
        <translation>Ficheiros de vídeo</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="73"/>
        <source>About MPlayer Plugin</source>
        <translation>Sobre o engadido Mplayer</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="74"/>
        <source>Qmmp MPlayer Plugin</source>
        <translation>Engadido Qmmp Mplayer</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="75"/>
        <source>This plugin uses MPlayer as backend</source>
        <translation>Este engadido usa Mplayer como motor</translation>
    </message>
    <message>
        <location filename="../mplayerenginefactory.cpp" line="76"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Escrito por: LLya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>MplayerMetaDataModel</name>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="57"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="57"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="58"/>
        <source>Demuxer</source>
        <translation>Demultiplexor</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="60"/>
        <source>Video format</source>
        <translation>Formato de vídeo</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="61"/>
        <source>FPS</source>
        <translation>FPS</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="62"/>
        <source>Video codec</source>
        <translation>Códec de vídeo</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="63"/>
        <source>Aspect ratio</source>
        <translation>Relación de aspecto</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="64"/>
        <source>Video bitrate</source>
        <translation>Taxa de bits de vídeo</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="64"/>
        <location filename="../mplayermetadatamodel.cpp" line="68"/>
        <source>kbps</source>
        <translation>kbps</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="66"/>
        <source>Audio codec</source>
        <translation>Códec de audio</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="67"/>
        <source>Sample rate</source>
        <translation>Frecuencia de mostra</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="67"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="68"/>
        <source>Audio bitrate</source>
        <translation>Taxa de bits de audio</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="69"/>
        <source>Channels</source>
        <translation>Canais</translation>
    </message>
    <message>
        <location filename="../mplayermetadatamodel.cpp" line="65"/>
        <source>Resolution</source>
        <translation>Resolución</translation>
    </message>
</context>
<context>
    <name>MplayerSettingsDialog</name>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="14"/>
        <source>MPlayer Settings</source>
        <translation>Opcións Mplayer</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="34"/>
        <source>Video:</source>
        <translation>Vídeo:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="48"/>
        <source>Audio:</source>
        <translation>Audio:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="62"/>
        <source>Audio/video auto synchronization</source>
        <translation>Autosincronización audio/vídeo</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="72"/>
        <source>Synchronization factor:</source>
        <translation>Factor de sincronización:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="89"/>
        <source>Extra options:</source>
        <translation>Opcións extra:</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.ui" line="96"/>
        <source>Extra command line options</source>
        <translation>Opcións extra da liña de comandos</translation>
    </message>
    <message>
        <location filename="../mplayersettingsdialog.cpp" line="29"/>
        <location filename="../mplayersettingsdialog.cpp" line="33"/>
        <location filename="../mplayersettingsdialog.cpp" line="37"/>
        <location filename="../mplayersettingsdialog.cpp" line="38"/>
        <location filename="../mplayersettingsdialog.cpp" line="54"/>
        <location filename="../mplayersettingsdialog.cpp" line="55"/>
        <source>default</source>
        <translation>por defecto</translation>
    </message>
</context>
</TS>
