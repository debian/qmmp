<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>DecoderArchiveFactory</name>
    <message>
        <location filename="../decoderarchivefactory.cpp" line="39"/>
        <source>Archive Plugin</source>
        <translation>Втулок архівів</translation>
    </message>
    <message>
        <location filename="../decoderarchivefactory.cpp" line="41"/>
        <source>Archives</source>
        <translation>Архіви</translation>
    </message>
    <message>
        <location filename="../decoderarchivefactory.cpp" line="167"/>
        <source>About Archive Reader Plugin</source>
        <translation>Про втулок читання архівів</translation>
    </message>
    <message>
        <location filename="../decoderarchivefactory.cpp" line="168"/>
        <source>Qmmp Archive Reader Plugin</source>
        <translation>Втулок читання архівів для Qmmp</translation>
    </message>
    <message>
        <location filename="../decoderarchivefactory.cpp" line="169"/>
        <source>Compiled against %1</source>
        <translation>Зібрано з %1</translation>
    </message>
    <message>
        <location filename="../decoderarchivefactory.cpp" line="170"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
