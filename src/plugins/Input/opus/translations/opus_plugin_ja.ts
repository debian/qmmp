<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>DecoderOpusFactory</name>
    <message>
        <location filename="../decoderopusfactory.cpp" line="42"/>
        <source>Opus Plugin</source>
        <translation>Opus プラグイン</translation>
    </message>
    <message>
        <location filename="../decoderopusfactory.cpp" line="45"/>
        <source>Ogg Opus Files</source>
        <translation>Ogg Opus ファイル</translation>
    </message>
    <message>
        <location filename="../decoderopusfactory.cpp" line="125"/>
        <source>About Opus Audio Plugin</source>
        <translation>Opus 音響プラグインについて</translation>
    </message>
    <message>
        <location filename="../decoderopusfactory.cpp" line="126"/>
        <source>Qmmp Opus Audio Plugin</source>
        <translation>QMMP Opus 音響プラグイン</translation>
    </message>
    <message>
        <location filename="../decoderopusfactory.cpp" line="127"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>制作: Илья Котов (Ilya Kotov) &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>OpusMetaDataModel</name>
    <message>
        <location filename="../opusmetadatamodel.cpp" line="55"/>
        <source>Version</source>
        <translation>版</translation>
    </message>
</context>
</TS>
