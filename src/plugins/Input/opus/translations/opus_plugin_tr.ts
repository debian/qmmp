<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>DecoderOpusFactory</name>
    <message>
        <location filename="../decoderopusfactory.cpp" line="42"/>
        <source>Opus Plugin</source>
        <translation>Opus Eklentisi</translation>
    </message>
    <message>
        <location filename="../decoderopusfactory.cpp" line="45"/>
        <source>Ogg Opus Files</source>
        <translation>Ogg Opus Dosyaları</translation>
    </message>
    <message>
        <location filename="../decoderopusfactory.cpp" line="125"/>
        <source>About Opus Audio Plugin</source>
        <translation>Opus Ses Eklentisi Hakkında</translation>
    </message>
    <message>
        <location filename="../decoderopusfactory.cpp" line="126"/>
        <source>Qmmp Opus Audio Plugin</source>
        <translation>Qmmp Opus Ses Eklentisi</translation>
    </message>
    <message>
        <location filename="../decoderopusfactory.cpp" line="127"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Yazan: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>OpusMetaDataModel</name>
    <message>
        <location filename="../opusmetadatamodel.cpp" line="55"/>
        <source>Version</source>
        <translation>Uyarlama</translation>
    </message>
</context>
</TS>
