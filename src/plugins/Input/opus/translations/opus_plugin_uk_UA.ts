<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>DecoderOpusFactory</name>
    <message>
        <location filename="../decoderopusfactory.cpp" line="42"/>
        <source>Opus Plugin</source>
        <translation>Втулок Opus</translation>
    </message>
    <message>
        <location filename="../decoderopusfactory.cpp" line="45"/>
        <source>Ogg Opus Files</source>
        <translation>Файли Ogg Opus</translation>
    </message>
    <message>
        <location filename="../decoderopusfactory.cpp" line="125"/>
        <source>About Opus Audio Plugin</source>
        <translation>Про втулок авдіо Opus</translation>
    </message>
    <message>
        <location filename="../decoderopusfactory.cpp" line="126"/>
        <source>Qmmp Opus Audio Plugin</source>
        <translation>Втулок Opus для Qmmp</translation>
    </message>
    <message>
        <location filename="../decoderopusfactory.cpp" line="127"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>OpusMetaDataModel</name>
    <message>
        <location filename="../opusmetadatamodel.cpp" line="55"/>
        <source>Version</source>
        <translation>Версія</translation>
    </message>
</context>
</TS>
