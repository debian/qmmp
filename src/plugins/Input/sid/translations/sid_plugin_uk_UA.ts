<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>DecoderSIDFactory</name>
    <message>
        <location filename="../decodersidfactory.cpp" line="57"/>
        <source>SID Plugin</source>
        <translation>Втулок SID</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="59"/>
        <source>SID Files</source>
        <translation>Файли SID</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="112"/>
        <source>About SID Audio Plugin</source>
        <translation>Про втулок звуку SID</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="113"/>
        <source>Qmmp SID Audio Plugin</source>
        <translation>Втулок звуку SID для Qmmp</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="114"/>
        <source>This plugin plays Commodore 64 music files using libsidplayfp library</source>
        <translation>Цей втулок відтворює музичні файли Commodore 64 з використанням бібліотеки libsidplayfp</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="115"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>SidSettingsDialog</name>
    <message>
        <location filename="../sidsettingsdialog.ui" line="14"/>
        <source>SID Plugin Settings</source>
        <translation>Налаштування втулка SID</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="55"/>
        <source>Fast resampling</source>
        <translation>Швидка зміна частоти</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="62"/>
        <source>Sample rate:</source>
        <translation>Частота дискретизації:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="69"/>
        <source>HVSC database file:</source>
        <translation>Файл бази даних HVSC:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="76"/>
        <source>Resampling method:</source>
        <translation>Метод зміни частоти:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="93"/>
        <source>Defaults song length, sec:</source>
        <translation>Усталена тривалість пісні, сек:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="100"/>
        <source>Enable HVSC song length database</source>
        <translation>Активувати базу даних тривалости пісні HVSC</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="107"/>
        <source>Emulation:</source>
        <translation>Емуляція:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.cpp" line="41"/>
        <source>44100 Hz</source>
        <translation>44100 Гц</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.cpp" line="42"/>
        <source>48000 Hz</source>
        <translation>48000 Гц</translation>
    </message>
</context>
</TS>
