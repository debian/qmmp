<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>DecoderSIDFactory</name>
    <message>
        <location filename="../decodersidfactory.cpp" line="57"/>
        <source>SID Plugin</source>
        <translation>SID Eklentisi</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="59"/>
        <source>SID Files</source>
        <translation>SID Dosyaları</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="112"/>
        <source>About SID Audio Plugin</source>
        <translation>SID Ses Eklentisi Hakkında</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="113"/>
        <source>Qmmp SID Audio Plugin</source>
        <translation>Qmmp SID Ses Eklentisi</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="114"/>
        <source>This plugin plays Commodore 64 music files using libsidplayfp library</source>
        <translation>Bu eklenti libsidplayfp kitaplığını kullanarak Commodore 64 müzik dosyalarını oynatır</translation>
    </message>
    <message>
        <location filename="../decodersidfactory.cpp" line="115"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Yazan: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>SidSettingsDialog</name>
    <message>
        <location filename="../sidsettingsdialog.ui" line="14"/>
        <source>SID Plugin Settings</source>
        <translation>SID Eklenti Ayarları</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="55"/>
        <source>Fast resampling</source>
        <translation>Hızlı yeniden örnekleme</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="62"/>
        <source>Sample rate:</source>
        <translation>Örnekleme oranı:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="69"/>
        <source>HVSC database file:</source>
        <translation>HVSC veritabanı dosyası:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="76"/>
        <source>Resampling method:</source>
        <translation>Yeniden örnekleme yöntemi:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="93"/>
        <source>Defaults song length, sec:</source>
        <translation>Varsayılan şarkı uzunluğu, sn:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="100"/>
        <source>Enable HVSC song length database</source>
        <translation>HVSC şarkı uzunluğu veritabanını etkinleştir</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.ui" line="107"/>
        <source>Emulation:</source>
        <translation>Öykünüm:</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.cpp" line="41"/>
        <source>44100 Hz</source>
        <translation>44100 Hz</translation>
    </message>
    <message>
        <location filename="../sidsettingsdialog.cpp" line="42"/>
        <source>48000 Hz</source>
        <translation>48000 Hz</translation>
    </message>
</context>
</TS>
