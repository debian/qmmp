<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="he">
<context>
    <name>DecoderWavPackFactory</name>
    <message>
        <location filename="../decoderwavpackfactory.cpp" line="38"/>
        <source>WavPack Plugin</source>
        <translation>תוסף WavPack</translation>
    </message>
    <message>
        <location filename="../decoderwavpackfactory.cpp" line="40"/>
        <source>WavPack Files</source>
        <translation>קבצי WavPack</translation>
    </message>
    <message>
        <location filename="../decoderwavpackfactory.cpp" line="180"/>
        <source>About WavPack Audio Plugin</source>
        <translation>אודות תוסף שמע WavPack</translation>
    </message>
    <message>
        <location filename="../decoderwavpackfactory.cpp" line="181"/>
        <source>Qmmp WavPack Audio Plugin</source>
        <translation>תוסף שמע WavPack של Qmmp</translation>
    </message>
    <message>
        <location filename="../decoderwavpackfactory.cpp" line="182"/>
        <source>WavPack library version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decoderwavpackfactory.cpp" line="184"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WavPackMetaDataModel</name>
    <message>
        <location filename="../wavpackmetadatamodel.cpp" line="71"/>
        <source>Ratio</source>
        <translation>יחס</translation>
    </message>
    <message>
        <location filename="../wavpackmetadatamodel.cpp" line="72"/>
        <source>Version</source>
        <translation>גירסה</translation>
    </message>
</context>
</TS>
