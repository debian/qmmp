<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>DecoderMPCFactory</name>
    <message>
        <location filename="../decodermpcfactory.cpp" line="47"/>
        <source>Musepack Plugin</source>
        <translation>Втулок Musepack</translation>
    </message>
    <message>
        <location filename="../decodermpcfactory.cpp" line="49"/>
        <source>Musepack Files</source>
        <translation>Файли Musepack</translation>
    </message>
    <message>
        <location filename="../decodermpcfactory.cpp" line="114"/>
        <source>About Musepack Audio Plugin</source>
        <translation>Про авдіовтулок Musepack</translation>
    </message>
    <message>
        <location filename="../decodermpcfactory.cpp" line="115"/>
        <source>Qmmp Musepack Audio Plugin</source>
        <translation>Авдіовтулок Musepack для Qmmp</translation>
    </message>
    <message>
        <location filename="../decodermpcfactory.cpp" line="116"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
