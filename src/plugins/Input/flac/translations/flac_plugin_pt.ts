<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt">
<context>
    <name>DecoderFLACFactory</name>
    <message>
        <location filename="../decoderflacfactory.cpp" line="53"/>
        <source>FLAC Plugin</source>
        <translation>Suplemento FLAC</translation>
    </message>
    <message>
        <location filename="../decoderflacfactory.cpp" line="55"/>
        <source>FLAC Files</source>
        <translation>Ficheiros FLAC</translation>
    </message>
    <message>
        <location filename="../decoderflacfactory.cpp" line="217"/>
        <source>About FLAC Audio Plugin</source>
        <translation>Acerca de suplemento FLAC Audio</translation>
    </message>
    <message>
        <location filename="../decoderflacfactory.cpp" line="218"/>
        <source>Qmmp FLAC Audio Plugin</source>
        <translation>Suplemento Qmmp FLAC Audio</translation>
    </message>
    <message>
        <location filename="../decoderflacfactory.cpp" line="219"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Desenvolvido por: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
