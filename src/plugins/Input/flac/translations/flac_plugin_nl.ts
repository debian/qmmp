<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>DecoderFLACFactory</name>
    <message>
        <location filename="../decoderflacfactory.cpp" line="53"/>
        <source>FLAC Plugin</source>
        <translation>FLAC-plug-in</translation>
    </message>
    <message>
        <location filename="../decoderflacfactory.cpp" line="55"/>
        <source>FLAC Files</source>
        <translation>FLAC-bestanden</translation>
    </message>
    <message>
        <location filename="../decoderflacfactory.cpp" line="217"/>
        <source>About FLAC Audio Plugin</source>
        <translation>Over de FLAC-audioplug-in</translation>
    </message>
    <message>
        <location filename="../decoderflacfactory.cpp" line="218"/>
        <source>Qmmp FLAC Audio Plugin</source>
        <translation>FLAC-plug-in voor Qmmp</translation>
    </message>
    <message>
        <location filename="../decoderflacfactory.cpp" line="219"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Auteur: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
