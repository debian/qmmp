<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>DecoderWildMidiFactory</name>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="49"/>
        <source>WildMidi Plugin</source>
        <translation>Втулок WildMidi</translation>
    </message>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="54"/>
        <source>Midi Files</source>
        <translation>Файли Midi</translation>
    </message>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="105"/>
        <source>About WildMidi Audio Plugin</source>
        <translation>Про авдіовтулок WildMidi</translation>
    </message>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="106"/>
        <source>Qmmp WildMidi Audio Plugin</source>
        <translation>Авдіовтулок WildMidi для Qmmp</translation>
    </message>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="107"/>
        <source>This plugin uses WildMidi library to play midi files</source>
        <translation>Цей втулок використовує бібліотеку WildMidi для програвання файлів midi</translation>
    </message>
    <message>
        <location filename="../decoderwildmidifactory.cpp" line="108"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>WildMidiSettingsDialog</name>
    <message>
        <location filename="../wildmidisettingsdialog.ui" line="14"/>
        <source>WildMidi Plugin Settings</source>
        <translation>Налаштування втулка WildMidi</translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.ui" line="29"/>
        <source>Instrument configuration: </source>
        <translation>Конфігурація знадобу: </translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.ui" line="43"/>
        <source>Sample rate:</source>
        <translation>Частота дискретизації:</translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.ui" line="50"/>
        <source>Enhanced resampling</source>
        <translation>Покращена передискретизація</translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.ui" line="57"/>
        <source>Reverberation</source>
        <translation>Реверберація</translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.cpp" line="39"/>
        <source>44100 Hz</source>
        <translation>44100 Гц</translation>
    </message>
    <message>
        <location filename="../wildmidisettingsdialog.cpp" line="40"/>
        <source>48000 Hz</source>
        <translation>48000 Гц</translation>
    </message>
</context>
</TS>
