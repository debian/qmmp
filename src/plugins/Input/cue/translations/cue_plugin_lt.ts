<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="lt">
<context>
    <name>CueSettingsDialog</name>
    <message>
        <location filename="../cuesettingsdialog.ui" line="14"/>
        <source>CUE Plugin Settings</source>
        <translation type="unfinished">CUE įskiepio nustatymai</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="29"/>
        <source>Common settings</source>
        <translation type="unfinished">Bendri nustatymai</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="35"/>
        <source>Load incorrect cue sheets if possible</source>
        <translation type="unfinished">Jei įmanoma įkelti neteisingą CUE informaciją  </translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="45"/>
        <source>CUE encoding</source>
        <translation type="unfinished">CUE kodavimas</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="51"/>
        <source>Automatic charset detection</source>
        <translation type="unfinished">Automatinis koduotės aptikimas</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="61"/>
        <source>Language:</source>
        <translation type="unfinished">Kalba:</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="81"/>
        <source>Default encoding:</source>
        <translation type="unfinished">Kodavinas pagal nutylėjimą:</translation>
    </message>
</context>
<context>
    <name>DecoderCUEFactory</name>
    <message>
        <location filename="../decodercuefactory.cpp" line="38"/>
        <source>CUE Plugin</source>
        <translation>CUE įskiepis</translation>
    </message>
    <message>
        <location filename="../decodercuefactory.cpp" line="41"/>
        <source>CUE Files</source>
        <translation>CUE bylos</translation>
    </message>
    <message>
        <location filename="../decodercuefactory.cpp" line="81"/>
        <source>About CUE Audio Plugin</source>
        <translation>Apie CUE audio įskiepį</translation>
    </message>
    <message>
        <location filename="../decodercuefactory.cpp" line="82"/>
        <source>Qmmp CUE Audio Plugin</source>
        <translation>Qmmp CUE audio įskiepis</translation>
    </message>
    <message>
        <location filename="../decodercuefactory.cpp" line="83"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Sukūrė:Ilya Kotov </translation>
    </message>
</context>
</TS>
