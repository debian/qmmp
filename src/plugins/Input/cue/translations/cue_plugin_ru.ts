<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>CueSettingsDialog</name>
    <message>
        <location filename="../cuesettingsdialog.ui" line="14"/>
        <source>CUE Plugin Settings</source>
        <translation>Настройки модуля CUE</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="29"/>
        <source>Common settings</source>
        <translation>Основные настройки</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="35"/>
        <source>Load incorrect cue sheets if possible</source>
        <translation>Загружать cue с ошибками, если возможно</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="45"/>
        <source>CUE encoding</source>
        <translation>Кодировка CUE</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="51"/>
        <source>Automatic charset detection</source>
        <translation>Автоматическое определение кодировки</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="61"/>
        <source>Language:</source>
        <translation>Язык:</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="81"/>
        <source>Default encoding:</source>
        <translation>Кодировка по умолчанию:</translation>
    </message>
</context>
<context>
    <name>DecoderCUEFactory</name>
    <message>
        <location filename="../decodercuefactory.cpp" line="38"/>
        <source>CUE Plugin</source>
        <translation>Модуль CUE</translation>
    </message>
    <message>
        <location filename="../decodercuefactory.cpp" line="41"/>
        <source>CUE Files</source>
        <translation>Файлы CUE</translation>
    </message>
    <message>
        <location filename="../decodercuefactory.cpp" line="81"/>
        <source>About CUE Audio Plugin</source>
        <translation>Об аудио-модуле CUE</translation>
    </message>
    <message>
        <location filename="../decodercuefactory.cpp" line="82"/>
        <source>Qmmp CUE Audio Plugin</source>
        <translation>Аудио-модуль CUE для Qmmp</translation>
    </message>
    <message>
        <location filename="../decodercuefactory.cpp" line="83"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Разработчик: Илья Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
