<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="gl_ES">
<context>
    <name>CueSettingsDialog</name>
    <message>
        <location filename="../cuesettingsdialog.ui" line="14"/>
        <source>CUE Plugin Settings</source>
        <translation>Preferencias do engadido CUE</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="29"/>
        <source>Common settings</source>
        <translation>Opcións comúns</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="35"/>
        <source>Load incorrect cue sheets if possible</source>
        <translation>Cargar follas cue incorrectas se é posible</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="45"/>
        <source>CUE encoding</source>
        <translation>Codificación CUE</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="51"/>
        <source>Automatic charset detection</source>
        <translation>Detección do conxunto de caracteres automática</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="61"/>
        <source>Language:</source>
        <translation>Lingua:</translation>
    </message>
    <message>
        <location filename="../cuesettingsdialog.ui" line="81"/>
        <source>Default encoding:</source>
        <translation>Codificación predeterminada:</translation>
    </message>
</context>
<context>
    <name>DecoderCUEFactory</name>
    <message>
        <location filename="../decodercuefactory.cpp" line="38"/>
        <source>CUE Plugin</source>
        <translation>Engadido CUE</translation>
    </message>
    <message>
        <location filename="../decodercuefactory.cpp" line="41"/>
        <source>CUE Files</source>
        <translation>Ficheiros CUE</translation>
    </message>
    <message>
        <location filename="../decodercuefactory.cpp" line="81"/>
        <source>About CUE Audio Plugin</source>
        <translation>Sobre o engadido de audio CUE</translation>
    </message>
    <message>
        <location filename="../decodercuefactory.cpp" line="82"/>
        <source>Qmmp CUE Audio Plugin</source>
        <translation>Engadido de audio CUE de Qmmp</translation>
    </message>
    <message>
        <location filename="../decodercuefactory.cpp" line="83"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Escrito por: LLya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
