<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>DecoderMpegFactory</name>
    <message>
        <location filename="../decodermpegfactory.cpp" line="178"/>
        <source>MPEG Plugin</source>
        <translation>MPEG-plug-in</translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="181"/>
        <source>MPEG Files</source>
        <translation>MPEG-bestanden</translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="417"/>
        <source>About MPEG Audio Plugin</source>
        <translation>Over de MPEG-audioplug-in</translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="418"/>
        <source>MPEG 1.0/2.0/2.5 layer 1/2/3 audio decoder</source>
        <translation>MPEG 1.0/2.0/2.5 layer 1/2/3-audiodecoder</translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="419"/>
        <source>Compiled against:</source>
        <translation>Gebouwd met:</translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="428"/>
        <source>mpg123, API version: %1</source>
        <translation>mpg123, API-versie: %1</translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="432"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Maker: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="433"/>
        <source>Source code based on mq3 and madplay projects</source>
        <translation>De broncode is gebaseerd op de projecten &apos;mq3&apos; en &apos;madplay&apos;</translation>
    </message>
</context>
<context>
    <name>MPEGMetaDataModel</name>
    <message>
        <location filename="../mpegmetadatamodel.cpp" line="70"/>
        <location filename="../mpegmetadatamodel.cpp" line="73"/>
        <location filename="../mpegmetadatamodel.cpp" line="76"/>
        <location filename="../mpegmetadatamodel.cpp" line="79"/>
        <source>Mode</source>
        <translation>Modus</translation>
    </message>
    <message>
        <location filename="../mpegmetadatamodel.cpp" line="82"/>
        <source>Protection</source>
        <translation>Bescherming</translation>
    </message>
    <message>
        <location filename="../mpegmetadatamodel.cpp" line="83"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
    <message>
        <location filename="../mpegmetadatamodel.cpp" line="84"/>
        <source>Original</source>
        <translation>Origineel</translation>
    </message>
</context>
<context>
    <name>MpegSettingsDialog</name>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="14"/>
        <source>MPEG Plugin Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="35"/>
        <source>Decoder</source>
        <translation>Decoder</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="41"/>
        <source>MAD</source>
        <translation>MAD</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="48"/>
        <source>MPG123</source>
        <translation>MPG123</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="71"/>
        <source>Enable CRC checking</source>
        <translation>CRC-controle inschakelen</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="87"/>
        <source>Tag Priority</source>
        <translation>Tagprioriteit</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="99"/>
        <source>First:</source>
        <translation>Primair:</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="119"/>
        <location filename="../mpegsettingsdialog.ui" line="168"/>
        <location filename="../mpegsettingsdialog.ui" line="217"/>
        <source>ID3v1</source>
        <translation>ID3v1</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="124"/>
        <location filename="../mpegsettingsdialog.ui" line="173"/>
        <location filename="../mpegsettingsdialog.ui" line="222"/>
        <source>ID3v2</source>
        <translation>ID3v2</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="129"/>
        <location filename="../mpegsettingsdialog.ui" line="178"/>
        <location filename="../mpegsettingsdialog.ui" line="227"/>
        <source>APE</source>
        <translation>APE</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="134"/>
        <location filename="../mpegsettingsdialog.ui" line="183"/>
        <location filename="../mpegsettingsdialog.ui" line="232"/>
        <source>Disabled</source>
        <translation>Uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="148"/>
        <source>Second:</source>
        <translation>Secundair:</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="197"/>
        <source>Third:</source>
        <translation>Tertiair:</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="253"/>
        <source>Merge selected tag types</source>
        <translation>Geselecteerde tagsoorten samenvoegen</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="272"/>
        <source>Encodings</source>
        <translation>Codering</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="294"/>
        <source>ID3v2 encoding:</source>
        <translation>ID3v2-codering:</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="333"/>
        <source>ID3v1 encoding:</source>
        <translation>ID3v1-codering:</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="343"/>
        <source>Try to detect encoding</source>
        <translation>Probeer codering te herkennen</translation>
    </message>
</context>
</TS>
