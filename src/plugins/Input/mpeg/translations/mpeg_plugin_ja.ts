<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>DecoderMpegFactory</name>
    <message>
        <location filename="../decodermpegfactory.cpp" line="178"/>
        <source>MPEG Plugin</source>
        <translation>MPEG プラグイン</translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="181"/>
        <source>MPEG Files</source>
        <translation>MPEG ファイル</translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="417"/>
        <source>About MPEG Audio Plugin</source>
        <translation>MPEG 音響プラグインについて</translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="418"/>
        <source>MPEG 1.0/2.0/2.5 layer 1/2/3 audio decoder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="419"/>
        <source>Compiled against:</source>
        <translation>コンパイルに使用したライブラリ:</translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="428"/>
        <source>mpg123, API version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="432"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>制作: Илья Котов (Ilya Kotov) &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../decodermpegfactory.cpp" line="433"/>
        <source>Source code based on mq3 and madplay projects</source>
        <translation>ソースコードは mq3 と madplay の両プロジェクトから流用</translation>
    </message>
</context>
<context>
    <name>MPEGMetaDataModel</name>
    <message>
        <location filename="../mpegmetadatamodel.cpp" line="70"/>
        <location filename="../mpegmetadatamodel.cpp" line="73"/>
        <location filename="../mpegmetadatamodel.cpp" line="76"/>
        <location filename="../mpegmetadatamodel.cpp" line="79"/>
        <source>Mode</source>
        <translation>モード</translation>
    </message>
    <message>
        <location filename="../mpegmetadatamodel.cpp" line="82"/>
        <source>Protection</source>
        <translation>保護</translation>
    </message>
    <message>
        <location filename="../mpegmetadatamodel.cpp" line="83"/>
        <source>Copyright</source>
        <translation>著作権</translation>
    </message>
    <message>
        <location filename="../mpegmetadatamodel.cpp" line="84"/>
        <source>Original</source>
        <translation>原作</translation>
    </message>
</context>
<context>
    <name>MpegSettingsDialog</name>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="14"/>
        <source>MPEG Plugin Settings</source>
        <translation>MPEG プラグイン設定</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="35"/>
        <source>Decoder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="41"/>
        <source>MAD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="48"/>
        <source>MPG123</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="71"/>
        <source>Enable CRC checking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="87"/>
        <source>Tag Priority</source>
        <translation>タグ優先度</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="99"/>
        <source>First:</source>
        <translation>第一:</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="119"/>
        <location filename="../mpegsettingsdialog.ui" line="168"/>
        <location filename="../mpegsettingsdialog.ui" line="217"/>
        <source>ID3v1</source>
        <translation>ID3v1</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="124"/>
        <location filename="../mpegsettingsdialog.ui" line="173"/>
        <location filename="../mpegsettingsdialog.ui" line="222"/>
        <source>ID3v2</source>
        <translation>ID3v2</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="129"/>
        <location filename="../mpegsettingsdialog.ui" line="178"/>
        <location filename="../mpegsettingsdialog.ui" line="227"/>
        <source>APE</source>
        <translation>APE</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="134"/>
        <location filename="../mpegsettingsdialog.ui" line="183"/>
        <location filename="../mpegsettingsdialog.ui" line="232"/>
        <source>Disabled</source>
        <translation>無効</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="148"/>
        <source>Second:</source>
        <translation>第二:</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="197"/>
        <source>Third:</source>
        <translation>第三:</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="253"/>
        <source>Merge selected tag types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="272"/>
        <source>Encodings</source>
        <translation>文字符号化の形式</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="294"/>
        <source>ID3v2 encoding:</source>
        <translation>ID3v2 用文字符号化形式:</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="333"/>
        <source>ID3v1 encoding:</source>
        <translation>ID3v1 用文字符号化形式:</translation>
    </message>
    <message>
        <location filename="../mpegsettingsdialog.ui" line="343"/>
        <source>Try to detect encoding</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
