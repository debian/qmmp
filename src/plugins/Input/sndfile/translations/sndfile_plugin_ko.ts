<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko">
<context>
    <name>DecoderSndFileFactory</name>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="119"/>
        <source>Sndfile Plugin</source>
        <translation>Sndfile 플러그인</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="123"/>
        <source>PCM Files</source>
        <translation>PCM 파일</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="228"/>
        <source>About Sndfile Audio Plugin</source>
        <translation>Sndfile 오디오 플러그인 정보</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="229"/>
        <source>Qmmp Sndfile Audio Plugin</source>
        <translation>Qmmp Sndfile 오디오 플러그인</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="230"/>
        <source>Compiled against %1</source>
        <translation>%1에 대해 컴파일됨</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="231"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>작성자: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
