<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>DecoderSndFileFactory</name>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="119"/>
        <source>Sndfile Plugin</source>
        <translation>Sndfile プラグイン</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="123"/>
        <source>PCM Files</source>
        <translation>PCM ファイル</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="228"/>
        <source>About Sndfile Audio Plugin</source>
        <translation>Sndfile 音響プラグインについて</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="229"/>
        <source>Qmmp Sndfile Audio Plugin</source>
        <translation>QMMP Sndfile 音響プラグイン</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="230"/>
        <source>Compiled against %1</source>
        <translation>%1 を使用してコンパイル</translation>
    </message>
    <message>
        <location filename="../decodersndfilefactory.cpp" line="231"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>制作: Илья Котов (Ilya Kotov) &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
