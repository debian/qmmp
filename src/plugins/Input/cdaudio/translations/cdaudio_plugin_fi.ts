<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fi">
<context>
    <name>CDAudioSettingsDialog</name>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="14"/>
        <source>CD Audio Plugin Settings</source>
        <translation>Asetukset CD Audio Plugin</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="139"/>
        <source>Override device:</source>
        <translation>Ohita laite:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="129"/>
        <source>Limit cd speed:</source>
        <translation>Rajoita cd:n nopeutta:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="122"/>
        <source>Use cd-text</source>
        <translation>Käytä cd-tekstiä</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="39"/>
        <source>CDDB</source>
        <translation>CDDB</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="48"/>
        <source>Use HTTP instead of CDDBP</source>
        <translation>Käytä HTTP:tä CDDBP:n sijaan</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="58"/>
        <source>Server:</source>
        <translation>Palvelin:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="65"/>
        <source>Path:</source>
        <translation>Polku:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="75"/>
        <source>Port:</source>
        <translation>Portti:</translation>
    </message>
    <message>
        <location filename="../cdaudiosettingsdialog.ui" line="87"/>
        <source>Clear CDDB cache</source>
        <translation>Tyhjennä CDDB-välimuisti</translation>
    </message>
</context>
<context>
    <name>DecoderCDAudioFactory</name>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="52"/>
        <source>CD Audio Plugin</source>
        <translation>CD Audio Plugin</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="98"/>
        <source>About CD Audio Plugin</source>
        <translation>Tietoja: CD Audio Plugin</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="99"/>
        <source>Qmmp CD Audio Plugin</source>
        <translation>Qmmp CD Audio Plugin</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="101"/>
        <source>Compiled against libcdio-%1 and libcddb-%2</source>
        <translation>Koostettu: libcdio-%1 ja libcddb-%2</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="104"/>
        <source>Compiled against libcdio-%1</source>
        <translation>Koostettu libcdio-%1</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="107"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Kirjoittanut: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../decodercdaudiofactory.cpp" line="108"/>
        <source>Usage: open cdda:/// using Add URL dialog or command line</source>
        <translation>Käyttö: avaa käyttäen cdda:/// lisää URL valintaan tai komentorivillä</translation>
    </message>
</context>
</TS>
