<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW">
<context>
    <name>MMSInputFactory</name>
    <message>
        <location filename="../mmsinputfactory.cpp" line="32"/>
        <source>MMS Plugin</source>
        <translation>MMS 外掛</translation>
    </message>
    <message>
        <location filename="../mmsinputfactory.cpp" line="51"/>
        <source>About MMS Transport Plugin</source>
        <translation>關於 MMS 傳輸插件</translation>
    </message>
    <message>
        <location filename="../mmsinputfactory.cpp" line="52"/>
        <source>Qmmp MMS Transport Plugin</source>
        <translation>Qmmp MMS 傳輸插件</translation>
    </message>
    <message>
        <location filename="../mmsinputfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>撰寫：Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>MmsSettingsDialog</name>
    <message>
        <location filename="../mmssettingsdialog.ui" line="14"/>
        <source>MMS Plugin Settings</source>
        <translation>MMS 插件設定</translation>
    </message>
    <message>
        <location filename="../mmssettingsdialog.ui" line="29"/>
        <source>Buffer size:</source>
        <translation>緩沖大小：</translation>
    </message>
    <message>
        <location filename="../mmssettingsdialog.ui" line="64"/>
        <source>KB</source>
        <translation>KB</translation>
    </message>
</context>
</TS>
