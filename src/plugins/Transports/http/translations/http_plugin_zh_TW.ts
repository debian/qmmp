<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW">
<context>
    <name>HTTPInputFactory</name>
    <message>
        <location filename="../httpinputfactory.cpp" line="33"/>
        <source>HTTP Plugin</source>
        <translation>HTTP 外掛</translation>
    </message>
    <message>
        <location filename="../httpinputfactory.cpp" line="52"/>
        <source>About HTTP Transport Plugin</source>
        <translation>關於 HTTP 傳輸插件</translation>
    </message>
    <message>
        <location filename="../httpinputfactory.cpp" line="53"/>
        <source>Qmmp HTTP Transport Plugin</source>
        <translation>Qmmp HTTP 傳輸插件</translation>
    </message>
    <message>
        <location filename="../httpinputfactory.cpp" line="54"/>
        <source>Compiled against libcurl-%1</source>
        <translation>編譯依賴於 libcurl-%1</translation>
    </message>
    <message>
        <location filename="../httpinputfactory.cpp" line="55"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>撰寫：Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>HttpSettingsDialog</name>
    <message>
        <location filename="../httpsettingsdialog.ui" line="14"/>
        <source>HTTP Plugin Settings</source>
        <translation>HTTP 插件設定</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="115"/>
        <source>Metadata encoding</source>
        <translation>元數據編碼</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="121"/>
        <source>Automatic charset detection</source>
        <translation>自動檢測字符集</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="131"/>
        <source>Language:</source>
        <translation>語言：</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="151"/>
        <source>Default encoding:</source>
        <translation>默認編碼：</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="76"/>
        <source>User Agent:</source>
        <translation>用戶端:</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="34"/>
        <source>Default buffer size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="50"/>
        <source>This value is used if information about bitrate is &lt;b&gt;not&lt;/b&gt; available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="53"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="69"/>
        <source>Change User Agent</source>
        <translation>變更用戶端</translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="90"/>
        <source>Buffer duration:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="97"/>
        <source>This value is used if information about bitrate is available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../httpsettingsdialog.ui" line="100"/>
        <source>ms</source>
        <translation>毫秒</translation>
    </message>
</context>
</TS>
