<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>TrackChangeFactory</name>
    <message>
        <location filename="../trackchangefactory.cpp" line="29"/>
        <source>Track Change Plugin</source>
        <translation>Втулок зміни доріжки</translation>
    </message>
    <message>
        <location filename="../trackchangefactory.cpp" line="49"/>
        <source>About Track Change Plugin</source>
        <translation>Про втулок зміни доріжки</translation>
    </message>
    <message>
        <location filename="../trackchangefactory.cpp" line="50"/>
        <source>Qmmp Track Change Plugin</source>
        <translation>Втулок зміни доріжки для Qmmp</translation>
    </message>
    <message>
        <location filename="../trackchangefactory.cpp" line="51"/>
        <source>This plugin executes external command when current track is changed</source>
        <translation>Цей втулок призначено для запуску зовнішньої команди при зміні поточної доріжки</translation>
    </message>
    <message>
        <location filename="../trackchangefactory.cpp" line="52"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>TrackChangeSettingsDialog</name>
    <message>
        <location filename="../trackchangesettingsdialog.ui" line="14"/>
        <source>Track Change Plugin Settings</source>
        <translation>Налаштування втулка зміни доріжки</translation>
    </message>
    <message>
        <location filename="../trackchangesettingsdialog.ui" line="29"/>
        <source>Command to run when Qmmp starts new track</source>
        <translation>Команда для запуску, коли Qmmp починає нову доріжку</translation>
    </message>
    <message>
        <location filename="../trackchangesettingsdialog.ui" line="39"/>
        <location filename="../trackchangesettingsdialog.ui" line="56"/>
        <location filename="../trackchangesettingsdialog.ui" line="73"/>
        <location filename="../trackchangesettingsdialog.ui" line="90"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../trackchangesettingsdialog.ui" line="46"/>
        <source>Command to run toward to end of a track</source>
        <translation>Команда для запуску в кінці доріжки</translation>
    </message>
    <message>
        <location filename="../trackchangesettingsdialog.ui" line="63"/>
        <source>Command to run when Qmmp reaches the end of the playlist</source>
        <translation>Команда для запуску, коли Qmmp доходить кінця грайлиста</translation>
    </message>
    <message>
        <location filename="../trackchangesettingsdialog.ui" line="80"/>
        <source>Command to run when title changes (i.e. network streams title)</source>
        <translation>Команда для запуску при зміні назви (наприклад, при прослуховуванні онлайн)</translation>
    </message>
    <message>
        <location filename="../trackchangesettingsdialog.ui" line="97"/>
        <source>Command to run on application startup:</source>
        <translation>Команда, що запускається при старті програми:</translation>
    </message>
    <message>
        <location filename="../trackchangesettingsdialog.ui" line="104"/>
        <source>Command to run on application exit:</source>
        <translation>Команда, що запускається при завершенні програми:</translation>
    </message>
</context>
</TS>
