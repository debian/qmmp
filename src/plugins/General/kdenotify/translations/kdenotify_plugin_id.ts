<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id">
<context>
    <name>KdeNotify</name>
    <message>
        <location filename="../kdenotify.cpp" line="120"/>
        <source>Qmmp now playing:</source>
        <translation>Qmmp sekarang memainkan:</translation>
    </message>
</context>
<context>
    <name>KdeNotifyFactory</name>
    <message>
        <location filename="../kdenotifyfactory.cpp" line="29"/>
        <source>KDE notification plugin</source>
        <translation>Plugin pemberitahuan KDE</translation>
    </message>
    <message>
        <location filename="../kdenotifyfactory.cpp" line="49"/>
        <source>About KDE Notification Plugin</source>
        <translation>Tentang Plugin Pemberitahuan KDE</translation>
    </message>
    <message>
        <location filename="../kdenotifyfactory.cpp" line="50"/>
        <source>KDE notification plugin for Qmmp</source>
        <translation>Plugin pemberitahuan KDE untuk Qmmp</translation>
    </message>
</context>
<context>
    <name>KdeNotifySettingsDialog</name>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="14"/>
        <source>KDE Notification Plugin Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="39"/>
        <source>Options</source>
        <translation type="unfinished">Pilihan</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="45"/>
        <source>Notification delay:</source>
        <translation type="unfinished">Tundaan pemberitahuan:</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="52"/>
        <source>Update visible notification instead create new</source>
        <translation type="unfinished">Perbarui pemberitahuan yang terlihat bukannya menciptakan baru</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="72"/>
        <source>s</source>
        <translation type="unfinished">d</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="85"/>
        <source>Volume change notification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="95"/>
        <source>Appearance</source>
        <translation type="unfinished">Tampilan</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="101"/>
        <source>Show covers</source>
        <translation type="unfinished">Tampakkan sampul</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="110"/>
        <source>Edit template</source>
        <translation type="unfinished">Edit template</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.cpp" line="76"/>
        <source>Notification Template</source>
        <translation type="unfinished">Template Pemberitahuan</translation>
    </message>
</context>
</TS>
