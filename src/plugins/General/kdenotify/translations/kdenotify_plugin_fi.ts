<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fi">
<context>
    <name>KdeNotify</name>
    <message>
        <location filename="../kdenotify.cpp" line="120"/>
        <source>Qmmp now playing:</source>
        <translation>Qmmp toistaa:</translation>
    </message>
</context>
<context>
    <name>KdeNotifyFactory</name>
    <message>
        <location filename="../kdenotifyfactory.cpp" line="29"/>
        <source>KDE notification plugin</source>
        <translation>KDE notification plugin</translation>
    </message>
    <message>
        <location filename="../kdenotifyfactory.cpp" line="49"/>
        <source>About KDE Notification Plugin</source>
        <translation>Tietoja: KDE Notification Plugin</translation>
    </message>
    <message>
        <location filename="../kdenotifyfactory.cpp" line="50"/>
        <source>KDE notification plugin for Qmmp</source>
        <translation>Qmmp KDE notification plugin</translation>
    </message>
</context>
<context>
    <name>KdeNotifySettingsDialog</name>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="14"/>
        <source>KDE Notification Plugin Settings</source>
        <translation>KDE Notification Plugin asetukset</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="39"/>
        <source>Options</source>
        <translation>Valinnat</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="45"/>
        <source>Notification delay:</source>
        <translation>Ilmoituksen viive:</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="52"/>
        <source>Update visible notification instead create new</source>
        <translation>Päivitä ilmoitus ja luo uusi</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="72"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="85"/>
        <source>Volume change notification</source>
        <translation>Ilmoitus voimakkuuden muutoksesta</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="95"/>
        <source>Appearance</source>
        <translation>Ulkoasu</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="101"/>
        <source>Show covers</source>
        <translation>Näytä kannet</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="110"/>
        <source>Edit template</source>
        <translation>Muokkaa mallia</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.cpp" line="76"/>
        <source>Notification Template</source>
        <translation>Ilmoituksen malli</translation>
    </message>
</context>
</TS>
