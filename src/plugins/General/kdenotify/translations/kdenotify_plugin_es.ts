<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>KdeNotify</name>
    <message>
        <location filename="../kdenotify.cpp" line="120"/>
        <source>Qmmp now playing:</source>
        <translation>Qmmp está reproduciendo:</translation>
    </message>
</context>
<context>
    <name>KdeNotifyFactory</name>
    <message>
        <location filename="../kdenotifyfactory.cpp" line="29"/>
        <source>KDE notification plugin</source>
        <translation>Complemento de notificación de KDE</translation>
    </message>
    <message>
        <location filename="../kdenotifyfactory.cpp" line="49"/>
        <source>About KDE Notification Plugin</source>
        <translation>Acerca del módulo de notificación KDE</translation>
    </message>
    <message>
        <location filename="../kdenotifyfactory.cpp" line="50"/>
        <source>KDE notification plugin for Qmmp</source>
        <translation>Complemento de notificación de KDE para Qmmp</translation>
    </message>
</context>
<context>
    <name>KdeNotifySettingsDialog</name>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="14"/>
        <source>KDE Notification Plugin Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="39"/>
        <source>Options</source>
        <translation>Opciones</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="45"/>
        <source>Notification delay:</source>
        <translation>Demora de notificación</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="52"/>
        <source>Update visible notification instead create new</source>
        <translation>Actualizar la notificación visible en lugar de crear otra</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="72"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="85"/>
        <source>Volume change notification</source>
        <translation>Aviso de cambio de volumen</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="95"/>
        <source>Appearance</source>
        <translation>Apariencia</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="101"/>
        <source>Show covers</source>
        <translation>Mostrar carátulas</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="110"/>
        <source>Edit template</source>
        <translation>Editar la plantilla</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.cpp" line="76"/>
        <source>Notification Template</source>
        <translation>Plantilla de avisos</translation>
    </message>
</context>
</TS>
