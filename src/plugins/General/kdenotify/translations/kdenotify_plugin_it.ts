<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>KdeNotify</name>
    <message>
        <location filename="../kdenotify.cpp" line="120"/>
        <source>Qmmp now playing:</source>
        <translation>In riproduzione da Qmmp:</translation>
    </message>
</context>
<context>
    <name>KdeNotifyFactory</name>
    <message>
        <location filename="../kdenotifyfactory.cpp" line="29"/>
        <source>KDE notification plugin</source>
        <translation>Estensione notifiche KDE</translation>
    </message>
    <message>
        <location filename="../kdenotifyfactory.cpp" line="49"/>
        <source>About KDE Notification Plugin</source>
        <translation>Informazioni sull&apos;estensione notifiche KDE</translation>
    </message>
    <message>
        <location filename="../kdenotifyfactory.cpp" line="50"/>
        <source>KDE notification plugin for Qmmp</source>
        <translation>Estensione notifiche KDE per Qmmp</translation>
    </message>
</context>
<context>
    <name>KdeNotifySettingsDialog</name>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="14"/>
        <source>KDE Notification Plugin Settings</source>
        <translation>Impostazioni dell&apos;estensione notifiche di KDE</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="39"/>
        <source>Options</source>
        <translation>Opzioni</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="45"/>
        <source>Notification delay:</source>
        <translation>Ritardo notifiche:</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="52"/>
        <source>Update visible notification instead create new</source>
        <translation>Aggiorna la notifica visibile invece di crearne una nuova</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="72"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="85"/>
        <source>Volume change notification</source>
        <translation>Notifica di modifica volume</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="95"/>
        <source>Appearance</source>
        <translation>Aspetto</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="101"/>
        <source>Show covers</source>
        <translation>Mostra copertine</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.ui" line="110"/>
        <source>Edit template</source>
        <translation>Modifica modello</translation>
    </message>
    <message>
        <location filename="../kdenotifysettingsdialog.cpp" line="76"/>
        <source>Notification Template</source>
        <translation>Modello di notifica</translation>
    </message>
</context>
</TS>
