<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>Library</name>
    <message>
        <location filename="../library.cpp" line="63"/>
        <source>Library</source>
        <translation>Biblioteca</translation>
    </message>
    <message>
        <location filename="../library.cpp" line="64"/>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <location filename="../library.cpp" line="70"/>
        <source>Update library</source>
        <translation>Actualizar biblioteca</translation>
    </message>
    <message>
        <location filename="../library.cpp" line="192"/>
        <location filename="../library.cpp" line="193"/>
        <source>Unknown</source>
        <translation>Desconocido</translation>
    </message>
</context>
<context>
    <name>LibraryFactory</name>
    <message>
        <location filename="../libraryfactory.cpp" line="33"/>
        <source>Media Library Plugin</source>
        <translation>Módulo Biblioteca Multimedia</translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="38"/>
        <source>Library</source>
        <translation>Biblioteca</translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="38"/>
        <source>Ctrl+1</source>
        <translation>Ctrl+1</translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="84"/>
        <source>About Media Library Plugin</source>
        <translation>Acerca del Módulo Biblioteca Multimedia</translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="85"/>
        <source>Qmmp Media Library Plugin</source>
        <translation>Módulo Biblioteca Multimedia de Qmmp</translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="86"/>
        <source>This plugin represents a database to store music files tags for a fast access</source>
        <translation>Este módulo consiste en una base de datos para almacenar etiquetas de archivos de música para un acceso más rápido</translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="87"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Escrito por: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>LibraryModel</name>
    <message>
        <location filename="../librarymodel.cpp" line="206"/>
        <source>%1 - %2</source>
        <translation>%1 - %2</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="354"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="354"/>
        <source>Unable to connect to database</source>
        <translation>Imposible conectarse a la base de datos</translation>
    </message>
    <message numerus="yes">
        <location filename="../librarymodel.cpp" line="375"/>
        <source>%n day(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../librarymodel.cpp" line="376"/>
        <source>%n hour(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../librarymodel.cpp" line="377"/>
        <source>%n minute(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../librarymodel.cpp" line="378"/>
        <source>%n second(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="382"/>
        <source>%1 %2 %3 %4</source>
        <comment>days hours minutes seconds</comment>
        <translation>%1 %2 %3 %4</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="384"/>
        <source>%1 %2 %3</source>
        <comment>hours minutes seconds</comment>
        <translation>%1 %2 %3</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="386"/>
        <source>%1 %2</source>
        <comment>minutes seconds</comment>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="389"/>
        <source>Number of tracks: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Número de pistas: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="390"/>
        <source>Number of albums: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Número de álbumes: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="391"/>
        <source>Number of artists: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Número de artistas: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="392"/>
        <source>Total duration: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Duración total: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="395"/>
        <source>Library Information</source>
        <translation>Información de la Biblioteca</translation>
    </message>
</context>
<context>
    <name>LibrarySettingsDialog</name>
    <message>
        <location filename="../librarysettingsdialog.ui" line="14"/>
        <source>Media Library Settings</source>
        <translation>Configuración de la Biblioteca Multimedia</translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.ui" line="39"/>
        <source>Recreate database</source>
        <translation>Recrear base de datos</translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.ui" line="46"/>
        <source>Show album year</source>
        <translation>Mostrar año del álbum</translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.ui" line="55"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.ui" line="66"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.ui" line="95"/>
        <source>List of directories for scanning:</source>
        <translation>Lista de directorios para examinar:</translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.cpp" line="65"/>
        <source>Select Directories for Scanning</source>
        <translation>Seleccionar Directorios para Examinar</translation>
    </message>
</context>
<context>
    <name>LibraryWidget</name>
    <message>
        <location filename="../librarywidget.cpp" line="51"/>
        <source>&amp;Add to Playlist</source>
        <translation>&amp;Añadir a la Lista de Reproducción</translation>
    </message>
    <message>
        <location filename="../librarywidget.cpp" line="52"/>
        <source>Replace Playlist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../librarywidget.cpp" line="53"/>
        <source>&amp;View Track Details</source>
        <translation>&amp;Ver detalles de la pista</translation>
    </message>
    <message>
        <location filename="../librarywidget.cpp" line="55"/>
        <source>Quick Search</source>
        <translation>Búsqueda Rápida</translation>
    </message>
    <message>
        <location filename="../librarywidget.cpp" line="56"/>
        <source>&amp;Library Information</source>
        <translation>Información de &amp;la Biblioteca</translation>
    </message>
    <message>
        <location filename="../librarywidget.cpp" line="90"/>
        <source>Scanning directories...</source>
        <translation>Revisando directorios...</translation>
    </message>
    <message>
        <location filename="../librarywidget.ui" line="14"/>
        <source>Media Library</source>
        <translation>Biblioteca Multimedia</translation>
    </message>
</context>
</TS>
