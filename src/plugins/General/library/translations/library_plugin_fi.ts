<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fi">
<context>
    <name>Library</name>
    <message>
        <location filename="../library.cpp" line="63"/>
        <source>Library</source>
        <translation>Kirjasto</translation>
    </message>
    <message>
        <location filename="../library.cpp" line="64"/>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <location filename="../library.cpp" line="70"/>
        <source>Update library</source>
        <translation>Päivitä kirjasto</translation>
    </message>
    <message>
        <location filename="../library.cpp" line="192"/>
        <location filename="../library.cpp" line="193"/>
        <source>Unknown</source>
        <translation>Tuntematon</translation>
    </message>
</context>
<context>
    <name>LibraryFactory</name>
    <message>
        <location filename="../libraryfactory.cpp" line="33"/>
        <source>Media Library Plugin</source>
        <translation>Media Library Plugin</translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="38"/>
        <source>Library</source>
        <translation>Kirjasto</translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="38"/>
        <source>Ctrl+1</source>
        <translation>Ctrl+1</translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="84"/>
        <source>About Media Library Plugin</source>
        <translation>Tietoja: Media Library Plugin</translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="85"/>
        <source>Qmmp Media Library Plugin</source>
        <translation>Qmmp Media Library Plugin</translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="86"/>
        <source>This plugin represents a database to store music files tags for a fast access</source>
        <translation>Tämä laajennus edustaa tietokantaa musiikkitiedostojen tunnisteiden nopeaa käyttöä varten</translation>
    </message>
    <message>
        <location filename="../libraryfactory.cpp" line="87"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Kirjoittanut: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>LibraryModel</name>
    <message>
        <location filename="../librarymodel.cpp" line="206"/>
        <source>%1 - %2</source>
        <translation>%1 - %2</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="354"/>
        <source>Error</source>
        <translation>Virhe</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="354"/>
        <source>Unable to connect to database</source>
        <translation>Tietokantaan yhdistäminen ei onnistu</translation>
    </message>
    <message numerus="yes">
        <location filename="../librarymodel.cpp" line="375"/>
        <source>%n day(s)</source>
        <translation>
            <numerusform>%n päivää</numerusform>
            <numerusform>%n päivää</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../librarymodel.cpp" line="376"/>
        <source>%n hour(s)</source>
        <translation>
            <numerusform>%n tuntia</numerusform>
            <numerusform>%n tuntia</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../librarymodel.cpp" line="377"/>
        <source>%n minute(s)</source>
        <translation>
            <numerusform>%n minuutia</numerusform>
            <numerusform>%n minuuttia</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../librarymodel.cpp" line="378"/>
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n sekuntia</numerusform>
            <numerusform>%n sekuntia</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="382"/>
        <source>%1 %2 %3 %4</source>
        <comment>days hours minutes seconds</comment>
        <translation>%1 %2 %3 %4</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="384"/>
        <source>%1 %2 %3</source>
        <comment>hours minutes seconds</comment>
        <translation>%1 %2 %3</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="386"/>
        <source>%1 %2</source>
        <comment>minutes seconds</comment>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="389"/>
        <source>Number of tracks: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Kappaleita: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="390"/>
        <source>Number of albums: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Albumeita: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="391"/>
        <source>Number of artists: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Artisteja: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="392"/>
        <source>Total duration: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Kesto: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../librarymodel.cpp" line="395"/>
        <source>Library Information</source>
        <translation>Kirjaston tiedot</translation>
    </message>
</context>
<context>
    <name>LibrarySettingsDialog</name>
    <message>
        <location filename="../librarysettingsdialog.ui" line="14"/>
        <source>Media Library Settings</source>
        <translation>Asetukset Media Library</translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.ui" line="39"/>
        <source>Recreate database</source>
        <translation>Luo tietokanta uudelleen</translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.ui" line="46"/>
        <source>Show album year</source>
        <translation>Näytä albumin vuosi</translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.ui" line="55"/>
        <source>Add</source>
        <translation>Lisää</translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.ui" line="66"/>
        <source>Remove</source>
        <translation>Poista</translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.ui" line="95"/>
        <source>List of directories for scanning:</source>
        <translation>Luettelo läpikäytävistä kansioista:</translation>
    </message>
    <message>
        <location filename="../librarysettingsdialog.cpp" line="65"/>
        <source>Select Directories for Scanning</source>
        <translation>Valitse kansiot läpikäyntiä varten</translation>
    </message>
</context>
<context>
    <name>LibraryWidget</name>
    <message>
        <location filename="../librarywidget.cpp" line="51"/>
        <source>&amp;Add to Playlist</source>
        <translation>&amp;Lisää soittolistaan</translation>
    </message>
    <message>
        <location filename="../librarywidget.cpp" line="52"/>
        <source>Replace Playlist</source>
        <translation>Korvaa soittolista</translation>
    </message>
    <message>
        <location filename="../librarywidget.cpp" line="53"/>
        <source>&amp;View Track Details</source>
        <translation>Näy&amp;tä kappaleen tiedot</translation>
    </message>
    <message>
        <location filename="../librarywidget.cpp" line="55"/>
        <source>Quick Search</source>
        <translation>Pikahaku</translation>
    </message>
    <message>
        <location filename="../librarywidget.cpp" line="56"/>
        <source>&amp;Library Information</source>
        <translation>&amp;Kirjaston tiedot</translation>
    </message>
    <message>
        <location filename="../librarywidget.cpp" line="90"/>
        <source>Scanning directories...</source>
        <translation>Kartoitetaan kansioita...</translation>
    </message>
    <message>
        <location filename="../librarywidget.ui" line="14"/>
        <source>Media Library</source>
        <translation>Media Library</translation>
    </message>
</context>
</TS>
