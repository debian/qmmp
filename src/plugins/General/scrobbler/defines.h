/***************************************************************************
 *   Copyright (C) 2013-2025 by Ilya Kotov                                 *
 *   forkotov02@ya.ru                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/

#ifndef DEFINES_H
#define DEFINES_H

#define SCROBBLER_LASTFM_URL u"http://ws.audioscrobbler.com/2.0/"_s
#define SCROBBLER_LIBREFM_URL u"https://libre.fm/2.0/"_s

#define LASTFM_AUTH_URL u"http://www.last.fm/api/auth/"_s
#define LIBREFM_AUTH_URL u"https://libre.fm/api/auth/"_s

#endif // DEFINES_H
