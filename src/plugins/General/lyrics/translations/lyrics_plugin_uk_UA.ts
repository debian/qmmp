<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>Lyrics</name>
    <message>
        <location filename="../lyrics.cpp" line="34"/>
        <source>View Lyrics</source>
        <translation>Огляд текстів</translation>
    </message>
    <message>
        <location filename="../lyrics.cpp" line="35"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
</context>
<context>
    <name>LyricsFactory</name>
    <message>
        <location filename="../lyricsfactory.cpp" line="31"/>
        <source>Lyrics Plugin</source>
        <translation>Модуль текстів</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="36"/>
        <source>Lyrics</source>
        <translation>Текст пісні</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="36"/>
        <source>Ctrl+2</source>
        <translation>Ctrl+2</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="68"/>
        <source>About Lyrics Plugin</source>
        <translation>Про модуль текстів</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="69"/>
        <source>Qmmp Lyrics Plugin</source>
        <translation>Модуль текстів для Qmmp</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="70"/>
        <source>This plugin retrieves lyrics from LyricWiki</source>
        <translation>Цей втулок отримує тексти з LyricWiki</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="71"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="72"/>
        <source>Based on Ultimate Lyrics script by Vladimir Brkic &lt;vladimir_brkic@yahoo.com&gt;</source>
        <translation>Базується на сценарії Ultimate Lyrics від Володимира Бркича &lt;vladimir_brkic@yahoo.com&gt;</translation>
    </message>
</context>
<context>
    <name>LyricsSettingsDialog</name>
    <message>
        <location filename="../lyricssettingsdialog.ui" line="14"/>
        <source>Lyrics Plugin Settings</source>
        <translation>Налаштування втулка текстів пісень</translation>
    </message>
    <message>
        <location filename="../lyricssettingsdialog.ui" line="29"/>
        <source>Lyrics providers:</source>
        <translation>Постачальники текстів пісень:</translation>
    </message>
</context>
<context>
    <name>LyricsWidget</name>
    <message>
        <location filename="../lyricswidget.ui" line="14"/>
        <source>Lyrics Plugin</source>
        <translation>Модуль текстів</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="31"/>
        <source>Provider:</source>
        <translation>Постачальник:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="115"/>
        <source>Title:</source>
        <translation>Заголовок:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="132"/>
        <source>Album:</source>
        <translation>Альбом:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="142"/>
        <source>Artist:</source>
        <translation>Виконавець:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="159"/>
        <source>Track:</source>
        <translation>Доріжка:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="171"/>
        <source>Year:</source>
        <translation>Рік:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="148"/>
        <location filename="../lyricswidget.cpp" line="234"/>
        <source>&lt;h2&gt;%1 - %2&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;%1 - %2&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="159"/>
        <source>Not found</source>
        <translation>Не знайдено</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="169"/>
        <source>Error: %1 - %2</source>
        <translation>Помилка: %1 - %2</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="182"/>
        <source>Receiving</source>
        <translation>Отримання</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="236"/>
        <source>Tag</source>
        <translation>Теґ</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="257"/>
        <source>Cache</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UltimateLyricsParser</name>
    <message>
        <location filename="../ultimatelyricsparser.cpp" line="116"/>
        <source>%1 (line: %2)</source>
        <translation>%1 (рядок: %2)</translation>
    </message>
</context>
</TS>
