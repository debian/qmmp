<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>Lyrics</name>
    <message>
        <location filename="../lyrics.cpp" line="34"/>
        <source>View Lyrics</source>
        <translation>Liedtext anzeigen</translation>
    </message>
    <message>
        <location filename="../lyrics.cpp" line="35"/>
        <source>Ctrl+L</source>
        <translation>Strg+L</translation>
    </message>
</context>
<context>
    <name>LyricsFactory</name>
    <message>
        <location filename="../lyricsfactory.cpp" line="31"/>
        <source>Lyrics Plugin</source>
        <translation>Liedtext-Modul</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="36"/>
        <source>Lyrics</source>
        <translation>Liedtext</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="36"/>
        <source>Ctrl+2</source>
        <translation>Strg+2</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="68"/>
        <source>About Lyrics Plugin</source>
        <translation>Über Liedtext-Modul</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="69"/>
        <source>Qmmp Lyrics Plugin</source>
        <translation>Qmmp Liedtext-Modul</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="70"/>
        <source>This plugin retrieves lyrics from LyricWiki</source>
        <translation>Dieses Modul empfängt Liedtexte von LyricWiki</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="71"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Geschrieben von: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../lyricsfactory.cpp" line="72"/>
        <source>Based on Ultimate Lyrics script by Vladimir Brkic &lt;vladimir_brkic@yahoo.com&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LyricsSettingsDialog</name>
    <message>
        <location filename="../lyricssettingsdialog.ui" line="14"/>
        <source>Lyrics Plugin Settings</source>
        <translation>Einstellungen Liedtext-Modul</translation>
    </message>
    <message>
        <location filename="../lyricssettingsdialog.ui" line="29"/>
        <source>Lyrics providers:</source>
        <translation>Anbieter von Liedtexten:</translation>
    </message>
</context>
<context>
    <name>LyricsWidget</name>
    <message>
        <location filename="../lyricswidget.ui" line="14"/>
        <source>Lyrics Plugin</source>
        <translation>Liedtext-Modul</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="31"/>
        <source>Provider:</source>
        <translation>Anbieter:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="115"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="132"/>
        <source>Album:</source>
        <translation>Album:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="142"/>
        <source>Artist:</source>
        <translation>Interpret:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="159"/>
        <source>Track:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.ui" line="171"/>
        <source>Year:</source>
        <translation>Jahr:</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="148"/>
        <location filename="../lyricswidget.cpp" line="234"/>
        <source>&lt;h2&gt;%1 - %2&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;%1 - %2&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="159"/>
        <source>Not found</source>
        <translation>Es kann kein Liedtext für dieses Stück gefunden werden.</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="169"/>
        <source>Error: %1 - %2</source>
        <translation>Fehler: %1 - %2</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="182"/>
        <source>Receiving</source>
        <translation>Daten werden empfangen</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="236"/>
        <source>Tag</source>
        <translation>Schlagwort</translation>
    </message>
    <message>
        <location filename="../lyricswidget.cpp" line="257"/>
        <source>Cache</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UltimateLyricsParser</name>
    <message>
        <location filename="../ultimatelyricsparser.cpp" line="116"/>
        <source>%1 (line: %2)</source>
        <translation>%1 (Zeile: %2)</translation>
    </message>
</context>
</TS>
