<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>SleepInhibitFactory</name>
    <message>
        <location filename="../sleepinhibitorfactory.cpp" line="28"/>
        <source>Sleep Mode Inhibition Plugin</source>
        <translation>Втулок блокування режиму сну</translation>
    </message>
    <message>
        <location filename="../sleepinhibitorfactory.cpp" line="49"/>
        <source>About Sleep Mode Inhibit Plugin</source>
        <translation>Про втулок блокування режиму сну</translation>
    </message>
    <message>
        <location filename="../sleepinhibitorfactory.cpp" line="50"/>
        <source>Qmmp Sleep Mode Inhibit Plugin</source>
        <translation>Втулок блокування режиму сну для Qmmp</translation>
    </message>
    <message>
        <location filename="../sleepinhibitorfactory.cpp" line="51"/>
        <source>This plugin inhibits sleep mode while audio playback</source>
        <translation>Цей втулок блокує режим сну під час відтворення звуку</translation>
    </message>
    <message>
        <location filename="../sleepinhibitorfactory.cpp" line="52"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов  &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
</TS>
