/***************************************************************************
 *   Copyright (C) 2009-2025 by Ilya Kotov                                 *
 *   forkotov02@ya.ru                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/
#ifndef FILEOPSSETTINGSDIALOG_H
#define FILEOPSSETTINGSDIALOG_H

#include <QDialog>

class QComboBox;
class QTableWidgetItem;

namespace Ui {
class FileOpsSettingsDialog;
}

/**
    @author Ilya Kotov <forkotov02@ya.ru>
*/
class FileOpsSettingsDialog : public QDialog
{
    Q_OBJECT
public:
    explicit FileOpsSettingsDialog(QWidget *parent = nullptr);

    ~FileOpsSettingsDialog();


public slots:
    virtual void accept() override;

private slots:
    void on_newButton_clicked();
    void on_deleteButton_clicked();
    void updateLineEdits();
    void on_destinationEdit_textChanged(QString dest);
    void on_patternEdit_textChanged(QString pattern);
    void addTitleString(const QString &str);
    void on_destButton_clicked();
    void on_tableWidget_itemDoubleClicked(QTableWidgetItem *item);

private:
    void createMenus();
    QComboBox *createComboBox();

    Ui::FileOpsSettingsDialog *m_ui;

    enum DataTypeRole
    {
        PatternRole = Qt::UserRole + 1,
        DestionationRole,
        CommandRole
    };
};

#endif
