<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>FileOps</name>
    <message>
        <location filename="../fileops.cpp" line="107"/>
        <location filename="../fileops.cpp" line="150"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="108"/>
        <location filename="../fileops.cpp" line="151"/>
        <source>Destination directory doesn&apos;t exist</source>
        <translation>A pasta de destino não existe</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="154"/>
        <source>Move Files</source>
        <translation>Mover arquivos</translation>
    </message>
    <message numerus="yes">
        <location filename="../fileops.cpp" line="155"/>
        <source>Are you sure you want to move %n file(s)?</source>
        <translation>
            <numerusform>Tem certeza que deseja mover %n arquivo?</numerusform>
            <numerusform>Tem certeza que deseja mover %n arquivos?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="177"/>
        <source>Copying</source>
        <translation>Copiando</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="178"/>
        <location filename="../fileops.cpp" line="275"/>
        <source>Stop</source>
        <translation>Parar</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="223"/>
        <source>Copying file %1/%2</source>
        <translation>Copiando arquivo %1/%2</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="274"/>
        <source>Moving</source>
        <translation>Movendo</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="313"/>
        <source>Moving file %1/%2</source>
        <translation>Movendo arquivo %1/%2</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="123"/>
        <source>Remove Files</source>
        <translation>Remover arquivos</translation>
    </message>
    <message numerus="yes">
        <location filename="../fileops.cpp" line="124"/>
        <source>Are you sure you want to remove %n file(s) from disk?</source>
        <translation>
            <numerusform>Tem certeza que quer apagar %n arquivo(s) do disco?</numerusform>
            <numerusform>Tem certeza que quer apagar %n arquivo(s) do disco?</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>FileOpsFactory</name>
    <message>
        <location filename="../fileopsfactory.cpp" line="29"/>
        <source>File Operations Plugin</source>
        <translation>Plugin File Operations</translation>
    </message>
    <message>
        <location filename="../fileopsfactory.cpp" line="49"/>
        <source>About File Operations Plugin</source>
        <translation>Sobre o plugin File Operations</translation>
    </message>
    <message>
        <location filename="../fileopsfactory.cpp" line="50"/>
        <source>Qmmp File Operations Plugin</source>
        <translation>Plugin Qmmp File Operations</translation>
    </message>
    <message>
        <location filename="../fileopsfactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileOpsSettingsDialog</name>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="14"/>
        <source>File Operations Settings</source>
        <translation>Preferências de File Operations</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="51"/>
        <source>Enabled</source>
        <translation>Ativo</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="56"/>
        <source>Operation</source>
        <translation>Operação</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="61"/>
        <source>Menu text</source>
        <translation>Texto do menu</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="66"/>
        <source>Shortcut</source>
        <translation>Atalho</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="82"/>
        <source>Add</source>
        <translation>Adicionar</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="99"/>
        <location filename="../fileopssettingsdialog.cpp" line="222"/>
        <source>Remove</source>
        <translation>Remover</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="127"/>
        <source>Destination:</source>
        <translation>Destino:</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="137"/>
        <location filename="../fileopssettingsdialog.ui" line="154"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="144"/>
        <location filename="../fileopssettingsdialog.cpp" line="155"/>
        <source>File name pattern:</source>
        <translation>Padrão para nome de arquivo:</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="127"/>
        <source>New action</source>
        <translation>Nova ação</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="184"/>
        <source>Command:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="219"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="220"/>
        <source>Rename</source>
        <translation>Renomear</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="221"/>
        <source>Move</source>
        <translation>Mover</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="223"/>
        <source>Execute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="235"/>
        <source>Choose a directory</source>
        <translation>Escolha a pasta</translation>
    </message>
</context>
</TS>
