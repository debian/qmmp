<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>FileOps</name>
    <message>
        <location filename="../fileops.cpp" line="107"/>
        <location filename="../fileops.cpp" line="150"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="108"/>
        <location filename="../fileops.cpp" line="151"/>
        <source>Destination directory doesn&apos;t exist</source>
        <translation>目标目录不存在</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="154"/>
        <source>Move Files</source>
        <translation>转移文件</translation>
    </message>
    <message numerus="yes">
        <location filename="../fileops.cpp" line="155"/>
        <source>Are you sure you want to move %n file(s)?</source>
        <translation>
            <numerusform>你确定你希望移动%n文件(文件组)吗？</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="177"/>
        <source>Copying</source>
        <translation>正在复制</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="178"/>
        <location filename="../fileops.cpp" line="275"/>
        <source>Stop</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="223"/>
        <source>Copying file %1/%2</source>
        <translation>正在复制文件 %1/%2</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="274"/>
        <source>Moving</source>
        <translation>移动中</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="313"/>
        <source>Moving file %1/%2</source>
        <translation>正在移动文件%1%2</translation>
    </message>
    <message>
        <location filename="../fileops.cpp" line="123"/>
        <source>Remove Files</source>
        <translation>删除文件</translation>
    </message>
    <message numerus="yes">
        <location filename="../fileops.cpp" line="124"/>
        <source>Are you sure you want to remove %n file(s) from disk?</source>
        <translation>
            <numerusform>你确认要从磁盘中删除 %n 文件？</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>FileOpsFactory</name>
    <message>
        <location filename="../fileopsfactory.cpp" line="29"/>
        <source>File Operations Plugin</source>
        <translation>文件操作插件</translation>
    </message>
    <message>
        <location filename="../fileopsfactory.cpp" line="49"/>
        <source>About File Operations Plugin</source>
        <translation>关于文件操作插件</translation>
    </message>
    <message>
        <location filename="../fileopsfactory.cpp" line="50"/>
        <source>Qmmp File Operations Plugin</source>
        <translation>Qmmp 文件操作插件</translation>
    </message>
    <message>
        <location filename="../fileopsfactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileOpsSettingsDialog</name>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="14"/>
        <source>File Operations Settings</source>
        <translation>文件操作设置</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="51"/>
        <source>Enabled</source>
        <translation>启用</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="56"/>
        <source>Operation</source>
        <translation>操作</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="61"/>
        <source>Menu text</source>
        <translation>菜单文本</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="66"/>
        <source>Shortcut</source>
        <translation>快捷键</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="82"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="99"/>
        <location filename="../fileopssettingsdialog.cpp" line="222"/>
        <source>Remove</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="127"/>
        <source>Destination:</source>
        <translation>目标目录：</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="137"/>
        <location filename="../fileopssettingsdialog.ui" line="154"/>
        <source>...</source>
        <translation>…</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.ui" line="144"/>
        <location filename="../fileopssettingsdialog.cpp" line="155"/>
        <source>File name pattern:</source>
        <translation>文件名方案：</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="127"/>
        <source>New action</source>
        <translation>新建动作</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="184"/>
        <source>Command:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="219"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="220"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="221"/>
        <source>Move</source>
        <translation>移动</translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="223"/>
        <source>Execute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileopssettingsdialog.cpp" line="235"/>
        <source>Choose a directory</source>
        <translation>选择文件夹</translation>
    </message>
</context>
</TS>
