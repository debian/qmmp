<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id">
<context>
    <name>NotifierFactory</name>
    <message>
        <location filename="../notifierfactory.cpp" line="29"/>
        <source>Notifier Plugin</source>
        <translation>Plugin Notifier</translation>
    </message>
    <message>
        <location filename="../notifierfactory.cpp" line="49"/>
        <source>About Notifier Plugin</source>
        <translation>Tentang Plugin Notifier</translation>
    </message>
    <message>
        <location filename="../notifierfactory.cpp" line="50"/>
        <source>Qmmp Notifier Plugin</source>
        <translation>Plugin Notifier Qmmp</translation>
    </message>
    <message>
        <location filename="../notifierfactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Ditulis oleh: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>NotifierSettingsDialog</name>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="14"/>
        <source>Notifier Plugin Settings</source>
        <translation type="unfinished">Setelan Plugin Notifier</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="35"/>
        <source>Desktop Notification</source>
        <translation type="unfinished">Pemberitahuan Desktop</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="51"/>
        <source>Font:</source>
        <translation type="unfinished">Fonta:</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="71"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="78"/>
        <location filename="../notifiersettingsdialog.ui" line="279"/>
        <source>0</source>
        <translation type="unfinished">0</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="85"/>
        <source>Transparency:</source>
        <translation type="unfinished">Transparan:</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="229"/>
        <source>Position</source>
        <translation type="unfinished">Posisi</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="251"/>
        <source>Edit template</source>
        <translation type="unfinished">Edit template</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="299"/>
        <source>Cover size:</source>
        <translation type="unfinished">Ukuran sampul:</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="325"/>
        <source>Volume change notification</source>
        <translation type="unfinished">Pemberitahuan perubahan volume</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="332"/>
        <source>Delay (ms):</source>
        <translation type="unfinished">Tunda (ms):</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="361"/>
        <source>Playback resume notification</source>
        <translation type="unfinished">Pemberitahuan resume playback</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="368"/>
        <source>Song change notification</source>
        <translation type="unfinished">Pemberitahuan perubahan lagu</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="375"/>
        <source>Disable notifications when another application is in the Full Screen Mode</source>
        <translation type="unfinished">Nonaktifkan pemberitahuan ketika aplikasi yang lain dalam Mode Layar Penuh</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="378"/>
        <source>Disable for full screen windows</source>
        <translation type="unfinished">Nonaktifkan untuk jendela layar penuh</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="388"/>
        <source>Psi Notification</source>
        <translation type="unfinished">Pemberitahuan Psi</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="400"/>
        <source>Enable Psi notification</source>
        <translation type="unfinished">Aktifkan pemberitahuan Psi</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.cpp" line="115"/>
        <source>Notification Template</source>
        <translation type="unfinished">Template Pemberitahuan</translation>
    </message>
</context>
<context>
    <name>PopupWidget</name>
    <message>
        <location filename="../popupwidget.cpp" line="115"/>
        <source>Volume:</source>
        <translation>Volume:</translation>
    </message>
</context>
</TS>
