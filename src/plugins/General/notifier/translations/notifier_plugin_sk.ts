<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sk">
<context>
    <name>NotifierFactory</name>
    <message>
        <location filename="../notifierfactory.cpp" line="29"/>
        <source>Notifier Plugin</source>
        <translation>Notifier Plugin</translation>
    </message>
    <message>
        <location filename="../notifierfactory.cpp" line="49"/>
        <source>About Notifier Plugin</source>
        <translation>O notifikačnom plugine</translation>
    </message>
    <message>
        <location filename="../notifierfactory.cpp" line="50"/>
        <source>Qmmp Notifier Plugin</source>
        <translation>Qmmp notifikačný plugin</translation>
    </message>
    <message>
        <location filename="../notifierfactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NotifierSettingsDialog</name>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="14"/>
        <source>Notifier Plugin Settings</source>
        <translation type="unfinished">Nastavenia notfikačného pluginu</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="35"/>
        <source>Desktop Notification</source>
        <translation type="unfinished">Notifkácia na ploche</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="51"/>
        <source>Font:</source>
        <translation type="unfinished">Písmo:</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="71"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="78"/>
        <location filename="../notifiersettingsdialog.ui" line="279"/>
        <source>0</source>
        <translation type="unfinished">0</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="85"/>
        <source>Transparency:</source>
        <translation type="unfinished">Priehľadnosť:</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="229"/>
        <source>Position</source>
        <translation type="unfinished">Umiestnenie</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="251"/>
        <source>Edit template</source>
        <translation type="unfinished">Upraviť šablónu</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="299"/>
        <source>Cover size:</source>
        <translation type="unfinished">Veľkosť obalu:</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="325"/>
        <source>Volume change notification</source>
        <translation type="unfinished">Notifikácia zmeny hlasitosti</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="332"/>
        <source>Delay (ms):</source>
        <translation type="unfinished">Oneskorenie (ms):</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="361"/>
        <source>Playback resume notification</source>
        <translation type="unfinished">Notifikácia obnovenia prehrávania</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="368"/>
        <source>Song change notification</source>
        <translation type="unfinished">Notifikácia zmeny skladby</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="375"/>
        <source>Disable notifications when another application is in the Full Screen Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="378"/>
        <source>Disable for full screen windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="388"/>
        <source>Psi Notification</source>
        <translation type="unfinished">Psi notifikácia</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.ui" line="400"/>
        <source>Enable Psi notification</source>
        <translation type="unfinished">Povoliť Psi notifikáciu</translation>
    </message>
    <message>
        <location filename="../notifiersettingsdialog.cpp" line="115"/>
        <source>Notification Template</source>
        <translation type="unfinished">Šablóna notifikácie</translation>
    </message>
</context>
<context>
    <name>PopupWidget</name>
    <message>
        <location filename="../popupwidget.cpp" line="115"/>
        <source>Volume:</source>
        <translation>Hlasitosť:</translation>
    </message>
</context>
</TS>
