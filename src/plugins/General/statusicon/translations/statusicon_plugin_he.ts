<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="he">
<context>
    <name>StatusIcon</name>
    <message>
        <location filename="../statusicon.cpp" line="70"/>
        <source>Play</source>
        <translation>נגן</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="71"/>
        <source>Pause</source>
        <translation>השהה</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="72"/>
        <source>Stop</source>
        <translation>הפסק</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="74"/>
        <source>Next</source>
        <translation>הבא</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="75"/>
        <source>Previous</source>
        <translation>הקודם</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="77"/>
        <source>Exit</source>
        <translation>יציאה</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="119"/>
        <source>Stopped</source>
        <translation>הופסקה</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="144"/>
        <source>Now Playing</source>
        <translation>מנגן עכשיו</translation>
    </message>
</context>
<context>
    <name>StatusIconFactory</name>
    <message>
        <location filename="../statusiconfactory.cpp" line="29"/>
        <source>Status Icon Plugin</source>
        <translation>תוסף סמל מצב</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="49"/>
        <source>About Status Icon Plugin</source>
        <translation>אודות תוסף סמל מצב</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="50"/>
        <source>Qmmp Status Icon Plugin</source>
        <translation>תוסף סמל מצב של Qmmp</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="51"/>
        <source>Written by:</source>
        <translation>חובר על ידי:</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="52"/>
        <source>Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="53"/>
        <source>Artur Guzik &lt;a.guzik88@gmail.com&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StatusIconSettingsDialog</name>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="14"/>
        <source>Status Icon Plugin Settings</source>
        <translation type="unfinished">הגדרות תוסף סמל מצב</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="29"/>
        <source>Balloon message</source>
        <translation type="unfinished">הודעת בלון</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="38"/>
        <location filename="../statusiconsettingsdialog.ui" line="105"/>
        <source>Delay, ms:</source>
        <translation type="unfinished">שיהוי, מ״ש:</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="79"/>
        <source>Tooltip</source>
        <translation type="unfinished">כיתוב קופץ</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="91"/>
        <source>Try to split file name when no tag</source>
        <translation type="unfinished">נסה לפצל שם קובץ כאשר אין תגית</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="98"/>
        <source>Show progress bar</source>
        <translation type="unfinished">הצג שורת התקדמות</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="131"/>
        <source>Transparency:</source>
        <translation type="unfinished">שקיפות:</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="165"/>
        <source>0</source>
        <translation type="unfinished">0</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="174"/>
        <source>Cover size:</source>
        <translation type="unfinished">מידת כיסוי:</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="214"/>
        <source>32</source>
        <translation type="unfinished">32</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="225"/>
        <source>Edit template</source>
        <translation type="unfinished">ערוך תבנית</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="250"/>
        <source>Use standard icons</source>
        <translation type="unfinished">השתמש בסמלים רגילים</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.cpp" line="85"/>
        <source>Tooltip Template</source>
        <translation type="unfinished">תבנית תיבת עזר</translation>
    </message>
</context>
</TS>
