<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>StatusIcon</name>
    <message>
        <location filename="../statusicon.cpp" line="70"/>
        <source>Play</source>
        <translation>Riproduci</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="71"/>
        <source>Pause</source>
        <translation>Pausa</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="72"/>
        <source>Stop</source>
        <translation>Ferma</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="74"/>
        <source>Next</source>
        <translation>Successivo</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="75"/>
        <source>Previous</source>
        <translation>Precedente</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="77"/>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="119"/>
        <source>Stopped</source>
        <translation>Fermato</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="144"/>
        <source>Now Playing</source>
        <translation>In riproduzione</translation>
    </message>
</context>
<context>
    <name>StatusIconFactory</name>
    <message>
        <location filename="../statusiconfactory.cpp" line="29"/>
        <source>Status Icon Plugin</source>
        <translation>Estensione icona di stato</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="49"/>
        <source>About Status Icon Plugin</source>
        <translation>Informazioni sull&apos;estensione icona di stato</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="50"/>
        <source>Qmmp Status Icon Plugin</source>
        <translation>Estensione icona di stato per Qmmp</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="51"/>
        <source>Written by:</source>
        <translation>Autori:</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="52"/>
        <source>Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="53"/>
        <source>Artur Guzik &lt;a.guzik88@gmail.com&gt;</source>
        <translation>Artur Guzik &lt;a.guzik88@gmail.com&gt;</translation>
    </message>
</context>
<context>
    <name>StatusIconSettingsDialog</name>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="14"/>
        <source>Status Icon Plugin Settings</source>
        <translation>Impostazione dell&apos;estensione icona di stato</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="29"/>
        <source>Balloon message</source>
        <translation>Messaggio a comparsa</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="38"/>
        <location filename="../statusiconsettingsdialog.ui" line="105"/>
        <source>Delay, ms:</source>
        <translation>Ritardo, ms:</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="79"/>
        <source>Tooltip</source>
        <translation>Suggerimento</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="91"/>
        <source>Try to split file name when no tag</source>
        <translation>Dividi nome file se senza tag</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="98"/>
        <source>Show progress bar</source>
        <translation>Mostra barra di avanzamento</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="131"/>
        <source>Transparency:</source>
        <translation>Trasparenza:</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="165"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="174"/>
        <source>Cover size:</source>
        <translation>Dimensione copertina:</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="214"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="225"/>
        <source>Edit template</source>
        <translation>Modifica modello</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="250"/>
        <source>Use standard icons</source>
        <translation>Usa icone standard</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.cpp" line="85"/>
        <source>Tooltip Template</source>
        <translation>Modello suggerimento</translation>
    </message>
</context>
</TS>
