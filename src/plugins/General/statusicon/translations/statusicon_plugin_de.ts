<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>StatusIcon</name>
    <message>
        <location filename="../statusicon.cpp" line="70"/>
        <source>Play</source>
        <translation>Wiedergabe</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="71"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="72"/>
        <source>Stop</source>
        <translation>Stopp</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="74"/>
        <source>Next</source>
        <translation>Nächster</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="75"/>
        <source>Previous</source>
        <translation>Vorheriger</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="77"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="119"/>
        <source>Stopped</source>
        <translation>Gestoppt</translation>
    </message>
    <message>
        <location filename="../statusicon.cpp" line="144"/>
        <source>Now Playing</source>
        <translation>Aktueller Titel</translation>
    </message>
</context>
<context>
    <name>StatusIconFactory</name>
    <message>
        <location filename="../statusiconfactory.cpp" line="29"/>
        <source>Status Icon Plugin</source>
        <translation>Statussymbolmodul</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="49"/>
        <source>About Status Icon Plugin</source>
        <translation>Über Statussymbolmodul</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="50"/>
        <source>Qmmp Status Icon Plugin</source>
        <translation>Qmmp Statussymbolmodul</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="51"/>
        <source>Written by:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="52"/>
        <source>Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
    <message>
        <location filename="../statusiconfactory.cpp" line="53"/>
        <source>Artur Guzik &lt;a.guzik88@gmail.com&gt;</source>
        <translation>Artur Guzik &lt;a.guzik88@gmail.com&gt;</translation>
    </message>
</context>
<context>
    <name>StatusIconSettingsDialog</name>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="14"/>
        <source>Status Icon Plugin Settings</source>
        <translation>Einstellungen Statussymbolmodul</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="29"/>
        <source>Balloon message</source>
        <translation>Ballon-Nachricht</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="38"/>
        <location filename="../statusiconsettingsdialog.ui" line="105"/>
        <source>Delay, ms:</source>
        <translation>Anzeigedauer (ms):</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="79"/>
        <source>Tooltip</source>
        <translation>Kurzinfo</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="91"/>
        <source>Try to split file name when no tag</source>
        <translation>Bei keinem Schlagwort versuchen Dateinamen zu trennen</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="98"/>
        <source>Show progress bar</source>
        <translation>Fortschrittsbalken anzeigen</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="131"/>
        <source>Transparency:</source>
        <translation>Transparenz:</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="165"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="174"/>
        <source>Cover size:</source>
        <translation>Hüllengröße:</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="214"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="225"/>
        <source>Edit template</source>
        <translation>Vorlage bearbeiten</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.ui" line="250"/>
        <source>Use standard icons</source>
        <translation>System-Symbole benutzen</translation>
    </message>
    <message>
        <location filename="../statusiconsettingsdialog.cpp" line="85"/>
        <source>Tooltip Template</source>
        <translation>Kurzinfo-Vorlage</translation>
    </message>
</context>
</TS>
