<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>UDisksFactory</name>
    <message>
        <location filename="../udisksfactory.cpp" line="30"/>
        <source>UDisks Plugin</source>
        <translation>UDisks 插件</translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="50"/>
        <source>About UDisks Plugin</source>
        <translation>关于 UDisks 插件</translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="51"/>
        <source>Qmmp UDisks Plugin</source>
        <translation>Qmmp UDisks 插件</translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="52"/>
        <source>This plugin provides removable devices detection using UDisks</source>
        <translation>此插件依据可移动检测使用 UDisks</translation>
    </message>
    <message>
        <location filename="../udisksfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UDisksPlugin</name>
    <message>
        <location filename="../udisksplugin.cpp" line="132"/>
        <source>Add CD &quot;%1&quot;</source>
        <translation>添加 CD &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../udisksplugin.cpp" line="140"/>
        <source>Add Volume &quot;%1&quot;</source>
        <translation>添加卷 &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>UDisksSettingsDialog</name>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="14"/>
        <source>UDisks Plugin Settings</source>
        <translation>UDisks 插件设置</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="29"/>
        <source>CD Audio Detection</source>
        <translation>CD 音频检测</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="38"/>
        <source>Add tracks to playlist automatically</source>
        <translation>自动添加音轨到播放列表</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="45"/>
        <source>Remove tracks from playlist automatically</source>
        <translation>自动从播放列表删除音轨</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="55"/>
        <source>Removable Device Detection</source>
        <translation>可移动设备检测</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="64"/>
        <source>Add files to playlist automatically</source>
        <translation>自动添加文件到播放列表</translation>
    </message>
    <message>
        <location filename="../udiskssettingsdialog.ui" line="71"/>
        <source>Remove files from playlist automatically</source>
        <translation>自动从播放列表删除文件</translation>
    </message>
</context>
</TS>
