<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>EditStreamDialog</name>
    <message>
        <location filename="../editstreamdialog.ui" line="14"/>
        <source>Edit Stream</source>
        <translation>Akışı Düzenle</translation>
    </message>
    <message>
        <location filename="../editstreamdialog.ui" line="34"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../editstreamdialog.ui" line="44"/>
        <source>Name:</source>
        <translation>Ad:</translation>
    </message>
    <message>
        <location filename="../editstreamdialog.ui" line="54"/>
        <source>Genre:</source>
        <translation>Tarz:</translation>
    </message>
    <message>
        <location filename="../editstreamdialog.ui" line="64"/>
        <source>Bitrate:</source>
        <translation>Bit hızı:</translation>
    </message>
    <message>
        <location filename="../editstreamdialog.ui" line="74"/>
        <source>Type:</source>
        <translation>Tür:</translation>
    </message>
</context>
<context>
    <name>StreamBrowser</name>
    <message>
        <location filename="../streambrowser.cpp" line="33"/>
        <source>Add Stream</source>
        <translation>Akış Ekle</translation>
    </message>
    <message>
        <location filename="../streambrowser.cpp" line="34"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
</context>
<context>
    <name>StreamBrowserFactory</name>
    <message>
        <location filename="../streambrowserfactory.cpp" line="29"/>
        <source>Stream Browser Plugin</source>
        <translation>Akış Tarayıcısı Eklentisi</translation>
    </message>
    <message>
        <location filename="../streambrowserfactory.cpp" line="50"/>
        <source>About Stream Browser Plugin</source>
        <translation>Akış Tarayıcısı Eklentisi Hakkında</translation>
    </message>
    <message>
        <location filename="../streambrowserfactory.cpp" line="51"/>
        <source>Qmmp Stream Browser Plugin</source>
        <translation>Qmmp Akış Tarayıcısı Eklentisi</translation>
    </message>
    <message>
        <location filename="../streambrowserfactory.cpp" line="52"/>
        <source>This plugin allows one to add stream from IceCast stream directory</source>
        <translation>Bu eklenti birinin IceCast akış dizininden akış eklemesine izin verir</translation>
    </message>
    <message>
        <location filename="../streambrowserfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Yazan: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>StreamWindow</name>
    <message>
        <location filename="../streamwindow.ui" line="14"/>
        <source>Stream Browser</source>
        <translation>Akış Tarayıcısı</translation>
    </message>
    <message>
        <location filename="../streamwindow.ui" line="31"/>
        <source>Filter:</source>
        <translation>Filtre:</translation>
    </message>
    <message>
        <location filename="../streamwindow.ui" line="47"/>
        <source>Favorites</source>
        <translation>Tercih Edillenler</translation>
    </message>
    <message>
        <location filename="../streamwindow.ui" line="73"/>
        <source>IceCast</source>
        <translation>IceCast</translation>
    </message>
    <message>
        <location filename="../streamwindow.ui" line="102"/>
        <source>Add</source>
        <translation>Ekle</translation>
    </message>
    <message>
        <location filename="../streamwindow.ui" line="109"/>
        <source>Update</source>
        <translation>Güncelle</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="55"/>
        <location filename="../streamwindow.cpp" line="69"/>
        <source>Name</source>
        <translation>Ad</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="55"/>
        <location filename="../streamwindow.cpp" line="69"/>
        <source>Genre</source>
        <translation>Tarz</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="55"/>
        <location filename="../streamwindow.cpp" line="69"/>
        <source>Bitrate</source>
        <translation>Bit hızı</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="55"/>
        <location filename="../streamwindow.cpp" line="69"/>
        <source>Format</source>
        <translation>Biçim</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="125"/>
        <source>&amp;Add to favorites</source>
        <translation>&amp;Gözdelere ekle</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="128"/>
        <source>&amp;Add to playlist</source>
        <translation>&amp;Çalma listesine ekle</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="131"/>
        <source>&amp;Create</source>
        <translation>&amp;Oluştur</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="132"/>
        <source>&amp;Edit</source>
        <translation>&amp;Düzenle</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="136"/>
        <location filename="../streamwindow.cpp" line="139"/>
        <source>&amp;Remove</source>
        <translation>&amp;Kaldır</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="152"/>
        <source>Done</source>
        <translation>Tamamlandı</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="155"/>
        <location filename="../streamwindow.cpp" line="156"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="175"/>
        <source>Receiving</source>
        <translation>Alınıyor</translation>
    </message>
    <message>
        <location filename="../streamwindow.cpp" line="278"/>
        <source>Edit Stream</source>
        <translation>Akışı Düzenle</translation>
    </message>
</context>
</TS>
