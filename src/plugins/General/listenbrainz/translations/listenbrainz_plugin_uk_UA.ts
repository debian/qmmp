<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>ListenBrainzFactory</name>
    <message>
        <location filename="../listenbrainzfactory.cpp" line="30"/>
        <source>ListenBrainz Plugin</source>
        <translation>Втулок ListenBrainz</translation>
    </message>
    <message>
        <location filename="../listenbrainzfactory.cpp" line="50"/>
        <source>About ListenBrainz Plugin</source>
        <translation>Про втулок ListenBrainz</translation>
    </message>
    <message>
        <location filename="../listenbrainzfactory.cpp" line="51"/>
        <source>Qmmp ListenBrainz Plugin</source>
        <translation>Втулок ListenBrainz для Qmmp</translation>
    </message>
    <message>
        <location filename="../listenbrainzfactory.cpp" line="52"/>
        <source>This plugin submits listen history to ListenBrainz server</source>
        <translation>Цей втулок надсилає дієпис прослуховування на сервер ListenBrainz</translation>
    </message>
    <message>
        <location filename="../listenbrainzfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов  &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>ListenBrainzSettingsDialog</name>
    <message>
        <location filename="../listenbrainzsettingsdialog.ui" line="14"/>
        <source>ListenBrainz Plugin Settings</source>
        <translation>Налаштування втулка ListenBrainz</translation>
    </message>
    <message>
        <location filename="../listenbrainzsettingsdialog.ui" line="29"/>
        <source>ListenBrainz user token:</source>
        <translation>Токен користувача ListenBrainz:</translation>
    </message>
</context>
</TS>
