<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>Converter</name>
    <message>
        <location filename="../converter.cpp" line="125"/>
        <location filename="../converter.cpp" line="228"/>
        <source>Cancelled</source>
        <translation>Afgebroken</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="140"/>
        <location filename="../converter.cpp" line="210"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="178"/>
        <source>Converting</source>
        <translation>Bezig met converteren</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="239"/>
        <source>Encoding</source>
        <translation>Bezig met omzetten</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="277"/>
        <source>Finished</source>
        <translation>Afgerond</translation>
    </message>
</context>
<context>
    <name>ConverterDialog</name>
    <message>
        <location filename="../converterdialog.ui" line="14"/>
        <source>Audio Converter</source>
        <translation>Audio converteren</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="44"/>
        <source>Progress</source>
        <translation>Voortgang</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="49"/>
        <source>State</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="63"/>
        <source>Output directory:</source>
        <translation>Uitvoermap:</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="80"/>
        <source>Output file name:</source>
        <translation>Uitvoerbestandsnaam:</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="97"/>
        <source>Preset:</source>
        <translation>Voorinstelling:</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="114"/>
        <source>Overwrite existing files</source>
        <translation>Bestaande bestanden overschrijven</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="124"/>
        <source>Convert</source>
        <translation>Converteren</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="131"/>
        <source>Stop</source>
        <translation>Stoppen</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="116"/>
        <source>Choose a directory</source>
        <translation>Kies een map</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="39"/>
        <source>Title</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="205"/>
        <source>Create a Copy</source>
        <translation>Kopie maken</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="136"/>
        <location filename="../converterdialog.cpp" line="358"/>
        <location filename="../converterdialog.cpp" line="365"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="141"/>
        <source>Waiting</source>
        <translation>Bezig met wachten</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="203"/>
        <source>Create</source>
        <translation>Maken</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="204"/>
        <source>Edit</source>
        <translation>Bewerken</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="206"/>
        <source>Delete</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="358"/>
        <source>Unable to execute &quot;%1&quot;. Program not found.</source>
        <translation>Kan &apos;%1&apos; niet uitvoeren: programma niet aangetroffen.</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="365"/>
        <source>Process &quot;%1&quot; finished with error.</source>
        <translation>Het proces &apos;%1&apos; is afgerond, maar met fouten.</translation>
    </message>
</context>
<context>
    <name>ConverterFactory</name>
    <message>
        <location filename="../converterfactory.cpp" line="28"/>
        <source>Converter Plugin</source>
        <translation>Converteer-plug-in</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="49"/>
        <source>About Converter Plugin</source>
        <translation>Over de Converteer-plug-in</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="50"/>
        <source>Qmmp Converter Plugin</source>
        <translation>Converteer-plug-in voor Qmmp</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="51"/>
        <source>This plugin converts supported audio files to other file formats using external command-line encoders</source>
        <translation>Deze plug-in converteert ondersteunde audiobestanden naar andere formaten door gebruik van externe cli-encoders</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Auteur: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>ConverterHelper</name>
    <message>
        <location filename="../converterhelper.cpp" line="33"/>
        <source>Convert</source>
        <translation>Converteren</translation>
    </message>
    <message>
        <location filename="../converterhelper.cpp" line="34"/>
        <source>Meta+C</source>
        <translation>Super+C</translation>
    </message>
</context>
<context>
    <name>ConverterPresetEditor</name>
    <message>
        <location filename="../converterpreseteditor.ui" line="14"/>
        <source>Preset Editor</source>
        <translation>Voorinstellingen bewerken</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="29"/>
        <source>General</source>
        <translation>Algemeen</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="35"/>
        <source>Name:</source>
        <translation>Naam:</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="45"/>
        <source>Extension:</source>
        <translation>Extensie:</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="58"/>
        <source>Command</source>
        <translation>Opdracht</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="77"/>
        <source>Options</source>
        <translation>Opties</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="83"/>
        <source>Write tags</source>
        <translation>Tags wegschrijven</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="90"/>
        <source>Convert to 16 bit</source>
        <translation>Converteren naar 16-bit</translation>
    </message>
</context>
<context>
    <name>PresetEditor</name>
    <message>
        <location filename="../converterpreseteditor.cpp" line="40"/>
        <source>%1 (Read Only)</source>
        <translation>%1 (alleen-lezen)</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.cpp" line="73"/>
        <source>Output file</source>
        <translation>Uitvoerbestand</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.cpp" line="74"/>
        <source>Input file</source>
        <translation>Invoerbestand</translation>
    </message>
</context>
</TS>
