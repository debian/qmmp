<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko">
<context>
    <name>Converter</name>
    <message>
        <location filename="../converter.cpp" line="125"/>
        <location filename="../converter.cpp" line="228"/>
        <source>Cancelled</source>
        <translation>취소됨</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="140"/>
        <location filename="../converter.cpp" line="210"/>
        <source>Error</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="178"/>
        <source>Converting</source>
        <translation>변환 중</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="239"/>
        <source>Encoding</source>
        <translation>인코딩</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="277"/>
        <source>Finished</source>
        <translation>완료됨</translation>
    </message>
</context>
<context>
    <name>ConverterDialog</name>
    <message>
        <location filename="../converterdialog.ui" line="14"/>
        <source>Audio Converter</source>
        <translation>오디오 변환기</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="44"/>
        <source>Progress</source>
        <translation>진행률</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="49"/>
        <source>State</source>
        <translation>상태</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="63"/>
        <source>Output directory:</source>
        <translation>출력 디렉터리:</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="80"/>
        <source>Output file name:</source>
        <translation>출력 파일 이름:</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="97"/>
        <source>Preset:</source>
        <translation>프리셋</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="114"/>
        <source>Overwrite existing files</source>
        <translation>기존 파일 덮어쓰기</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="124"/>
        <source>Convert</source>
        <translation>변환하기</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="131"/>
        <source>Stop</source>
        <translation>중지</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="116"/>
        <source>Choose a directory</source>
        <translation>디렉토리 선택하기</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="39"/>
        <source>Title</source>
        <translation>제목</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="205"/>
        <source>Create a Copy</source>
        <translation>사본 만들기</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="136"/>
        <location filename="../converterdialog.cpp" line="358"/>
        <location filename="../converterdialog.cpp" line="365"/>
        <source>Error</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="141"/>
        <source>Waiting</source>
        <translation>대기 중</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="203"/>
        <source>Create</source>
        <translation>만들기</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="204"/>
        <source>Edit</source>
        <translation>편집</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="206"/>
        <source>Delete</source>
        <translation>삭제</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="358"/>
        <source>Unable to execute &quot;%1&quot;. Program not found.</source>
        <translation>%1을(를) 실행할 수 없습니다. 프로그램을 찾을 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="365"/>
        <source>Process &quot;%1&quot; finished with error.</source>
        <translation>&quot;%1&quot; 프로세스가 오류와 함께 완료되었습니다.</translation>
    </message>
</context>
<context>
    <name>ConverterFactory</name>
    <message>
        <location filename="../converterfactory.cpp" line="28"/>
        <source>Converter Plugin</source>
        <translation>변환기 플러그인</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="49"/>
        <source>About Converter Plugin</source>
        <translation>변환기 플러그인 정보</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="50"/>
        <source>Qmmp Converter Plugin</source>
        <translation>Qmmp 변환기 플러그인</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="51"/>
        <source>This plugin converts supported audio files to other file formats using external command-line encoders</source>
        <translation>이 플러그인은 지원되는 오디오 파일을 외부 명령 줄 인코더를 사용하여 다른 파일 형식으로 변환합니다</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>작성자: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>ConverterHelper</name>
    <message>
        <location filename="../converterhelper.cpp" line="33"/>
        <source>Convert</source>
        <translation>변환하기</translation>
    </message>
    <message>
        <location filename="../converterhelper.cpp" line="34"/>
        <source>Meta+C</source>
        <translation>Meta+C</translation>
    </message>
</context>
<context>
    <name>ConverterPresetEditor</name>
    <message>
        <location filename="../converterpreseteditor.ui" line="14"/>
        <source>Preset Editor</source>
        <translation>프리셋 편집기</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="29"/>
        <source>General</source>
        <translation>일반</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="35"/>
        <source>Name:</source>
        <translation>이름:</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="45"/>
        <source>Extension:</source>
        <translation>확장자:</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="58"/>
        <source>Command</source>
        <translation>명령</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="77"/>
        <source>Options</source>
        <translation>옵션</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="83"/>
        <source>Write tags</source>
        <translation>태그 쓰기</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="90"/>
        <source>Convert to 16 bit</source>
        <translation>16비트로 변환</translation>
    </message>
</context>
<context>
    <name>PresetEditor</name>
    <message>
        <location filename="../converterpreseteditor.cpp" line="40"/>
        <source>%1 (Read Only)</source>
        <translation>%1 (읽기 전용)</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.cpp" line="73"/>
        <source>Output file</source>
        <translation>출력 파일</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.cpp" line="74"/>
        <source>Input file</source>
        <translation>입력 파일</translation>
    </message>
</context>
</TS>
