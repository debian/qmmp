<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>Converter</name>
    <message>
        <location filename="../converter.cpp" line="125"/>
        <location filename="../converter.cpp" line="228"/>
        <source>Cancelled</source>
        <translation>İptal Edildi</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="140"/>
        <location filename="../converter.cpp" line="210"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="178"/>
        <source>Converting</source>
        <translation>Dönüştürülüyor</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="239"/>
        <source>Encoding</source>
        <translation>Kodlama</translation>
    </message>
    <message>
        <location filename="../converter.cpp" line="277"/>
        <source>Finished</source>
        <translation>Bitirildi</translation>
    </message>
</context>
<context>
    <name>ConverterDialog</name>
    <message>
        <location filename="../converterdialog.ui" line="14"/>
        <source>Audio Converter</source>
        <translation>Ses Dönüştürücü</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="44"/>
        <source>Progress</source>
        <translation>İlerliyor</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="49"/>
        <source>State</source>
        <translation>Durum</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="63"/>
        <source>Output directory:</source>
        <translation>Çıktı dizini:</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="80"/>
        <source>Output file name:</source>
        <translation>Çıktı dosyası adı:</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="97"/>
        <source>Preset:</source>
        <translation>Tanımlı Ayar:</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="114"/>
        <source>Overwrite existing files</source>
        <translation>Mevcut dosyaların üzerine yaz</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="124"/>
        <source>Convert</source>
        <translation>Dönüştür</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="131"/>
        <source>Stop</source>
        <translation>Durdur</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="116"/>
        <source>Choose a directory</source>
        <translation>Dizin seç</translation>
    </message>
    <message>
        <location filename="../converterdialog.ui" line="39"/>
        <source>Title</source>
        <translation>Başlık</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="205"/>
        <source>Create a Copy</source>
        <translation>Kopyasını Oluştur </translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="136"/>
        <location filename="../converterdialog.cpp" line="358"/>
        <location filename="../converterdialog.cpp" line="365"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="141"/>
        <source>Waiting</source>
        <translation>Bekliyor</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="203"/>
        <source>Create</source>
        <translation>Oluştur</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="204"/>
        <source>Edit</source>
        <translation>Düzenle</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="206"/>
        <source>Delete</source>
        <translation>Sil</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="358"/>
        <source>Unable to execute &quot;%1&quot;. Program not found.</source>
        <translation>&quot;%1&quot; yürütülemiyor. Program bulunamadı.</translation>
    </message>
    <message>
        <location filename="../converterdialog.cpp" line="365"/>
        <source>Process &quot;%1&quot; finished with error.</source>
        <translation>&quot;%1&quot; işlemesi hatayla bitirildi.</translation>
    </message>
</context>
<context>
    <name>ConverterFactory</name>
    <message>
        <location filename="../converterfactory.cpp" line="28"/>
        <source>Converter Plugin</source>
        <translation>Dönüştürücü Eklentisi</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="49"/>
        <source>About Converter Plugin</source>
        <translation>Dönüştürücü Eklentisi Hakkında</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="50"/>
        <source>Qmmp Converter Plugin</source>
        <translation>Qmmp Dönüştürücü Eklentisi</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="51"/>
        <source>This plugin converts supported audio files to other file formats using external command-line encoders</source>
        <translation>Bu eklenti harici komut satırı kodlayıcıları kullanarak desteklenen ses dosyalarını diğer dosya biçimlerine dönüştürür</translation>
    </message>
    <message>
        <location filename="../converterfactory.cpp" line="53"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Yazan: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>ConverterHelper</name>
    <message>
        <location filename="../converterhelper.cpp" line="33"/>
        <source>Convert</source>
        <translation>Dönüştür</translation>
    </message>
    <message>
        <location filename="../converterhelper.cpp" line="34"/>
        <source>Meta+C</source>
        <translation>Meta+C</translation>
    </message>
</context>
<context>
    <name>ConverterPresetEditor</name>
    <message>
        <location filename="../converterpreseteditor.ui" line="14"/>
        <source>Preset Editor</source>
        <translation>Tanımlanmış Ayar Düzenleyici</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="29"/>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="35"/>
        <source>Name:</source>
        <translation>Ad:</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="45"/>
        <source>Extension:</source>
        <translation>Uzantı:</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="58"/>
        <source>Command</source>
        <translation>Komut</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="77"/>
        <source>Options</source>
        <translation>Seçenekler</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="83"/>
        <source>Write tags</source>
        <translation>Etiketleri yaz</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.ui" line="90"/>
        <source>Convert to 16 bit</source>
        <translation>16 bite dönüştür</translation>
    </message>
</context>
<context>
    <name>PresetEditor</name>
    <message>
        <location filename="../converterpreseteditor.cpp" line="40"/>
        <source>%1 (Read Only)</source>
        <translation>%1 (Sadece Oku)</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.cpp" line="73"/>
        <source>Output file</source>
        <translation>Çıktı dosyası</translation>
    </message>
    <message>
        <location filename="../converterpreseteditor.cpp" line="74"/>
        <source>Input file</source>
        <translation>Girdi dosyası</translation>
    </message>
</context>
</TS>
