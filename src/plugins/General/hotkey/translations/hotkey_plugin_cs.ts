<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>HotkeyDialog</name>
    <message>
        <location filename="../hotkeydialog.ui" line="14"/>
        <source>Modify Shortcut</source>
        <translation>Změnit zkratku</translation>
    </message>
    <message>
        <location filename="../hotkeydialog.ui" line="32"/>
        <source>Press the key combination you want to assign</source>
        <translation>Stlačte kombinaci kláves, kterou chcete přiřadit</translation>
    </message>
    <message>
        <location filename="../hotkeydialog.ui" line="52"/>
        <source>Clear</source>
        <translation>Smazat</translation>
    </message>
</context>
<context>
    <name>HotkeyFactory</name>
    <message>
        <location filename="../hotkeyfactory.cpp" line="31"/>
        <source>Global Hotkey Plugin</source>
        <translation>Modul klávesových zkratek</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="57"/>
        <source>About Global Hotkey Plugin</source>
        <translation>O modulu klávesových zkratek</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="58"/>
        <source>Qmmp Global Hotkey Plugin</source>
        <translation>Modul Qmmp pro klávesové zkratky</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="59"/>
        <source>This plugin adds support for multimedia keys or global key combinations</source>
        <translation>Tento modul přidává podporu multimediálních kláves a globálních klávesových zkratek</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="60"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HotkeySettingsDialog</name>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="14"/>
        <source>Global Hotkey Plugin Settings</source>
        <translation type="unfinished">Nastavení modulu klávesových zkratek</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="42"/>
        <source>Action</source>
        <translation type="unfinished">Akce</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="47"/>
        <source>Shortcut</source>
        <translation type="unfinished">Zkratka</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="55"/>
        <source>Reset</source>
        <translation type="unfinished">Původní</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="39"/>
        <source>Play</source>
        <translation type="unfinished">Hrát</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="40"/>
        <source>Stop</source>
        <translation type="unfinished">Zastavit</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="41"/>
        <source>Pause</source>
        <translation type="unfinished">Pauza</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="42"/>
        <source>Play/Pause</source>
        <translation type="unfinished">Hrát/Pauza</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="43"/>
        <source>Next</source>
        <translation type="unfinished">Další</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="44"/>
        <source>Previous</source>
        <translation type="unfinished">Předchozí</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="45"/>
        <source>Show/Hide</source>
        <translation type="unfinished">Zobrazit/Skrýt</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="46"/>
        <source>Volume +</source>
        <translation type="unfinished">Hlasitost +</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="47"/>
        <source>Volume -</source>
        <translation type="unfinished">Hlasitost -</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="48"/>
        <source>Forward 5 seconds</source>
        <translation type="unfinished">Vpřed o 5 sekund</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="49"/>
        <source>Rewind 5 seconds</source>
        <translation type="unfinished">Vzad o 5 sekund</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="50"/>
        <source>Jump to track</source>
        <translation type="unfinished">Skočit na stopu</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="51"/>
        <source>Mute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="108"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="108"/>
        <source>Key sequence &apos;%1&apos; is already used</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
