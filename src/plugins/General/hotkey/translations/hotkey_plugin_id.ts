<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id">
<context>
    <name>HotkeyDialog</name>
    <message>
        <location filename="../hotkeydialog.ui" line="14"/>
        <source>Modify Shortcut</source>
        <translation>Pintasan Modifikasi</translation>
    </message>
    <message>
        <location filename="../hotkeydialog.ui" line="32"/>
        <source>Press the key combination you want to assign</source>
        <translation>Tekan tombol kombinasi yang ingin kamu tetapkan</translation>
    </message>
    <message>
        <location filename="../hotkeydialog.ui" line="52"/>
        <source>Clear</source>
        <translation>Bersih</translation>
    </message>
</context>
<context>
    <name>HotkeyFactory</name>
    <message>
        <location filename="../hotkeyfactory.cpp" line="31"/>
        <source>Global Hotkey Plugin</source>
        <translation>Plugin Hotkey Global</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="57"/>
        <source>About Global Hotkey Plugin</source>
        <translation>Tentang Plugin Hotkey Global</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="58"/>
        <source>Qmmp Global Hotkey Plugin</source>
        <translation>Plugin Hotkey Global Qmmp</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="59"/>
        <source>This plugin adds support for multimedia keys or global key combinations</source>
        <translation>Plugin ini menambahkan dukungan untuk tombol multimedia atau kombinasi tombol global</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="60"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Ditulis oleh: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>HotkeySettingsDialog</name>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="14"/>
        <source>Global Hotkey Plugin Settings</source>
        <translation type="unfinished">Setelan Plugin Hotkey Global</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="42"/>
        <source>Action</source>
        <translation type="unfinished">Tindakan</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="47"/>
        <source>Shortcut</source>
        <translation type="unfinished">Pintasan</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="55"/>
        <source>Reset</source>
        <translation type="unfinished">Setel ulang</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="39"/>
        <source>Play</source>
        <translation type="unfinished">Main</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="40"/>
        <source>Stop</source>
        <translation type="unfinished">Henti</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="41"/>
        <source>Pause</source>
        <translation type="unfinished">Jeda</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="42"/>
        <source>Play/Pause</source>
        <translation type="unfinished">Main/Jeda</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="43"/>
        <source>Next</source>
        <translation type="unfinished">Selanjutnya</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="44"/>
        <source>Previous</source>
        <translation type="unfinished">Sebelumnya</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="45"/>
        <source>Show/Hide</source>
        <translation type="unfinished">Tampak/Sembunyi</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="46"/>
        <source>Volume +</source>
        <translation type="unfinished">Volume +</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="47"/>
        <source>Volume -</source>
        <translation type="unfinished">Volume -</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="48"/>
        <source>Forward 5 seconds</source>
        <translation type="unfinished">Maju 5 detik</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="49"/>
        <source>Rewind 5 seconds</source>
        <translation type="unfinished">Mundur 5 detik</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="50"/>
        <source>Jump to track</source>
        <translation type="unfinished">Lompat ke track</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="51"/>
        <source>Mute</source>
        <translation type="unfinished">Bungkam</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="108"/>
        <source>Warning</source>
        <translation type="unfinished">Peringatan</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="108"/>
        <source>Key sequence &apos;%1&apos; is already used</source>
        <translation type="unfinished">Rangkaian tombol &apos;%1&apos; sudah digunakan</translation>
    </message>
</context>
</TS>
