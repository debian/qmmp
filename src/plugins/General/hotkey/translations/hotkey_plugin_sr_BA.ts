<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sr_BA">
<context>
    <name>HotkeyDialog</name>
    <message>
        <location filename="../hotkeydialog.ui" line="14"/>
        <source>Modify Shortcut</source>
        <translation>Измијени пречицу</translation>
    </message>
    <message>
        <location filename="../hotkeydialog.ui" line="32"/>
        <source>Press the key combination you want to assign</source>
        <translation>Притисните комбинацију тастера коју желите да додијелите</translation>
    </message>
    <message>
        <location filename="../hotkeydialog.ui" line="52"/>
        <source>Clear</source>
        <translation>Очисти</translation>
    </message>
</context>
<context>
    <name>HotkeyFactory</name>
    <message>
        <location filename="../hotkeyfactory.cpp" line="31"/>
        <source>Global Hotkey Plugin</source>
        <translation>Глобалне пречице</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="57"/>
        <source>About Global Hotkey Plugin</source>
        <translation>О прикључку за глобалне пречице</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="58"/>
        <source>Qmmp Global Hotkey Plugin</source>
        <translation>Кумп прикључак за глобалне пречице</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="59"/>
        <source>This plugin adds support for multimedia keys or global key combinations</source>
        <translation>Подршка за мултимедијалне тастере или глобалне комбинације тастера</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="60"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Аутор: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>HotkeySettingsDialog</name>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="14"/>
        <source>Global Hotkey Plugin Settings</source>
        <translation type="unfinished">Поставке глобалних пречица</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="42"/>
        <source>Action</source>
        <translation type="unfinished">радња</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="47"/>
        <source>Shortcut</source>
        <translation type="unfinished">пречица</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="55"/>
        <source>Reset</source>
        <translation type="unfinished">Ресетуј</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="39"/>
        <source>Play</source>
        <translation type="unfinished">Пусти</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="40"/>
        <source>Stop</source>
        <translation type="unfinished">Заустави</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="41"/>
        <source>Pause</source>
        <translation type="unfinished">Паузирај</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="42"/>
        <source>Play/Pause</source>
        <translation type="unfinished">Пусти/паузирај</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="43"/>
        <source>Next</source>
        <translation type="unfinished">Сљедећа</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="44"/>
        <source>Previous</source>
        <translation type="unfinished">Претходна</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="45"/>
        <source>Show/Hide</source>
        <translation type="unfinished">Прикажи/сакриј</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="46"/>
        <source>Volume +</source>
        <translation type="unfinished">Јачина +</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="47"/>
        <source>Volume -</source>
        <translation type="unfinished">Јачина -</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="48"/>
        <source>Forward 5 seconds</source>
        <translation type="unfinished">Напријед 5 секунди</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="49"/>
        <source>Rewind 5 seconds</source>
        <translation type="unfinished">Уназад 5 секунди</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="50"/>
        <source>Jump to track</source>
        <translation type="unfinished">Скочи на нумеру</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="51"/>
        <source>Mute</source>
        <translation type="unfinished">Утишај</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="108"/>
        <source>Warning</source>
        <translation type="unfinished">Упозорење</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="108"/>
        <source>Key sequence &apos;%1&apos; is already used</source>
        <translation type="unfinished">Комбинација „%1“ је већ у употреби</translation>
    </message>
</context>
</TS>
