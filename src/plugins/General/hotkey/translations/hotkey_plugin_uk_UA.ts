<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>HotkeyDialog</name>
    <message>
        <location filename="../hotkeydialog.ui" line="14"/>
        <source>Modify Shortcut</source>
        <translation>Змінити сполучення клавішів</translation>
    </message>
    <message>
        <location filename="../hotkeydialog.ui" line="32"/>
        <source>Press the key combination you want to assign</source>
        <translation>Натисніть клавіші, які Ви бажаєте призначити</translation>
    </message>
    <message>
        <location filename="../hotkeydialog.ui" line="52"/>
        <source>Clear</source>
        <translation>Очистити</translation>
    </message>
</context>
<context>
    <name>HotkeyFactory</name>
    <message>
        <location filename="../hotkeyfactory.cpp" line="31"/>
        <source>Global Hotkey Plugin</source>
        <translation>Втулок глобальних клавішів</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="57"/>
        <source>About Global Hotkey Plugin</source>
        <translation>Про втулок глобальних клавішів</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="58"/>
        <source>Qmmp Global Hotkey Plugin</source>
        <translation>Втулок глобальних клавішів для Qmmp</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="59"/>
        <source>This plugin adds support for multimedia keys or global key combinations</source>
        <translation>Цей втулок додає підтримку мультимедійних чи глобальних клавішів</translation>
    </message>
    <message>
        <location filename="../hotkeyfactory.cpp" line="60"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Розробник: Ілля Котов &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>HotkeySettingsDialog</name>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="14"/>
        <source>Global Hotkey Plugin Settings</source>
        <translation>Налаштування втулка глобальних клавіш</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="42"/>
        <source>Action</source>
        <translation>Дія</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="47"/>
        <source>Shortcut</source>
        <translation>Гарячий клавіш</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.ui" line="55"/>
        <source>Reset</source>
        <translation>Скинути</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="39"/>
        <source>Play</source>
        <translation>Відтворити</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="40"/>
        <source>Stop</source>
        <translation>Зупинити</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="41"/>
        <source>Pause</source>
        <translation>Павза</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="42"/>
        <source>Play/Pause</source>
        <translation>Відтворити/Павза</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="43"/>
        <source>Next</source>
        <translation>Уперед</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="44"/>
        <source>Previous</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="45"/>
        <source>Show/Hide</source>
        <translation>Показати/Сховати</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="46"/>
        <source>Volume +</source>
        <translation>Гучність +</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="47"/>
        <source>Volume -</source>
        <translation>Гучність -</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="48"/>
        <source>Forward 5 seconds</source>
        <translation>Уперед на 5 секунд</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="49"/>
        <source>Rewind 5 seconds</source>
        <translation>Назад на 5 секунд</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="50"/>
        <source>Jump to track</source>
        <translation>Перейти на доріжку</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="51"/>
        <source>Mute</source>
        <translation>Вимкнути звук</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="108"/>
        <source>Warning</source>
        <translation>Попередження</translation>
    </message>
    <message>
        <location filename="../hotkeysettingsdialog.cpp" line="108"/>
        <source>Key sequence &apos;%1&apos; is already used</source>
        <translation>Сполучення клавішів &quot;%1&quot; вже використовується</translation>
    </message>
</context>
</TS>
