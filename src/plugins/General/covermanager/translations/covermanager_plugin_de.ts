<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>CoverManager</name>
    <message>
        <location filename="../covermanager.cpp" line="35"/>
        <source>Show Cover</source>
        <translation>Cover anzeigen</translation>
    </message>
    <message>
        <location filename="../covermanager.cpp" line="36"/>
        <source>Ctrl+M</source>
        <translation>Strg+M</translation>
    </message>
</context>
<context>
    <name>CoverManagerFactory</name>
    <message>
        <location filename="../covermanagerfactory.cpp" line="29"/>
        <source>Cover Manager Plugin</source>
        <translation>Cover-Verwaltungsmodul</translation>
    </message>
    <message>
        <location filename="../covermanagerfactory.cpp" line="49"/>
        <source>About Cover Manager Plugin</source>
        <translation>Über Cover-Verwaltungsmodul</translation>
    </message>
    <message>
        <location filename="../covermanagerfactory.cpp" line="50"/>
        <source>Qmmp Cover Manager Plugin</source>
        <translation>Qmmp Cover-Verwaltungsmodul</translation>
    </message>
    <message>
        <location filename="../covermanagerfactory.cpp" line="51"/>
        <source>Written by: Ilya Kotov &lt;forkotov02@ya.ru&gt;</source>
        <translation>Geschrieben von: Ilya Kotov &lt;forkotov02@ya.ru&gt;</translation>
    </message>
</context>
<context>
    <name>CoverWidget</name>
    <message>
        <location filename="../coverwidget.cpp" line="39"/>
        <location filename="../coverwidget.cpp" line="41"/>
        <source>&amp;Save As...</source>
        <translation>&amp;Speichern unter …</translation>
    </message>
    <message>
        <location filename="../coverwidget.cpp" line="39"/>
        <location filename="../coverwidget.cpp" line="41"/>
        <source>Ctrl+S</source>
        <translation>Strg+S</translation>
    </message>
    <message>
        <location filename="../coverwidget.cpp" line="43"/>
        <source>Size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="../coverwidget.cpp" line="45"/>
        <source>Actual Size</source>
        <translation>Originalgröße</translation>
    </message>
    <message>
        <location filename="../coverwidget.cpp" line="46"/>
        <source>128x128</source>
        <translation>128×128</translation>
    </message>
    <message>
        <location filename="../coverwidget.cpp" line="47"/>
        <source>256x256</source>
        <translation>256×256</translation>
    </message>
    <message>
        <location filename="../coverwidget.cpp" line="48"/>
        <source>512x512</source>
        <translation>512×512</translation>
    </message>
    <message>
        <location filename="../coverwidget.cpp" line="49"/>
        <source>1024x1024</source>
        <translation>1024×1024</translation>
    </message>
    <message>
        <location filename="../coverwidget.cpp" line="53"/>
        <location filename="../coverwidget.cpp" line="55"/>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
    <message>
        <location filename="../coverwidget.cpp" line="53"/>
        <location filename="../coverwidget.cpp" line="55"/>
        <source>Alt+F4</source>
        <translation>Alt+F4</translation>
    </message>
    <message>
        <location filename="../coverwidget.cpp" line="96"/>
        <source>Save Cover As</source>
        <translation>Cover speichern unter</translation>
    </message>
    <message>
        <location filename="../coverwidget.cpp" line="97"/>
        <source>Images</source>
        <translation>Bilder</translation>
    </message>
</context>
</TS>
